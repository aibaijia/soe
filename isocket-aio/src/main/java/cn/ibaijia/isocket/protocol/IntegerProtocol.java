package cn.ibaijia.isocket.protocol;

import cn.ibaijia.isocket.session.Session;

import java.nio.ByteBuffer;

public class IntegerProtocol implements Protocol<ByteBuffer,Integer> {

    private static final int INT_LENGTH = LenType.INT.len();

    @Override
    public Integer decode(ByteBuffer readBuffer, Session session) {
        if (readBuffer.remaining() < INT_LENGTH)
            return null;
        return readBuffer.getInt();
    }

    @Override
    public ByteBuffer encode(Integer integer, Session session) {
        ByteBuffer buffer = ByteBuffer.allocate(INT_LENGTH);
        buffer.putInt(integer);
        buffer.flip();
        return buffer;
    }
}
