package cn.ibaijia.isocket.protocol;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.isocket.session.SessionState;
import cn.ibaijia.isocket.util.BufferUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class FixLengthBigBufferProtocol<B extends Buffer> implements Protocol<ByteBuffer, ByteBuffer> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private short HEAD_LENGTH = 5;
    private Byte endFlag = '\n';

    public FixLengthBigBufferProtocol() {

    }

    @Override
    public ByteBuffer decode(ByteBuffer readBuffer, Session session) {
        SessionState sessionState = SessionManager.getSessionState(session);
        logger.trace("decode decodeNew:{}", sessionState.decodeNew);
        if (readBuffer.remaining() == 0) {
            logger.trace("readBuffer.remaining() == 0");
            return null;
        }
        //如果新消息
        if (sessionState.decodeNew) {
            if (readBuffer.remaining() < HEAD_LENGTH) {
                logger.trace("remaining length:{} < HEAD_LENGTH:{}", readBuffer.remaining(), HEAD_LENGTH);
                return null;
            }
            BufferUtil.free(sessionState.bigBuffer);
            //判断是否存在半包情况
            int len = readBuffer.getInt() - HEAD_LENGTH;
            sessionState.bigBuffer = BufferUtil.allocate(len, session.getContext().isUseDirectBuffer());
            sessionState.decodeNew = false;
        }
        if (sessionState.bigBuffer.hasRemaining()) {
            if (readBuffer.remaining() <= sessionState.bigBuffer.remaining()) {
                sessionState.bigBuffer.put(readBuffer);
            } else {//多了 少读,-- 只读读满当前buff
                byte[] bytes = new byte[sessionState.bigBuffer.remaining()];
                readBuffer.get(bytes);
                sessionState.bigBuffer.put(bytes);
            }
        }

        if (!sessionState.bigBuffer.hasRemaining() && readBuffer.hasRemaining()) {//bigBuffer 满了,并且 readBuffer 还有
            byte exceptFlag = readBuffer.get();
            if (exceptFlag != endFlag) {//预期结束标志不符合
                logger.error("exceptFlag:{} != endFlag:{}, end flag error,make sure you get the big buffer protocol!", exceptFlag, endFlag);
                SessionManager.close(session);
                return null;
            }
            sessionState.decodeNew = true;
            return sessionState.bigBuffer;
        } else {
            sessionState.decodeNew = false;
        }
        return null;
    }


    @Override
    public ByteBuffer encode(ByteBuffer object, Session session) {
        logger.trace("encode");
        if (object == null) {
            return null;
        }
        int length = HEAD_LENGTH + object.capacity();
        ByteBuffer buffer = BufferUtil.allocate(length, object.isDirect());
        buffer.putInt(length);
        buffer.put(object.array());
        buffer.put(endFlag);
        buffer.flip();
        return buffer;
    }
}
