package cn.ibaijia.isocket.protocol;

import cn.ibaijia.isocket.session.Session;

import java.nio.ByteBuffer;

public class ByteBufferProtocol implements Protocol<ByteBuffer,ByteBuffer> {

    private static final short LENGTH = LenType.BYTE.len();

    @Override
    public ByteBuffer decode(ByteBuffer readBuffer, Session session) {
        if (readBuffer.remaining() < LENGTH)
            return null;
        byte[] bytes = new byte[readBuffer.remaining()];
        readBuffer.get(bytes);
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer encode(ByteBuffer writeBuffer, Session session) {
        return writeBuffer;
    }
}
