package cn.ibaijia.isocket.protocol;

import cn.ibaijia.isocket.session.Session;


public interface Protocol<L, H> {

    /**
     * @param data 需要解码的数据 如果是buffer 不需要flip,框架已经flip了,毕竟使用者又可以少写一行代码
     * @param session
     * @return  输出 decode对象
     */
    H decode(L data, Session session);

    /**
     *
     * @param data 需要编码的数据
     * @param session
     * @return 解码返回的对象 如果是buffer 不需要flip,框架会flip,推荐不flip,毕竟可使用者又以少写一行代码
     */
    L encode(H data, Session session);

}
