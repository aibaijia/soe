package cn.ibaijia.isocket.protocol;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * 每条不能超过ReadBufferSize
 */
public class FixLengthStringProtocol implements Protocol<ByteBuffer, String> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private short HEAD_LENGTH = 5;
    private Byte endFlag = '\n';
    private Charset charset = Charset.forName("UTF-8");

    public FixLengthStringProtocol() {

    }


    @Override
    public String decode(ByteBuffer readBuffer, Session session) {
        logger.trace("decode");
        if (readBuffer.remaining() < HEAD_LENGTH) {
            logger.trace("remaining length:{} < intLen:{}", readBuffer.remaining(), HEAD_LENGTH);
            return null;
        }
        //判断是否存在半包情况
        readBuffer.mark();
        int len = readBuffer.getInt() - HEAD_LENGTH;
        if (readBuffer.remaining() < len + 1) {
            logger.trace("remaining length:{} < msgLen:{}", readBuffer.remaining(), len);
            readBuffer.reset();
            return null;
        }
        byte[] bytes = new byte[len];
        readBuffer.get(bytes);
        byte exceptFlag = readBuffer.get();
        if (exceptFlag != endFlag) {//预期结束标志不符合
            logger.error("exceptFlag:{} != endFlag:{}, end flag error,make sure you get the string protocol!", exceptFlag, endFlag);
            SessionManager.close(session);
            return null;
        }
        String strData = null;
        try {
            strData = new String(bytes, charset);
        } catch (Exception e) {
            logger.error("bytes to String error,charset:" + charset, e);
            SessionManager.close(session);
        }
        return strData;
    }

    @Override
    public ByteBuffer encode(String object, Session session) {
        logger.trace("encode");
        byte[] bytes = new byte[0];
        try {
            bytes = object.getBytes(charset);
        } catch (Exception e) {
            logger.error("String to bytes error,charset:" + charset, e);
            SessionManager.close(session);
        }
        ByteBuffer buffer = ByteBuffer.allocate(HEAD_LENGTH + bytes.length);
        buffer.putInt(HEAD_LENGTH + bytes.length);//msg length
        buffer.put(bytes);
        buffer.put(endFlag);
        buffer.flip();
        return buffer;
    }

    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }
}
