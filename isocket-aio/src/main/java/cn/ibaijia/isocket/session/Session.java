package cn.ibaijia.isocket.session;


import cn.ibaijia.isocket.Context;
import cn.ibaijia.isocket.cache.EhCacheService;
import cn.ibaijia.isocket.handler.ReadCompletionHandler;
import cn.ibaijia.isocket.handler.WriteCompletionHandler;
import cn.ibaijia.isocket.protocol.Protocol;
import cn.ibaijia.isocket.util.BufferUtil;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.NetworkChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Session {
    private static final Logger logger = LoggerFactory.getLogger(Session.class);

    private String sessionId;
    private Context context;

    private AsynchronousSocketChannel channel;
    private ByteBuffer readBuffer;
    private Map<String, Object> sessionMap = new HashMap<String, Object>();
    private InetSocketAddress localAddress;
    private InetSocketAddress remoteAddress;
    private boolean needClose = false;
    /**
     * 响应消息缓存队列。 queue ehcache redis?
     */
    private Queue<ByteBuffer> writeCacheQueue;
    private ReadCompletionHandler readCompletionHandler;
    private WriteCompletionHandler writeCompletionHandler;

    public Lock lock = new ReentrantLock();
    public Condition readCond = lock.newCondition();

    private EhCacheService ehCache = null;

    public Session(AsynchronousSocketChannel channel, Context context) {
        this.channel = channel;
        this.context = context;
        this.sessionId = SessionManager.genId(channel);
        if (context.isUseCompactQueue()) {
            if (context.getCompactBuffSize() > 100) {
                writeCacheQueue = new CompactBufferQueue(context.getCompactBuffSize());
            } else {
                writeCacheQueue = new CompactBufferQueue();
            }
        } else {
            writeCacheQueue = new ConcurrentLinkedQueue<>();
        }
        if (context.isUseEhcache()) {
            logger.info("use ehcache.");
            ehCache = EhCacheService.getInstance(context.getEhCacheConfig());
            ehCache.put(new Element(this, writeCacheQueue));
        }
        readBuffer = BufferUtil.allocate(context.getReadBuffSize(), context.isUseDirectBuffer());
        readCompletionHandler = new ReadCompletionHandler(this);
        writeCompletionHandler = new WriteCompletionHandler(this);
        context.getSessionListener().create(this);
        readNext();
    }

    public void releaseAndWriteNext() {
        lock.lock();
        try {
            readCond.signal();
            writeNext();
        } catch (Exception e) {
            logger.error("releaseAndWriteNext error.", e);
        } finally {
            lock.unlock();
        }
    }

    private void writeNext() {
        if (this.context.getProtocolList().isEmpty() || this.context.getProcessor() == null) {
            logger.error("protocol or processor not config return.");
            return;
        }
        logger.trace("writeNext");
        if (!writeCacheQueue.isEmpty()) {//还有就写下一条
            lock.lock();
            try {
                ByteBuffer byteBuffer = writeCacheQueue.poll();
                writeBuffer(byteBuffer);
                readCond.await();
            } catch (Exception e) {
                logger.error("writeBuffer error!", e);
            } finally {
                lock.unlock();
                if (this.needClose) {
                    SessionManager.close(this);
                }
            }
        } else {//队列为空 关闭等待的则执行关闭
            if (this.needClose) {//closing也会 继续写
                close(true);
            }
        }
    }

    public void readNext() {
        logger.trace("readNext");
        continueRead();
    }

    private boolean check() {
        int cacheSize = writeCacheQueue.size();
        if (cacheSize > context.getWriteWarnLimit()) {
            // 如果达到预警值交由用户控制
            return context.getSessionListener().writeWarn(this, cacheSize);
        }
        return true;
    }

    /**
     * 读到当前 readBuffer
     */
    private void continueRead() {
        logger.trace("continueRead:{} ", channel == null);
        channel.read(readBuffer, readBuffer, readCompletionHandler);
    }

    /**
     * 写到当前 writeBuffer
     */
    private void continueWrite(ByteBuffer buffer) {
        logger.trace("continueWrite");
        channel.write(buffer, buffer, writeCompletionHandler);
    }

    /**
     * 直接写出 buffer
     */
    private void writeBuffer(ByteBuffer buffer) {
        logger.trace("writeBuffer");
        try {
            continueWrite(buffer);
        } catch (Exception e) {
            logger.error("writeBuffer error!", e);
        } finally {
            BufferUtil.free(buffer);// TODO 会不会 把异步中的 DirectBuffer清除了?
        }
    }

    /**
     * 加到队列最后 等待写出
     * 写到当前 writeBuffer
     */
    private void write(ByteBuffer buffer) {
        writeCacheQueue.add(buffer);
        writeNext();
    }

    /**
     * 强制关闭当前
     */
    public void close() {
        close(true);
    }

    /**
     * 是否立即关闭会话
     */
    public void close(boolean force) {
        if (channel == null) {
            return;
        }
        if (writeCacheQueue.isEmpty()) {
            force = true;
        }
        this.needClose = true;
        if (force) {
            try {
                channel.close();
                channel = null;
            } catch (Exception e) {
                logger.error("close session exception", e);
            }
            context.getSessionListener().closed(this);
        }
    }

    /**
     * 触发通道的读操作，当发现存在严重消息积压时,会触发流控
     */
    public void processReadBuffer() {
        if (this.context.getProtocolList().isEmpty() || this.context.getProcessor() == null) {
            logger.error("protocol or processor not config return.");
            return;
        }
        readBuffer.flip();
        Object dataEntry;
        while (true) {
            dataEntry = decode(readBuffer);
            if (dataEntry == null) {
                logger.trace("break decode.");
                break;
            }
            //处理消息
            try {
                boolean success = context.getProcessor().process(this, dataEntry);
                if (!success) {
                    context.getSessionListener().processError(this, dataEntry, new RuntimeException("process fail."));
                }
            } catch (Exception e) {
                context.getSessionListener().processError(this, dataEntry, e);
            }
        }
        readBuffer.compact();// pos 不变，limit 变为 capacity
    }

    private Object decode(ByteBuffer readBuffer) {
        List<Protocol> protocolList = context.getProtocolList();
        Object inputData = readBuffer;
        for (int i = protocolList.size() - 1; i >= 0; i--) {
            inputData = (Object) (protocolList.get(i).decode(inputData, this));
        }
        return inputData;
    }

    private ByteBuffer encode(Object object) {
        if (object == null) {
            return null;
        }
        List<Protocol> protocolList = context.getProtocolList();
        Object outputData = object;
        for (int i = 0; i < protocolList.size(); i++) {
            outputData = protocolList.get(i).encode(outputData, this);
        }
        if (outputData instanceof ByteBuffer) {
            return (ByteBuffer) outputData;
        } else {
            logger.error("protocol not encode to ByteBuffer,please check protocol chain.");
            SessionManager.close(this);
            return null;
        }
    }

    public boolean write(Object t) {
        if (check()) {
            write(encode(t));
            return true;
        } else {
            return false;
        }

    }

    public String getSessionID() {
        return this.sessionId;
    }

    public Context getContext() {
        return context;
    }

    public void setAttribute(String key, Object value) {
        sessionMap.put(key, value);
    }

    public Object getAttribute(String key) {
        return sessionMap.get(key);
    }

    public InetSocketAddress getLocalAddress() {
        if (this.localAddress == null && this.channel != null) {
            try {
                this.localAddress = (InetSocketAddress) channel.getLocalAddress();
            } catch (IOException e) {
                logger.error("getLocalAddress error!", e);
            }
        }
        return this.localAddress;
    }

    public InetSocketAddress getRemoteAddress() {
        if (this.remoteAddress == null && this.channel != null) {
            try {
                this.remoteAddress = (InetSocketAddress) channel.getRemoteAddress();
            } catch (IOException e) {
                logger.error("getRemoteAddress error!", e);
            }
        }
        return remoteAddress;
    }


    public NetworkChannel getChannel() {
        return channel;
    }

    public boolean isOpen() {
        return !needClose;
    }
}
