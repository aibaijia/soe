package cn.ibaijia.isocket.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.AsynchronousSocketChannel;
import java.util.HashMap;
import java.util.Map;

public class SessionManager {
    private static final Logger logger = LoggerFactory.getLogger(SessionManager.class);
    private static Map<String, Session> sessionMap = new HashMap<>();

    public static void put(Session session) {
        sessionMap.put(session.getSessionID(), session);
    }

    public static Session get(String sessionId) {
        return sessionMap.get(sessionId);
    }

    public static Session get(AsynchronousSocketChannel channel) {
        String sessionId = genId(channel);
        return sessionMap.get(sessionId);
    }

    public static boolean close(String sessionId) {
        Session session = sessionMap.remove(sessionId);
        return close(session);
    }

    public static boolean close(AsynchronousSocketChannel channel) {
        String sessionId = genId(channel);
        Session session = sessionMap.remove(sessionId);
        return close(session);
    }

    public static boolean close(Session session) {
        logger.trace("session closing:{}", session.getSessionID());
        if (session != null) {
            closeState(session);
            sessionMap.remove(session.getSessionID());
            session.close();
            return true;
        }
        return false;
    }

    public static String genId(AsynchronousSocketChannel channel) {
        String sessionId = null;
        if (channel != null) {
            try {
                sessionId = channel.getLocalAddress() + "_" + channel.getRemoteAddress();
            } catch (Exception e) {
                logger.error("genId error.");
            }
        }
        return sessionId;
    }

    public static SessionState getSessionState(Session session) {
        SessionState sessionState = (SessionState) session.getAttribute("_sessionState");
        if (sessionState == null) {
            sessionState = new SessionState();
            session.setAttribute("_sessionState", sessionState);
        }
        return sessionState;
    }

    private static void closeState(Session session) {
        SessionState sessionState = (SessionState) session.getAttribute("_sessionState");
        if (sessionState != null) {
            sessionState.close();
        }
    }

}
