package cn.ibaijia.isocket.session;

import cn.ibaijia.isocket.util.BufferUtil;

import java.nio.ByteBuffer;

public class SessionState {

    public ByteBuffer bigBuffer = null;
    public boolean decodeNew = true;

    public void close() {
        if (bigBuffer != null) {
            BufferUtil.free(bigBuffer);
        }
        bigBuffer = null;
    }
}
