package cn.ibaijia.isocket;

import java.net.SocketOption;
import java.util.*;

import cn.ibaijia.isocket.cache.EhCacheConfig;
import cn.ibaijia.isocket.listener.*;
import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.protocol.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Context<T> {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static int NOT_CLOSE = 0;
    protected static int CLOSING = 1;
    protected static int CLOSED = 2;
    protected int closeStatus = NOT_CLOSE;// 0 normal 1 closing 2 closed
    protected String host;
    protected int port;
    protected String banner = "\n" +
            "  _                _        _   \n" +
            " (_)___  ___   ___| | _____| |_ \n" +
            " | / __|/ _ \\ / __| |/ / _ \\ __|\n" +
            " | \\__ \\ (_) | (__|   <  __/ |_ \n" +
            " |_|___/\\___/ \\___|_|\\_\\___|\\__|\n" +
            "                                \n";
    /**
     * 协议编码解码器
     */
    protected List<Protocol> protocolList = new ArrayList<>();
    /**
     * 业务数据处理器
     */
    protected Processor<T> processor;
    /**
     * Session监听器
     */
    protected SessionListener sessionListener = new DefaultSessionListener();
    /**
     * 线程池数
     */
    protected int threadNumber = Runtime.getRuntime().availableProcessors() < 4 ? 4 : Runtime.getRuntime().availableProcessors();
    /**
     * 读缓冲区大小
     */
    protected int readBuffSize = 256 * 1024;
    /**
     * 是否使用压缩缓冲区
     */
    protected boolean useCompactQueue = false;
    /**
     * 压缩缓冲区大小
     */
    protected int compactBuffSize = 100 * 1024;
    /**
     * 只对读缓冲区 和 big buffer缓冲区有效
     */
    protected boolean useDirectBuffer = false;
    /**
     * 写缓冲区预警数
     */
    protected int writeWarnLimit = 500;

    protected Set<String> blackList;

    protected Map<SocketOption, Object> options = new HashMap<SocketOption, Object>();

    /**
     * 是否使用EhCache
     */
    protected boolean useEhcache = false;

    protected EhCacheConfig ehCacheConfig = new EhCacheConfig();

    public List<Protocol> getProtocolList() {
        return protocolList;
    }


    public Context<?> addProtocol(Protocol<?, ?> protocol) {
        this.protocolList.add(protocol);
        return this;
    }

    public void setProcessor(Processor<T> processor) {
        this.processor = processor;
    }

    public Processor<T> getProcessor() {
        return processor;
    }

    public SessionListener getSessionListener() {
        return sessionListener;
    }

    public void setSessionListener(SessionListener sessionListener) {
        this.sessionListener = sessionListener;
    }

    public Context setSessionCreateListener(SessionCreateListener listener) {
        this.sessionListener.setSessionCreateListener(listener);
        return this;
    }

    public Context setSessionReadCompleteListener(SessionReadCompleteListener listener) {
        this.sessionListener.setSessionReadCompleteListener(listener);
        return this;
    }

    public Context setSessionReadErrorListener(SessionReadErrorListener listener) {
        this.sessionListener.setSessionReadErrorListener(listener);
        return this;
    }

    public Context setSessionProcessErrorListener(SessionProcessErrorListener listener) {
        this.sessionListener.setSessionProcessErrorListener(listener);
        return this;
    }

    public Context setSessionWriteCompleteListener(SessionWriteCompleteListener listener) {
        this.sessionListener.setSessionWriteCompleteListener(listener);
        return this;
    }

    public Context setSessionWriteErrorListener(SessionWriteErrorListener listener) {
        this.sessionListener.setSessionWriteErrorListener(listener);
        return this;
    }

    public Context setSessionLimitWarnListener(SessionWriteWarnListener listener) {
        this.sessionListener.setSessionWriteWarnListener(listener);
        return this;
    }

    public Context setSessionClosedListener(SessionClosedListener listener) {
        this.sessionListener.setSessionClosedListener(listener);
        return this;
    }

    public int getThreadNumber() {
        return threadNumber;
    }

    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    public int getReadBuffSize() {
        return readBuffSize;
    }

    public boolean isUseCompactQueue() {
        return useCompactQueue;
    }

    public int getCompactBuffSize() {
        return compactBuffSize;
    }

    public boolean isUseDirectBuffer() {
        return useDirectBuffer;
    }

    public int getWriteWarnLimit() {
        return writeWarnLimit;
    }

    public Set<String> getBlackList() {
        return blackList;
    }

    public void setBlackList(Set<String> blackList) {
        this.blackList = blackList;
    }

    public Context<?> setOption(SocketOption socketOption, Object value) {
        this.options.put(socketOption, value);
        return this;
    }

    public Context<?> setThreadNumber(Integer threadNumber) {
        this.threadNumber = threadNumber;
        return this;
    }

    public Context<?> setReadBuffSize(int readBuffSize) {
        if (readBuffSize > 4) {
            this.readBuffSize = readBuffSize;
        }
        return this;
    }

    public Context<?> setUseCompactQueue(boolean useCompactQueue) {
        this.useCompactQueue = useCompactQueue;
        return this;
    }

    public Context<?> setCompactBuffSize(int compactBuffSize) {
        this.compactBuffSize = compactBuffSize;
        return this;
    }

    public Context<?> setUseDirectBuffer(boolean useDirectBuffer) {
        this.useDirectBuffer = useDirectBuffer;
        return this;
    }

    public Context<?> setWriteWarnLimit(int writeWarnLimit) {
        this.writeWarnLimit = writeWarnLimit;
        return this;
    }

    public Context<?> setUseEhcache(boolean useEhcache) {
        this.useEhcache = useEhcache;
        return this;
    }

    public boolean isUseEhcache() {
        return useEhcache;
    }

    public EhCacheConfig getEhCacheConfig() {
        return ehCacheConfig;
    }

    public Context<?> setEhCacheConfig(EhCacheConfig ehCacheConfig) {
        this.ehCacheConfig = ehCacheConfig;
        return this;
    }

    public abstract boolean start();

    public abstract void close(boolean force);

    public void close() {
        this.close(false);
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    protected void startInfo() {
        logger.info("host:{} port:{} ", host, port);
        logger.info("protocol:{} ", protocolList.size());
        logger.info("processor:{} ", processor.getClass());
        logger.info("sessionListener:{} ", sessionListener.getClass());
        logger.info("readBuffSize:{} ", readBuffSize);
        logger.info("useCompactQueue:{},compactBuffSize:{}", useCompactQueue, compactBuffSize);
        logger.info(this.banner);
    }
}
