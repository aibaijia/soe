package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

import java.nio.ByteBuffer;

public interface SessionWriteErrorListener {

    void run(Session session, ByteBuffer byteBuffer, Throwable throwable);

}
