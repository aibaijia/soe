package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

public interface SessionCreateListener {

    void run(Session session);

}
