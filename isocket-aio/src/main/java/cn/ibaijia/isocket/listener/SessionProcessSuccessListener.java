package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

public interface SessionProcessSuccessListener {

    void run(Session session, Object entity);

}
