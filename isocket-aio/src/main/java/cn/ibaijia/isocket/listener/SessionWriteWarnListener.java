package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

public interface SessionWriteWarnListener {

    boolean run(Session session, int cacheSize);

}
