package cn.ibaijia.isocket.handler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.ibaijia.isocket.Context;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;

public class AcceptCompletionHandler implements CompletionHandler<AsynchronousSocketChannel, AsynchronousServerSocketChannel> {
    private static final Logger logger = LoggerFactory.getLogger(AcceptCompletionHandler.class);

    private Context context;

    public AcceptCompletionHandler(Context context) {
        this.context = context;
    }

    @Override
    public void completed(final AsynchronousSocketChannel channel, AsynchronousServerSocketChannel serverSocketChannel) {
        serverSocketChannel.accept(serverSocketChannel, this);
        createSession(channel);
    }

    @Override
    public void failed(Throwable throwable, AsynchronousServerSocketChannel serverSocketChannel) {
        logger.error("aio server accept fail", throwable);
    }

    private void createSession(AsynchronousSocketChannel channel) {
        String remote = getRemoteAddress(channel);
        logger.trace("aio server createSession:{}", remote);
        if (remote == null) {
            logger.error("can't get remote address, abort.");
            return;
        }
        //check blacklist
        if (isBlackList(channel, context)) {
            return;
        }
        Session session = new Session(channel, context);
        SessionManager.put(session);
    }

    private boolean isBlackList(AsynchronousSocketChannel channel, Context context) {
        Set<String> blackList = context.getBlackList();
        if (blackList == null || blackList.isEmpty()) {
            return false;
        }
        try {
            String remoteIp = ((InetSocketAddress) channel.getRemoteAddress()).getHostName();
            boolean res = blackList.contains(remoteIp);
            logger.trace("remoteIp:{} is in blackList:{}", remoteIp, res);
            return res;
        } catch (IOException e) {
            logger.error("check isBlackList error.", e);
            return false;
        }

    }

    private String getRemoteAddress(AsynchronousSocketChannel channel) {
        try {
            return channel.getRemoteAddress().toString();
        } catch (Exception e) {
            logger.error("getRemoteAddress error!", e);
            return null;
        }
    }

}
