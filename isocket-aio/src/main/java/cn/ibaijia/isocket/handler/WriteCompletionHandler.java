package cn.ibaijia.isocket.handler;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

public class WriteCompletionHandler implements CompletionHandler<Integer, ByteBuffer> {
    private static final Logger logger = LoggerFactory.getLogger(WriteCompletionHandler.class);

    private Session session;

    public WriteCompletionHandler(Session session) {
        this.session = session;
    }

    /**
     * buffer 写完了？
     *
     * @param result
     */
    @Override
    public void completed(final Integer result, final ByteBuffer byteBuffer) {
        logger.trace("write completed:{}", result);
        session.getContext().getSessionListener().writeComplete(session, result);
        session.releaseAndWriteNext();
    }

    @Override
    public void failed(Throwable throwable, final ByteBuffer byteBuffer) {
        logger.error("aio write fail:", throwable);
        try {
            session.getContext().getSessionListener().writeError(session, byteBuffer, throwable);
            SessionManager.close(session);
        } catch (Exception e) {
            logger.trace("failed process error!", e);
        }
    }
}