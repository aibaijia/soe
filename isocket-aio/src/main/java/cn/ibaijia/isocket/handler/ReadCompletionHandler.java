package cn.ibaijia.isocket.handler;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;

public class ReadCompletionHandler implements CompletionHandler<Integer, ByteBuffer> {
    private static final Logger logger = LoggerFactory.getLogger(ReadCompletionHandler.class);

    private Session session;

    public ReadCompletionHandler(Session session) {
        this.session = session;
    }

    @Override
    public void completed(Integer readLength, ByteBuffer byteBuffer) {
        logger.trace("read length:{}", readLength);
        session.getContext().getSessionListener().readComplete(session, readLength);
        if (readLength == -1) {
            logger.trace("session:{} read complete.length:{}", session.getSessionID(), readLength);
            SessionManager.close(session);
        } else {
            session.processReadBuffer();
            session.readNext();
        }
    }

    @Override
    public void failed(Throwable throwable, ByteBuffer byteBuffer) {
        logger.error("aio read fail:", throwable);
        try {
            session.getContext().getSessionListener().readError(session, byteBuffer, throwable);
            SessionManager.close(session);
        } catch (Exception e) {
            logger.error("failed process error!", e);
        }
    }
}