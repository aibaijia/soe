package cn.ibaijia.isocket;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.handler.AcceptCompletionHandler;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.isocket.protocol.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketOption;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class Server extends Context {
    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    private Map<SocketOption<Object>, Object> options = new HashMap<SocketOption<Object>, Object>();
    private int backlog = 1000;

    private AsynchronousServerSocketChannel asynchronousServerSocketChannel = null;
    private AsynchronousChannelGroup asynchronousChannelGroup;

    public Server(String host, int port, Protocol protocol, Processor processor) {
        this.host = host;
        this.port = port;
        this.addProtocol(protocol);
        this.processor = processor;
    }

    public Server(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * 启动
     */
    public boolean start() {
        if (this.protocolList.isEmpty() || this.processor == null) {
            logger.error("protocol or processor can't be empty");
            return false;
        }
        startInfo();
        logger.info("start server host:{},port:{},threads:{}", this.host, this.port, threadNumber);
        try {
            asynchronousChannelGroup = AsynchronousChannelGroup.withFixedThreadPool(threadNumber, new ThreadFactory() {
                byte index = 0;

                @Override
                public Thread newThread(Runnable runnable) {
                    return new Thread(runnable, "is-aio-s" + (++index));
                }
            });
            this.asynchronousServerSocketChannel = AsynchronousServerSocketChannel.open(asynchronousChannelGroup);
            //set isocket options
            if (!options.isEmpty()) {
                for (Map.Entry<SocketOption<Object>, Object> entry : options.entrySet()) {
                    this.asynchronousServerSocketChannel.setOption(entry.getKey(), entry.getValue());
                }
            }
            //bind host
            if (host != null) {
                asynchronousServerSocketChannel.bind(new InetSocketAddress(host, port), backlog);
            } else {
                asynchronousServerSocketChannel.bind(new InetSocketAddress(port), backlog);
            }
            asynchronousServerSocketChannel.accept(asynchronousServerSocketChannel, new AcceptCompletionHandler(this));
            return true;
        } catch (IOException e) {
            logger.error("server start error!", e);
            close();
            return false;
        }
    }

    /**
     * 关闭
     */
    public void close(boolean force) {
        this.closeStatus = CLOSING;
        try {
            if (asynchronousServerSocketChannel != null) {
                asynchronousServerSocketChannel.close();
                asynchronousServerSocketChannel = null;
            }
        } catch (IOException e) {
            logger.error("asynchronousServerSocketChannel close error!", e);
        }
        if (!asynchronousChannelGroup.isTerminated()) {
            try {
                asynchronousChannelGroup.shutdownNow();
            } catch (IOException e) {
                logger.error("asynchronousChannelGroup close error!", e);
            }
        }
        try {
            asynchronousChannelGroup.awaitTermination(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.error("asynchronousChannelGroup awaitTermination error!", e);
        }
        this.closeStatus = CLOSED;
    }

    /**
     * @param backlog accept queue length
     */
    public Server setBacklog(int backlog) {
        if (backlog > 0) {
            this.backlog = backlog;
        }
        return this;
    }

}




