package cn.ibaijia.isocket;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.protocol.Protocol;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.net.SocketOption;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadFactory;

public class Client extends Context {
    private static final Logger logger = LoggerFactory.getLogger(Client.class);
    private Map<SocketOption<Object>, Object> options = new HashMap<SocketOption<Object>, Object>();
    private Session session;

    private AsynchronousChannelGroup asynchronousChannelGroup;

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Client(String host, int port, Protocol protocol, Processor processor) {
        this.host = host;
        this.port = port;
        this.addProtocol(protocol);
        this.processor = processor;
    }

    public boolean start() {
        if (this.protocolList.isEmpty() || this.processor == null) {
            logger.error("protocol or processor can't be empty");
            return false;
        }
        startInfo();
        try {
            this.asynchronousChannelGroup = AsynchronousChannelGroup.withFixedThreadPool(threadNumber, new ThreadFactory() {
                byte index = 0;

                @Override
                public Thread newThread(Runnable runnable) {
                    return new Thread(runnable, "is-aio-c" + (++index));
                }
            });
            AsynchronousSocketChannel socketChannel = AsynchronousSocketChannel.open(asynchronousChannelGroup);
            //set isocket options
            if (!options.isEmpty()) {
                for (Map.Entry<SocketOption<Object>, Object> entry : options.entrySet()) {
                    socketChannel.setOption(entry.getKey(), entry.getValue());
                }
            }
            //bind host
            socketChannel.connect(new InetSocketAddress(this.host, this.port)).get();
            //连接成功则构造AIOSession对象
            session = new Session(socketChannel, this);
            SessionManager.put(session);
            return true;
        } catch (Exception e) {
            logger.error("", e);
            close(true);
            return false;
        }
    }

    public void close(boolean force) {
        this.closeStatus = CLOSING;
        if (session != null) {
            SessionManager.close(session);
            session = null;
        }
        if (asynchronousChannelGroup != null) {
            asynchronousChannelGroup.shutdown();
        }
        this.closeStatus = CLOSED;
    }

    public Session getSession() {
        return session;
    }

    public boolean isOpen() {
        if (session != null && session.isOpen() && session.getChannel() != null && session.getChannel().isOpen()) {
            return true;
        }
        return false;
    }
}
