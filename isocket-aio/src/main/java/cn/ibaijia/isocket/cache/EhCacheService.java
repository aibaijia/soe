package cn.ibaijia.isocket.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class EhCacheService {
    private CacheManager manager = null;
    private Cache cache = null;
    private static EhCacheService ehCacheService = null;
    private EhCacheConfig ehCacheConfig;

    private EhCacheService(EhCacheConfig ehCacheConfig) {
        manager = CacheManager.create();
        this.ehCacheConfig = ehCacheConfig;
        cache = getCache(this.ehCacheConfig.getCacheName());
    }

    public synchronized static EhCacheService getInstance(EhCacheConfig ehCacheConfig) {
        if (ehCacheService == null) {
            ehCacheService = new EhCacheService(ehCacheConfig);
        }
        return ehCacheService;
    }

    private Cache getCache(String cacheName) {
        //在内存管理器中添加缓存实例
        if (!manager.cacheExists(cacheName)) {
            cache = new Cache(cacheName, ehCacheConfig.getMaxElementsInMemory(), ehCacheConfig.isOverflowToDisk(), ehCacheConfig.isEternal(), ehCacheConfig.getTimeToLiveSeconds(), ehCacheConfig.getTimeToIdleSeconds());
            manager.addCache(cache);
        }
        return manager.getCache(cacheName);
    }

    public void put(Element element) {
        cache.put(element);
    }

    public Element getKey(String key) {
        return cache.get(key);
    }

}