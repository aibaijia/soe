## SOE(Simple Object Exchange)
    作为（中小型）服务者和消息者模式的另一种选择:provider<-->router<-->consumer
    优点：
    1 一定的分布式处理能力，同一类型的proivder可以开多个，router自动均衡请求到provider。
    2 router置于公网，provider和consumer可以在不同网络（即使一个国外，一个国内）!
    3 服务调用不需要暴露IP和端口，只需要知道应用名字即可。
    4 降低成本
    弱点：
    1 效率不如直接来得快，中间多了一层router,如果都在内网影响不是很大。
    2 存在一个中心（router）。TODO 后期计划提供主备切换功能。
    注：大量消息或大文件不建议直接使用。使用前请合理评估。
### soe-client:相对于router的客户端，
    其可以扮演两种角色分别是Provider(服务提供者)和Consumer(服务消费者)。
    Consumer调用Provider的服务中间是通过了Router的。
    SoeClient是Consumer或者还是Provider取决于业务是否对外提供服务接口
### soe-router:消息交换器。
    为什么不直接就是提供者和消费者，而中间有这个存在？
    这个是SOE区别于其它provider<-->consumer,SOE的特点.
    SOE 只是为了多一种选择，使用provider<-->consumer + VPN也可以实现在不同网络中服务。
### soe-console:控制台
    包括应用注册中心，应用统计，黑名单管理等。需要手动部署到tomcat

### soe-demo: soe-router,soe-client 示例
    

### isocket-aio:一个基于Java AIO的框架，可单独使用。思路借鉴Smart-Socket。
    使用都只需要自己实现一个或者多个Protocol（即实现多层协议）和Processor接口就可以。
    普通用户只需要关注，如果去实现Protocol中传输对象的编码（encode）和解码（decode）以及 Processor中的业务处理（process）方法即可。
### isocket-nio:基于Java NIO实现，与isocket-aio用法一样，兼容一些不支持Java AIO的平台。
    sokect : thread 为 n:m ,且 n>=m
    
### isocket-aio-demo: isocket-aio 示例

### SOE PROTOCOL
    _Length|FromId(2)|FromSn(1)|ToId(2)|ToSn(1)|ServiceId(2)|Type(1)|MessageId(4)|Body(n)|_Limiter
    说明：
    _Length,FixLengthBigBufferProtocol 协议头
    FromId,2个字节short类型，来源AppId
    FromSn,1个字节byte类型，来源序号，一个ID启动多份时序列号SN不一样。
    ToId,2个字节short类型，目的AppId
    ToSn,1个字节byte类型，目的序号，一个ID启动多份时序列号SN不一样。不指定ToSn或者ToSn=0时，随机发送到ToId应用
    ServiceId，2字节short类型，目标应用中已注册的服务ID
    Type,1字节byte类型，告诉SoeClient消息的类型 
    MessageId,4字节int类型,一定时间内，标识一条消息的唯一性，特别是阻塞等待消息返回的情况下。
    Body,变长N
    _Limiter,FixLengthBigBufferProtocol 协议尾
    
    

### SOE CLIENT PROTOCOLS
    1 LOGIN:
    client                    router               console
      |<---LOGIN_HELLO(-1)------|
      |----LOGIN_REQ(-2)------->|
      |<---LOGIN_RESP(-3)-------||-----LOGIN_EVENT---->|

    2 LOGOUT:
    client                    router               console
      |----LOGOUT_REQ(-4)------->|
      |<---LOGOUT_RESP(-5)-------||----LOGOUT_EVENT---->|
      
    3 RELOAD: reload config from console
    client                    router               console
      |----RELOAD_REQ(-6)------->|
      |--------------------------||----RELOAD_EVENT---->|
      |<---RELOAD_RESP(-7)-------|

    3 HEARTBEAT: 
    client                    router               console
      |---HEARTBEAT_REQ(-8)----->|
      |<--HEARTBEAT_RESP(-9)-----||--HEARTBEAT_EVENT--->|

    4 TOID_NOT_FOUND: 
    client                    router               console
      |<--TOID_NOT_FOUND(-101)---|

    5 TOID_NOT_AUTH: 
    client                    router               console
      |<--TOID_NOT_FOUND(-102)---|

### SOE CONSOLE PROTOCOLS

    1 PUTAPPS: Console 上传Apps信息（appId,appKey,appAuthInfo），定时调用或者手动调用
    console                    router               client
      |---PUTAPPS_REQ(-50)----->|
      |<--PUTAPPS_RESP(-51)-----||--HEARTBEAT_EVENT--->|
      
    2 PUTAPP: 
    console                    router               client
      |----PUTAPP_REQ(-52)----->|
      |<---PUTAPP_RESP(-53)-----||--HEARTBEAT_EVENT--->|
      
    3 GETAPPS: //Console 获取Router中Apps信息（状态，收发条数，数据大小），定时调用或者手动调用
    console                    router               client
      |----GETAPPS_REQ(-54)----->|
      |<---GETAPPS_RESP(-55)-----||--HEARTBEAT_EVENT--->|
      
    4 GETAPP: 
    console                    router               client
      |----GETAPP_REQ(-56)------>|
      |<---GETAPP_RESP(-57)------||--HEARTBEAT_EVENT--->|
    
    5 PUTBLACKLIST: 
    console                    router               client
      |--PUTBLACKLIST_REQ(-58)--->|
      |<-PUTBLACKLIST_RESP(-59)---||--HEARTBEAT_EVENT--->|
    
    6 SERVICE_REGSITER: // LOGIN_EVENT Router 发送服务注册 到console
    console                    router               client
      |<--SERVICE_REGSITER(1001)--|
    
    7 SERVICE_STATISTIC: // Router 发送流程统计 到console
    console                    router               client
      |<--SERVICE_REGSITER(1002)--|
    
    8 SERVICE_HEARTBEAT: // Router 发送心跳记录 到console
    console                    router               client
      |<--SERVICE_HEARTBEAT(1003)--|
    