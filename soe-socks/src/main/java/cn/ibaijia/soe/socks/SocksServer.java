package cn.ibaijia.soe.socks;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.soe.socks.listener.SourceSoeConsumerListener;
import cn.ibaijia.soe.socks.processor.SourceSoeConsumerProcessor;

public class SocksServer {

    public static void main(String[] args) {
        //启动consumer
        ConsumerStarter.start();

//        java.security.Security.setProperty("networkaddress.cache.ttl", "86400");
        Server server = new Server("127.0.0.1", 9980);
        server.addProtocol(new ByteBufferProtocol());
        server.setProcessor(new SourceSoeConsumerProcessor());
        server.setSessionListener(new SourceSoeConsumerListener());
        server.setThreadNumber(30);
//        server.showStatus(5000);
        server.start();

    }


}