package cn.ibaijia.soe.socks.utils;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.socks.callback.SocksListenerCallback;

import java.util.HashMap;
import java.util.Map;

public class ScoksListenerCallbackUtil {

    private static Map<Integer, SocksListenerCallback> socksListenerCallbackMap = new HashMap<>();


    public static SocksListenerCallback get(Integer msgId, Session session) {
        SocksListenerCallback socksListenerCallback = socksListenerCallbackMap.get(msgId);
        if (socksListenerCallback == null) {
            socksListenerCallback = new SocksListenerCallback();
            socksListenerCallbackMap.put(msgId, socksListenerCallback);
        }
        socksListenerCallback.setSession(session);
        return socksListenerCallback;
    }

    public static void remove(Integer msgId) {
        socksListenerCallbackMap.remove(msgId);
    }

}
