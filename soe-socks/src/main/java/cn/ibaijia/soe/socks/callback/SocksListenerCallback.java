package cn.ibaijia.soe.socks.callback;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.client.callback.SoeListenerCallBack;
import cn.ibaijia.soe.client.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class SocksListenerCallback extends SoeListenerCallBack {

    private static final Logger logger = LoggerFactory.getLogger(SocksListenerCallback.class);

    private Session session;

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    protected boolean recv(SoeObject soeObject) {
        logger.info("recv:{}",soeObject.getBody().length);
        // TODO CHECK
        // 返回给 source session
        session.write(ByteBuffer.wrap(soeObject.getBody()));
        return true;
    }

}
