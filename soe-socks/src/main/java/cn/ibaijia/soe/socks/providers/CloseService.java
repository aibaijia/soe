package cn.ibaijia.soe.socks.providers;


import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.socks.cache.ClientCache;
import cn.ibaijia.soe.socks.listener.SoeProviderProxyListener;
import cn.ibaijia.soe.socks.processor.SoeProviderProxyProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * 关掉代理
 *
 */
public class CloseService extends BaseService {
    private static Logger log = LoggerFactory.getLogger(CloseService.class);

    public CloseService(short id, String name) {
        super(id, name);
    }

    @Override
    public void doService(SoeObject soeObject) {
        String key = soeObject.getFromId() + "-" + soeObject.getMsgId();
        Client proxyClient = ClientCache.clientMap.get(key);
        ClientCache.clientMap.remove(key);
        if (proxyClient != null) {
            proxyClient.close();
        }
    }
}
