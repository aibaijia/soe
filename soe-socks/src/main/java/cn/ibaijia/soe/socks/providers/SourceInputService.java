package cn.ibaijia.soe.socks.providers;


import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.socks.cache.ClientCache;
import cn.ibaijia.soe.socks.listener.SoeProviderProxyListener;
import cn.ibaijia.soe.socks.processor.SoeProviderProxyProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

/**
 */
public class SourceInputService extends BaseService {
    private static Logger log = LoggerFactory.getLogger(SourceInputService.class);

    public SourceInputService(short id, String name) {
        super(id, name);
    }

    @Override
    public void doService(SoeObject dto) {
        logger.info("sourceInput:{}", dto.getBody().length);
        String key = dto.getFromId() + "-" + dto.getMsgId();
        Client proxyClient = ClientCache.clientMap.get(key);
        if (proxyClient != null) {
            proxyClient.getSession().write(ByteBuffer.wrap(dto.getBody()));
        } else {
            logger.info("proxyClient not found,key:{}", key);
        }
    }
}
