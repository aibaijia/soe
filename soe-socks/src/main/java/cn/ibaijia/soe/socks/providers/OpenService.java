package cn.ibaijia.soe.socks.providers;


import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.socks.cache.ClientCache;
import cn.ibaijia.soe.socks.listener.SoeProviderProxyListener;
import cn.ibaijia.soe.socks.processor.SoeProviderProxyProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 根据 host port 创建proxyClient ，msgId代表一次请求
 *
 */
public class OpenService extends BaseService {
    private static Logger log = LoggerFactory.getLogger(OpenService.class);

    public OpenService(short id, String name) {
        super(id, name);
    }

    @Override
    public void doService(SoeObject dto) {
        String msg = dto.getBodyAsString();//host;port;token
        logger.info(msg);
        String[] arr = msg.split(";");
        String host = arr[0];
        Integer port = Integer.valueOf(arr[1]);

        Client proxyClient = new Client(host, port);
        proxyClient.addProtocol(new ByteBufferProtocol());
        proxyClient.setProcessor(new SoeProviderProxyProcessor(this));
        proxyClient.setSessionListener(new SoeProviderProxyListener(this));
//        proxyClient.showStatus();
        boolean res = proxyClient.start();
        if (res) {
            String key = dto.getFromId() + "-" + dto.getMsgId();
            ClientCache.clientMap.put(key, proxyClient);
            proxyClient.getSession().setAttribute("dto", dto);
        }
        logger.info("create proxyClient:{}", res);
        SoeObject respDto = dto.makeResponse(res ? "1001" : "1002");
        response(respDto);
    }
}
