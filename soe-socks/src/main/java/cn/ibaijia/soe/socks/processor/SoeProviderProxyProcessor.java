package cn.ibaijia.soe.socks.processor;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class SoeProviderProxyProcessor implements Processor<ByteBuffer> {

    private static final Logger logger = LoggerFactory.getLogger(SoeProviderProxyProcessor.class);

    private BaseService baseService;

    public SoeProviderProxyProcessor(BaseService baseService) {
        this.baseService = baseService;
    }

    @Override
    public boolean process(Session session, ByteBuffer msg) {
//        logger.debug("recv:{}", JSONObject.toJSONString(msg.array()));
        SoeObject dto = (SoeObject) session.getAttribute("dto");
        if (dto == null) {
            logger.error("dto null.");
        } else {
            SoeObject respDto = dto.makeResponse(msg);
            baseService.response(respDto);
        }
        return true;
    }

}
