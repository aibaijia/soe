package cn.ibaijia.soe.socks.listener;

import cn.ibaijia.isocket.listener.DefaultSessionListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.socks.ConsumerStarter;
import cn.ibaijia.soe.socks.utils.ScoksListenerCallbackUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class SourceSoeConsumerListener extends DefaultSessionListener {
    private static final Logger logger = LoggerFactory.getLogger(SourceSoeConsumerListener.class);

    @Override
    public void readError(Session session, ByteBuffer byteBuffer, Throwable e) {
        logger.error("readError:{}", session.getSessionID(), e.getCause());
//        closed(session);
    }

    @Override
    public void closed(Session session) {
        try {
            Integer msgId = (Integer) session.getAttribute("msgId");
            if (msgId != null) {
                ScoksListenerCallbackUtil.remove(msgId);
                ConsumerStarter.close(msgId);
                logger.debug("SourceSoeConsumerListener close msgId." + msgId);
            }
        } catch (Exception e) {
            logger.error("SourceSoeConsumerListener close proxyClient error.", e);
        }
    }
}
