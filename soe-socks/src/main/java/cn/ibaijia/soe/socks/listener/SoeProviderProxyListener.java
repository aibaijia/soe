package cn.ibaijia.soe.socks.listener;

import cn.ibaijia.isocket.listener.DefaultSessionListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class SoeProviderProxyListener extends DefaultSessionListener {
    private static final Logger logger = LoggerFactory.getLogger(SoeProviderProxyListener.class);

    private BaseService baseService;

    public SoeProviderProxyListener(BaseService baseService) {
        this.baseService = baseService;
    }

    @Override
    public void readError(Session session, ByteBuffer byteBuffer, Throwable e) {
        logger.error("readError:{}", session.getSessionID(), e.getCause());
    }

    @Override
    public void closed(Session session) {
        super.closed(session);
//        try {
//            SoeObject dto = (SoeObject) session.getAttribute("dto");
//            if (dto == null) {
//                logger.error("dto null.");
//            } else {
//                SoeObject respDto = dto.makeResponse("-1");
//                baseService.response(respDto);
//            }
//        } catch (Exception e) {
//            logger.error("ProxyClientListener close sourceSession error.", e);
//        }
    }
}
