package cn.ibaijia.soe.socks;

import cn.ibaijia.soe.client.Consts;
import cn.ibaijia.soe.client.SoeClient;
import cn.ibaijia.soe.client.callback.SoeListenerCallBack;
import cn.ibaijia.soe.client.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class ConsumerStarter {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerStarter.class);

    private static SoeClient soeClient;
    private static short toId = 2;
    private static short openFuncId = 2001;
    private static short inputFuncId = 2002;
    private static short closeFuncId = 2003;

    public static void start() {
        String host = "121.42.145.67";
        int port = 7004;
        Short appId = 3;
        String appKey = "111111";
        soeClient = new SoeClient(host, port, appId, appKey);
        soeClient.setThreadNumber(30);
        soeClient.start();
        //启动失败 退出程序 TODO
    }

    public static SoeObject sendSync(int msgId, byte[] data) {
        //检查 soeClient状态 TODO
        return soeClient.sendSync(toId, inputFuncId, msgId, data);
    }

    public static void sendAndListenOnMsgId(int msgId, ByteBuffer data, SoeListenerCallBack soeCallBack) {
        //检查 soeClient状态 TODO
        byte toSn = (byte) 0;
        SoeObject soeObject = new SoeObject(toId, toSn, inputFuncId, Consts.MESSAGE_TYPE.BYTE.code(), msgId, data);
        soeClient.sendAndListenOnMsgId(soeObject,soeCallBack);
    }


    public static SoeObject open(int msgId, byte[] data) {
        return soeClient.sendSync(toId, openFuncId, msgId, data);
    }


    public static void close(int msgId) {
        byte toSn = (byte) 0;
        SoeObject soeObject = new SoeObject(toId, toSn, closeFuncId, Consts.MESSAGE_TYPE.BYTE.code(), msgId, "");
        soeClient.send(soeObject);
    }


}
