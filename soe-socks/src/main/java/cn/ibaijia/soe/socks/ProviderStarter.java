package cn.ibaijia.soe.socks;

import cn.ibaijia.soe.client.SoeClient;
import cn.ibaijia.soe.socks.providers.CloseService;
import cn.ibaijia.soe.socks.providers.OpenService;
import cn.ibaijia.soe.socks.providers.SourceInputService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProviderStarter {
    private static final Logger logger = LoggerFactory.getLogger(ProviderStarter.class);

    public static void main(String[] args) {
        String host = "118.24.114.130";
        int port = 7004;
        Short appId = 2;
        String appKey = "111111";
        if(args.length > 0){
            host = args[0];
        }
        if(args.length > 1){
            port = Integer.valueOf(args[1]);
        }
        if(args.length > 2){
            appId = Short.valueOf(args[2]);
        }
        if(args.length > 3){
            appKey = args[3];
        }
        SoeClient soeClient = new SoeClient(host,port,appId,appKey);
        soeClient.setThreadNumber(30);
        soeClient.addService(new OpenService((short)2001,"open"));
        soeClient.addService(new SourceInputService((short)2002,"sourceInput"));
        soeClient.addService(new CloseService((short)2003,"close"));
        soeClient.start();
    }

}
