package cn.ibaijia.isocket;

import cn.ibaijia.isocket.handler.Handler;
import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.protocol.Protocol;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;

import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.concurrent.CountDownLatch;

public class Client extends Context {

    private Session session;
    private SocketChannel socketChannel = null;
    private Handler handler;

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public Client(String host, int port, Protocol protocol, Processor processor) {
        this.host = host;
        this.port = port;
        this.addProtocol(protocol);
        this.processor = processor;
    }

    @Override
    public boolean start() {
        try {
            if (this.protocolList.isEmpty() || this.processor == null) {
                logger.error("protocol or processor can't be empty");
                return false;
            }
            closeStatus = NOT_CLOSE;
            startInfo();
            socketChannel = SocketChannel.open();
            //bind host
            socketChannel.connect(new InetSocketAddress(this.host, this.port));
            CountDownLatch countDownLatch = new CountDownLatch(1);
            handler = new Handler(0, countDownLatch);
            Thread thread = new Thread(handler);
            thread.setName("is-nio-p");
            thread.start();
            countDownLatch.await();
            if (socketChannel.isConnectionPending()) {
                socketChannel.finishConnect();
            }
            socketChannel.configureBlocking(false);
            //连接成功则构造Session对象
            session = new Session(socketChannel, this, handler);
            getSessionListener().create(session);
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }

    @Override
    public void close(boolean force) {
        if (force) {
            if (session != null) {
                SessionManager.close(session);
                session = null;
            }
            this.closeStatus = CLOSED;
        } else {
            this.closeStatus = CLOSING;
        }
    }

    public Session getSession() {
        return session;
    }

    public boolean isOpen() {
        if (session != null && session.isOpen() && session.getChannel() != null && session.getChannel().isConnected()) {
            return true;
        }
        return false;
    }
}
