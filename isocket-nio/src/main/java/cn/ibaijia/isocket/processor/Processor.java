package cn.ibaijia.isocket.processor;

import cn.ibaijia.isocket.session.Session;

public interface Processor<T> {

    /**
     * 处理接收到的消息
     * @param session
     * @param object  解码得到或者上一个processor的输出的对象
     * @return 是否处理成功
     */
    boolean process(Session session, T object);

    /**
     * process 如果 不内部处理异常，会到这里
     * @param session
     * @param object  解码得到或者上一个processor的输出的对象
     */
    void processError(Session session, T object, Throwable throwable);

}
