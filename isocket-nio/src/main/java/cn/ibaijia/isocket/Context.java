package cn.ibaijia.isocket;

import cn.ibaijia.isocket.listener.*;
import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.protocol.Protocol;
import cn.ibaijia.isocket.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadPoolExecutor;

public abstract class Context {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static int NOT_CLOSE = 0;
    protected static int CLOSING = 1;
    protected static int CLOSED = 2;
    protected int closeStatus = NOT_CLOSE;// 0 normal 1 closing 2 closed

    protected String host;
    protected int port;
    protected String banner = "\n" +
            "  _                _        _   \n" +
            " (_)___  ___   ___| | _____| |_ \n" +
            " | / __|/ _ \\ / __| |/ / _ \\ __|\n" +
            " | \\__ \\ (_) | (__|   <  __/ |_ \n" +
            " |_|___/\\___/ \\___|_|\\_\\___|\\__|\n" +
            "                                \n";
    /**
     * 协议编码解码器
     */
    protected List<Protocol> protocolList = new ArrayList<Protocol>();
    /**
     * 业务数据处理器
     */
    protected Processor processor;
    /**
     * Session监听器
     */
    protected SessionListener sessionListener = new DefaultSessionListener();

    /**
     * 是否使用压缩缓冲区
     */
    protected boolean useCompactQueue = false;
    /**
     * 压缩缓冲区大小
     */
    protected int compactBuffSize = 4 * 1024;
    /**
     * 写缓冲区预警数
     */
    protected int writeWarnLimit = 1000;

    protected Set<String> blackList;

    /**
     * 是否使用EhCache
     */
    protected boolean useEhcache = false;

    protected int MIN_THREAD_NUMBER = 2;
    protected int threadNumber = Util.availableProcessors() < MIN_THREAD_NUMBER ? MIN_THREAD_NUMBER : Util.availableProcessors();

    protected int readThreadNumber = threadNumber;
    protected int writeThreadNumber = 0;

    protected ThreadPoolExecutor readPool = null;
    protected ThreadPoolExecutor writePool = null;

    public List<Protocol> getProtocolList() {
        return protocolList;
    }

    public Context addProtocol(Protocol protocol) {
        this.protocolList.add(protocol);
        return this;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public Processor getProcessor() {
        return processor;
    }

    public SessionListener getSessionListener() {
        return sessionListener;
    }

    public void setSessionListener(SessionListener sessionListener) {
        this.sessionListener = sessionListener;
    }

    public Context setSessionCreateListener(SessionCreateListener listener) {
        this.sessionListener.setSessionCreateListener(listener);
        return this;
    }

    public Context setSessionReadCompleteListener(SessionReadCompleteListener listener) {
        this.sessionListener.setSessionReadCompleteListener(listener);
        return this;
    }

    public Context setSessionReadErrorListener(SessionReadErrorListener listener) {
        this.sessionListener.setSessionReadErrorListener(listener);
        return this;
    }

    public Context setSessionProcessErrorListener(SessionProcessErrorListener listener) {
        this.sessionListener.setSessionProcessErrorListener(listener);
        return this;
    }

    public Context setSessionWriteCompleteListener(SessionWriteCompleteListener listener) {
        this.sessionListener.setSessionWriteCompleteListener(listener);
        return this;
    }

    public Context setSessionWriteErrorListener(SessionWriteErrorListener listener) {
        this.sessionListener.setSessionWriteErrorListener(listener);
        return this;
    }

    public Context setSessionLimitWarnListener(SessionWriteWarnListener listener) {
        this.sessionListener.setSessionWriteWarnListener(listener);
        return this;
    }

    public Context setSessionClosedListener(SessionClosedListener listener) {
        this.sessionListener.setSessionClosedListener(listener);
        return this;
    }

    public int getReadBuffSize() {
        return Util.getIntProperty(Consts.PROP_BUFF_SIZE, 4 * 1024);
    }

    public boolean isUseCompactQueue() {
        return useCompactQueue;
    }

    public int getCompactBuffSize() {
        return compactBuffSize;
    }

    public boolean isUseDirectBuffer() {
        return Util.getBooleanProperty(Consts.PROP_USE_DIRECT_BUFF, false);
    }

    public int getWriteWarnLimit() {
        return writeWarnLimit;
    }

    public Set<String> getBlackList() {
        return blackList;
    }

    public void setBlackList(Set<String> blackList) {
        this.blackList = blackList;
    }

    public Context setReadFirst(boolean readFirst) {
        System.setProperty(Consts.PROP_READ_FIRST, readFirst + "");
        return this;
    }

    public Context setPoolPageSize(int poolPageSize) {
        if (poolPageSize > 1) {
            System.setProperty(Consts.PROP_POOL_PAGE_SIZE, poolPageSize + "");
        }
        return this;
    }

    public Context setPoolPage(int poolPage) {
        if (poolPage > 1) {
            System.setProperty(Consts.PROP_POOL_PAGE, poolPage + "");
        }
        return this;
    }

    public Context setUsePool(boolean usePool) {
        System.setProperty(Consts.PROP_USE_POOL, usePool + "");
        return this;
    }

    public Context setBuffSize(int readBuffSize) {
        if (readBuffSize > 4) {
            System.setProperty(Consts.PROP_BUFF_SIZE, readBuffSize + "");
        }
        return this;
    }

    public Context setUseCompactQueue(boolean useCompactQueue) {
        this.useCompactQueue = useCompactQueue;
        return this;
    }

    public Context setCompactBuffSize(int compactBuffSize) {
        this.compactBuffSize = compactBuffSize;
        return this;
    }

    public Context setUseDirectBuffer(boolean useDirectBuffer) {
        System.setProperty(Consts.PROP_USE_DIRECT_BUFF, useDirectBuffer + "");
        return this;
    }

    public Context setWriteWarnLimit(int writeWarnLimit) {
        this.writeWarnLimit = writeWarnLimit;
        return this;
    }

    public Context setUseEhcache(boolean useEhcache) {
        this.useEhcache = useEhcache;
        return this;
    }

    public void setReadThreadNumber(int readThreadNumber) {
        this.readThreadNumber = readThreadNumber;
    }

    public void setWriteThreadNumber(int writeThreadNumber) {
        this.writeThreadNumber = writeThreadNumber;
    }

    public ThreadPoolExecutor getWritePool() {
        return writePool;
    }

    public abstract boolean start();

    public abstract void close(boolean force);

    public void close() {
        this.close(false);
    }

    public void restart() {
        this.close(true);
        this.start();
    }

    protected Selector selector;

    public void setBanner(String banner) {
        this.banner = banner;
    }

    protected void startInfo() {
        logger.info("start on host:{} port:{} ", host, port);
        for (Protocol protocol : protocolList) {
            logger.info("protocol:{} ", protocol.getClass());
        }
        logger.info("processor:{} ", processor != null ? processor.getClass() : null);
        logger.info("sessionListener:{} ", sessionListener.getClass());
        logger.info("useCompactQueue:{},compactBuffSize:{}", useCompactQueue, compactBuffSize);
        logger.info("readFirst:{}", Util.getBooleanProperty(Consts.PROP_READ_FIRST));
        logger.info("useDirectBuff:{}", Util.getBooleanProperty(Consts.PROP_USE_DIRECT_BUFF));
        int page = Util.getIntProperty(Consts.PROP_POOL_PAGE);
        int pageSize = Util.getIntProperty(Consts.PROP_POOL_PAGE_SIZE);
        int buffSize = Util.getIntProperty(Consts.PROP_BUFF_SIZE);
        logger.info("pool page:{} pageSize:{}", page, pageSize);
        logger.info("buffSize:{} memory:{}M", buffSize, page * pageSize * buffSize / 1024 / 1024);
        logger.info(this.banner);
    }

}
