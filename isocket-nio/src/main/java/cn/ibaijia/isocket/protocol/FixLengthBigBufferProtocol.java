package cn.ibaijia.isocket.protocol;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.isocket.util.BufferUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class FixLengthBigBufferProtocol<B extends Buffer> implements Protocol<ByteBuffer, ByteBuffer> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private short HEAD_LENGTH = 5;
    private Byte endFlag = '\n';

    public FixLengthBigBufferProtocol() {

    }

    @Override
    public ByteBuffer decode(ByteBuffer readBuffer, Session session) {
        State state = (State) session.getAttribute("state");
        logger.debug("decode decodeNew:{}", state.decodeNew);
        if (readBuffer.remaining() == 0) {
            logger.debug("readBuffer.remaining() == 0");
            return null;
        }
        //如果新消息
        if (state.decodeNew) {
            if (readBuffer.remaining() < HEAD_LENGTH) {
                logger.debug("remaining length:{} < HEAD_LENGTH:{}", readBuffer.remaining(), HEAD_LENGTH);
                return null;
            }
            BufferUtil.free(state.bigBuffer);
            //判断是否存在半包情况
            int len = readBuffer.getInt() - HEAD_LENGTH;
            state.bigBuffer = BufferUtil.allocate(len, session.getContext().isUseDirectBuffer());
            state.decodeNew = false;
        }
        if (state.bigBuffer.hasRemaining()) {
            if (readBuffer.remaining() <= state.bigBuffer.remaining()) {
                state.bigBuffer.put(readBuffer);
            } else {//多了 少读,-- 只读读满当前buff
                byte[] bytes = new byte[state.bigBuffer.remaining()];
                readBuffer.get(bytes);
                state.bigBuffer.put(bytes);
            }
        }

        if (!state.bigBuffer.hasRemaining() && readBuffer.hasRemaining()) {//bigBuffer 满了,并且 readBuffer 还有
            byte exceptFlag = readBuffer.get();
            if (exceptFlag != endFlag) {//预期结束标志不符合
                logger.error("exceptFlag:{} != endFlag:{}, end flag error,make sure you get the big buffer protocol!", exceptFlag, endFlag);
                SessionManager.close(session);
                return null;
            }
            state.decodeNew = true;
            return state.bigBuffer;
        } else {
            state.decodeNew = false;
        }
        return null;
    }

    @Override
    public ByteBuffer encode(ByteBuffer object, Session session) {
        logger.debug("encode");
        if (object == null) {
            return null;
        }
        int length = HEAD_LENGTH + object.capacity();
        ByteBuffer buffer = BufferUtil.allocate(length, object.isDirect());
        buffer.putInt(length);
        buffer.put(object.array());
        buffer.put(endFlag);
        buffer.flip();
        return buffer;
    }

    private State getState(Session session) {
        String key = "state";
        State state = (State) session.getAttribute(key);
        if (state == null) {
            state = new State();
            session.setAttribute(key, state);
        }
        return state;
    }

}

class State {
    public ByteBuffer bigBuffer;
    public boolean decodeNew = true;
}
