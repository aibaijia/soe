package cn.ibaijia.isocket.protocol;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.isocket.util.BufferUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.Buffer;
import java.nio.ByteBuffer;

/**
 * 每条不能超过ReadBufferSize
 */
public class FixLengthBufferProtocol implements Protocol<ByteBuffer, ByteBuffer> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    protected short HEAD_LENGTH = 5;
    protected Byte endFlag = '\n';

    public FixLengthBufferProtocol() {

    }

    @Override
    public ByteBuffer decode(ByteBuffer readBuffer, Session session) {
        logger.debug("decode");
        if (readBuffer.remaining() < HEAD_LENGTH) {
            logger.debug("remaining length:{} < HEAD_LENGTH:{}", readBuffer.remaining(), HEAD_LENGTH);
            return null;
        }
        //判断是否存在半包情况
        readBuffer.mark();
        int len = readBuffer.getInt() - HEAD_LENGTH;
        if (readBuffer.remaining() < len + 1) {
            logger.debug("remaining length:{} < msgLen:{}", readBuffer.remaining(), len);
            readBuffer.reset();
            return null;
        }
        byte[] bytes = new byte[len];
        readBuffer.get(bytes);
        byte exceptFlag = readBuffer.get();
        if (exceptFlag != endFlag) {//预期结束标志不符合
            logger.error("exceptFlag:{} != endFlag:{}, end flag error,make sure you get the buffer protocol!", exceptFlag, endFlag);
            SessionManager.close(session);
            return null;
        }
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public ByteBuffer encode(ByteBuffer bufferData, Session session) {
        logger.debug("encode");
        int length = HEAD_LENGTH + bufferData.capacity();
        ByteBuffer buffer = BufferUtil.allocate(length, bufferData.isDirect());
//        ByteBuffer buffer = ByteBuffer.allocate(length);
        buffer.putInt(length);
        buffer.put(bufferData.array());
        buffer.put(endFlag);
        buffer.flip();
        return buffer;
    }
}
