package cn.ibaijia.isocket.protocol;

public enum LenType {
    BYTE((short)1),
    SHORT((short)2),
    INT((short)4);
//    LONG((short)8);
    private short len;

    LenType(short len) {
        this.len = len;
    }

    public short len() {
        return this.len;
    }
}
