package cn.ibaijia.isocket.util;

import java.util.concurrent.atomic.AtomicInteger;

public class RingArray<T> {

    private T[] arr;

    private AtomicInteger count = new AtomicInteger(0);

    public RingArray(T[] arr) {
        this.arr = arr;
    }

    public T poll() {
        int idx = count.getAndIncrement();
        return arr[idx % arr.length];
    }

    public static void  main(String[] args){
        AtomicInteger count = new AtomicInteger(Integer.MAX_VALUE);
//        count.getAndDecrement();
//        count.getAndDecrement();
        System.out.println(count.getAndDecrement());
    }
}
