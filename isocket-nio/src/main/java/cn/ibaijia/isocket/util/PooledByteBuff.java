package cn.ibaijia.isocket.util;

import cn.ibaijia.isocket.Consts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

/**
 * @author longzl
 */
public class PooledByteBuff {

    private final Logger logger = LoggerFactory.getLogger(PooledByteBuff.class);
    private int pageSize = Util.getIntProperty(Consts.PROP_POOL_PAGE_SIZE, 1024);
    private int bufferSize = Util.getIntProperty(Consts.PROP_BUFF_SIZE, 4 * 1024);
    private boolean useDirectBuffer = Util.getBooleanProperty(Consts.PROP_USE_DIRECT_BUFF, true);
    private boolean usePool = Util.getBooleanProperty(Consts.PROP_USE_POOL, true);
    private ByteBuffer[][] pool;
    private int poolIdx = 0;
    private int poolCursor = 0;
    private int backIdx = 1;
    private int backCursor = 0;

    private int handlerId;

    public PooledByteBuff(int handlerId) {
        this.handlerId = handlerId;
        pool = new ByteBuffer[2][pageSize];
        for (int i = 0; i < pageSize; i++) {
            pool[0][i] = BufferUtil.allocate(bufferSize, useDirectBuffer);
        }
        logger.info("handlerId:{} pageSize:{} isDirect:{} usePool:{}", handlerId, pageSize, pool[0][0].isDirect(), usePool);
    }

    public ByteBuffer get() {
        try {
            if (poolCursor < pageSize) {
                ByteBuffer buffer = pool[poolIdx][poolCursor];
                if (buffer == null) {
                    logger.info("createNewToUse handlerId:{} poolIdx:{} poolCursor:{} buffer null.", handlerId, poolIdx, poolCursor);
                    return createNewToUse();
                }
                poolCursor++;
                buffer.clear();
                return buffer;
            } else {
                poolCursor = 0;
                poolIdx = poolIdx == 0 ? 1 : 0;
                logger.trace("get handlerId:{} change to poolIdx:{}", handlerId, poolIdx);
                return get();
            }
        } catch (Exception e) {
            logger.error("get buffer error.", e);
            return BufferUtil.allocate(bufferSize, useDirectBuffer);
        }
    }

    private ByteBuffer createNewToUse() {
        ByteBuffer buffer = BufferUtil.allocate(bufferSize, useDirectBuffer);
        return buffer;
    }

    public void release(ByteBuffer byteBuffer) {
        if (backCursor < pageSize) {
            pool[backIdx][backCursor] = byteBuffer;
            backCursor++;
        } else {
            //poolIdx等于backCursor的时候才能 转到不然怕有问题
            if (backIdx == poolIdx) {
                backCursor = 0;
                backIdx = backIdx == 1 ? 0 : 1;
                logger.trace("release handlerId:{} change to backIdx:{}", handlerId, backIdx);
                release(byteBuffer);
            }
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public boolean isUseDirectBuffer() {
        return useDirectBuffer;
    }
}
