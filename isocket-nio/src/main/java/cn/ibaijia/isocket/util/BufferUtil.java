package cn.ibaijia.isocket.util;

import sun.nio.ch.DirectBuffer;

import java.nio.ByteBuffer;
import java.util.List;

public class BufferUtil {

    public static byte[] merge(byte[] arr1, byte[] arr2) {
        byte[] arr = new byte[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, arr, 0, arr1.length);
        System.arraycopy(arr2, 0, arr, arr1.length, arr2.length);
        return arr;
    }

    public static ByteBuffer mergeToBuffer(byte[] arr1, byte[] arr2) {
        byte[] arr = new byte[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, arr, 0, arr1.length);
        System.arraycopy(arr2, 0, arr, arr1.length, arr2.length);
        return ByteBuffer.wrap(arr);
    }

    public static ByteBuffer merge(int length,ByteBuffer... buffers) {
        byte[] arr = new byte[length];
        int destPos = 0;
        for(ByteBuffer buffer:buffers) {
            byte[] srcArr = buffer.array();
            System.arraycopy(srcArr, 0, arr, destPos, srcArr.length);
            destPos += srcArr.length;
        }
        return ByteBuffer.wrap(arr);
    }

    public static ByteBuffer merge(int length,List<ByteBuffer> buffers) {
        byte[] arr = new byte[length];
        int destPos = 0;
        for(ByteBuffer buffer:buffers) {
            byte[] srcArr = buffer.array();
            System.arraycopy(srcArr, 0, arr, destPos, srcArr.length);
            destPos += srcArr.length;
        }
        return ByteBuffer.wrap(arr);
    }

    public static ByteBuffer merge(ByteBuffer... buffers) {
        int length = 0;
        for(ByteBuffer buffer:buffers){
            length += buffer.array().length;
        }
        byte[] arr = new byte[length];
        int destPos = 0;
        for(ByteBuffer buffer:buffers) {
            byte[] srcArr = buffer.array();
            System.arraycopy(srcArr, 0, arr, destPos, srcArr.length);
            destPos += srcArr.length;
        }
        return ByteBuffer.wrap(arr);
    }

    public static ByteBuffer allocate(int size,boolean useDirect) {
        return useDirect ? ByteBuffer.allocateDirect(size) : ByteBuffer.allocate(size);
    }

    public static void free(ByteBuffer byteBuffer) {
        if(byteBuffer != null && byteBuffer.isDirect()){
            ((DirectBuffer) byteBuffer).cleaner().clean();
        }
    }

}
