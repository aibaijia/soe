package cn.ibaijia.isocket.handler;

import cn.ibaijia.isocket.Consts;
import cn.ibaijia.isocket.Context;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.util.PooledByteBuff;
import cn.ibaijia.isocket.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

/**
 * @author longzl
 */
public class Handler implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Handler.class);

    private Selector selector;

    private long threadId;

    private CountDownLatch countDownLatch;

    public int id;

    private PooledByteBuff pooledByteBuff;

    public Handler(int id, CountDownLatch countDownLatch) {
        try {
            this.id = id;
            pooledByteBuff = new PooledByteBuff(id);
            this.countDownLatch = countDownLatch;
            selector = Selector.open();
        } catch (Throwable e) {
            logger.error("init Handler open selector error.", e);
        }
    }

    private Map<Session, Integer> sessionEventMap = new ConcurrentHashMap<Session, Integer>();

    public void readWait(Session session) {
        logger.trace("readWait:{}", session.getSessionID());
        try {
            sessionEventMap.put(session, SelectionKey.OP_READ);
            if (Thread.currentThread().getId() != threadId) {
                logger.trace("readWait wakeup.");
                selector.wakeup();
            }
        } catch (Exception e) {
            logger.trace("readWait error.", e);
        }
    }

    public void writeWait(Session session) {
        logger.trace("writeWait:{}", session.getSessionID());
        sessionEventMap.put(session, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
        if (Thread.currentThread().getId() != threadId) {
            logger.trace("writeWait wakeup.");
            selector.wakeup();
        }
    }

    protected void regEvent() {
        logger.trace("regEvent sessionEventMap size:{}", sessionEventMap.size());
        Iterator<Map.Entry<Session, Integer>> it = sessionEventMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Session, Integer> entry = it.next();
            it.remove();
            Session session = entry.getKey();
            if (session == null || !session.isOpen()) {
                continue;
            }
            try {
                SocketChannel channel = session.getChannel();
                if (channel != null && channel.isOpen()) {
                    logger.trace("regEvent sessionId:{} ops:{}", session.getSessionID(), entry.getValue());
                    channel.register(selector, entry.getValue()).attach(session);
                }
            } catch (Throwable e) {
                logger.error("regEvent error. sessionId:{}", session.getSessionID(), e);
            }
        }
        logger.trace("regEvent sessionEventMap remain size:{}", sessionEventMap.size());
    }


    @Override
    public void run() {
        threadId = Thread.currentThread().getId();
        if (countDownLatch != null) {
            countDownLatch.countDown();
        }
        selectAndProcess();
    }

    private void selectAndProcess() {
        try {
            while (true) {
                logger.trace("select!");
                selector.select();
                Set<SelectionKey> readyKeys = selector.selectedKeys();
                logger.trace("readyKeys size:{}", readyKeys.size());
                Iterator<SelectionKey> it = readyKeys.iterator();
                while (it.hasNext()) {
                    final SelectionKey key = it.next();
                    it.remove();
                    try {
                        if (key.isValid() && key.isReadable()) {
                            read(key);
                        }
                        if (key.isValid() && key.isWritable()) {
                            write(key);
                        }
                    } catch (Throwable e) {
                        logger.error("process key error.", e);
                    }
                }
                logger.trace("selector keys before:{}", selector.keys().size());
                regEvent();
            }
        } catch (Throwable e) {
            logger.error("unknown error!", e);
        }
    }

    private void read(SelectionKey key) {
        logger.trace("isReadable");
        Session session = (Session) key.attachment();
        session.readBuffer();
    }

    private void write(SelectionKey key) {
        logger.trace("isWritable");
        Session session = (Session) key.attachment();
//        sessionEventMap.put(session, SelectionKey.OP_READ);
//        session.writeBufferAsync();
        session.writeBuffer();
    }

    public int getScore() {
        int score = selector.keys().size();
        return score;
    }

    public PooledByteBuff getPooledByteBuff() {
        return pooledByteBuff;
    }

    public long getThreadId() {
        return threadId;
    }
}
