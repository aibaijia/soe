package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

import java.nio.ByteBuffer;

public interface SessionWriteWarnListener {

    boolean run(Session session, int cacheSize);

}
