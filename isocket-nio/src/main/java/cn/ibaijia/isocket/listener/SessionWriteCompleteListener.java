package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

public interface SessionWriteCompleteListener {

    void run(Session session, int writeSize);

}
