package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

import java.nio.ByteBuffer;

public interface SessionClosingListener {

    void run(Session session);

}
