package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

import java.nio.ByteBuffer;

public interface SessionListener {

    /**
     * 建立连接
     *
     * @param session
     */
    void create(Session session);

    /**
     * 读取缓冲区完成
     *
     * @param session
     * @param readSize
     */
    void readComplete(Session session, int readSize);

    /**
     * 读取缓冲区失败
     *
     * @param session
     * @param e
     */
    void readError(Session session, ByteBuffer byteBuffer, Throwable e);

    /**
     * 处理失败
     *
     * @param session
     * @param msg
     * @param e
     */
    void processError(Session session, Object msg, Throwable e);

    /**
     * 写异常
     *
     * @param session
     * @param e
     */
    void writeError(Session session, ByteBuffer byteBuffer, Throwable e);

    /**
     * 写完成，可用于垃圾回收
     *
     * @param session
     * @param writeSize
     */
    void writeComplete(Session session, int writeSize);

    /**
     * 写任务预警，处理
     *
     * @param session
     * @param cacheSize
     */
    boolean writeWarn(Session session, int cacheSize);

    /**
     * 关闭完成（有可能是网络原因）
     *
     * @param session
     */
    void closed(Session session);

    void setSessionCreateListener(SessionCreateListener listener);

    void setSessionReadCompleteListener(SessionReadCompleteListener listener);

    void setSessionReadErrorListener(SessionReadErrorListener listener);

    void setSessionProcessErrorListener(SessionProcessErrorListener listener);

    void setSessionWriteCompleteListener(SessionWriteCompleteListener listener);

    void setSessionWriteErrorListener(SessionWriteErrorListener listener);

    void setSessionWriteWarnListener(SessionWriteWarnListener listener);

    void setSessionClosedListener(SessionClosedListener listener);
}
