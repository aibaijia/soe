package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

public interface SessionProcessErrorListener {

    void run(Session session, Object entity, Throwable throwable);

}
