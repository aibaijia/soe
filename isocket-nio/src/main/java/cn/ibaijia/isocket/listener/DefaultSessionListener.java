package cn.ibaijia.isocket.listener;

import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicInteger;

public class DefaultSessionListener implements SessionListener {
    private static final Logger logger = LoggerFactory.getLogger(DefaultSessionListener.class);

    public DefaultSessionListener() {
    }

//    private AtomicInteger totalReadSize = new AtomicInteger(0);
//    private AtomicInteger totalWriteSize = new AtomicInteger(0);

    private SessionCreateListener sessionCreateListener = new SessionCreateListener() {
        @Override
        public void run(Session session) {

        }
    };
    private SessionReadCompleteListener sessionReadCompleteListener = new SessionReadCompleteListener() {
        @Override
        public void run(Session session, int readSize) {

        }
    };
    private SessionReadErrorListener sessionReadErrorListener = new SessionReadErrorListener() {
        @Override
        public void run(Session session, ByteBuffer byteBuffer, Throwable throwable) {

        }
    };
    private SessionProcessErrorListener sessionProcessErrorListener = new SessionProcessErrorListener() {
        @Override
        public void run(Session session, Object entity, Throwable throwable) {

        }
    };
    private SessionWriteCompleteListener sessionWriteCompleteListener = new SessionWriteCompleteListener() {
        @Override
        public void run(Session session, int writeSize) {

        }
    };
    private SessionWriteErrorListener sessionWriteErrorListener = new SessionWriteErrorListener() {
        @Override
        public void run(Session session, ByteBuffer byteBuffer, Throwable throwable) {

        }
    };
    private SessionClosedListener sessionClosedListener = new SessionClosedListener() {
        @Override
        public void run(Session session) {

        }
    };
    private SessionWriteWarnListener sessionWriteWarnListener = new SessionWriteWarnListener() {

        @Override
        public boolean run(Session session, int cacheSize) {
            return true;
        }
    };

    @Override
    public void create(Session session) {
        logger.trace("create:{}", session.getSessionID());
        sessionCreateListener.run(session);
    }

    @Override
    public void readComplete(Session session, int readSize) {
        logger.trace("readComplete:{} readSize:{}", session.getSessionID(),readSize);
        sessionReadCompleteListener.run(session, readSize);
    }

    @Override
    public void readError(Session session, ByteBuffer byteBuffer, Throwable throwable) {
        logger.trace("readError:{} {}", session.getSessionID(), throwable.getMessage());
        sessionReadErrorListener.run(session, byteBuffer, throwable);
    }

    @Override
    public void processError(Session session, Object entity, Throwable throwable) {
        logger.error("processError:{}", session.getSessionID(), throwable.getMessage());
        sessionProcessErrorListener.run(session, entity, throwable);
    }

    @Override
    public void writeComplete(Session session, int writeSize) {
        logger.trace("writeComplete:{} writeSize:{}", session.getSessionID(), writeSize);
        sessionWriteCompleteListener.run(session, writeSize);
    }

    @Override
    public void writeError(Session session, ByteBuffer byteBuffer, Throwable throwable) {
        logger.error("writeError:{}", session.getSessionID(), throwable.getMessage());
        sessionWriteErrorListener.run(session, byteBuffer, throwable);
    }

    @Override
    public boolean writeWarn(Session session, int cacheSize) {
        logger.trace("writeWarn:{},cacheSize:{}", session.getSessionID(), cacheSize);
        return sessionWriteWarnListener.run(session, cacheSize);
    }

    @Override
    public void closed(Session session) {
        logger.trace("closed:{}", session.getSessionID());
        sessionClosedListener.run(session);
    }

    public void setSessionCreateListener(SessionCreateListener sessionCreateListener) {
        this.sessionCreateListener = sessionCreateListener;
    }

    public void setSessionReadCompleteListener(SessionReadCompleteListener sessionReadCompleteListener) {
        this.sessionReadCompleteListener = sessionReadCompleteListener;
    }

    public void setSessionReadErrorListener(SessionReadErrorListener sessionReadErrorListener) {
        this.sessionReadErrorListener = sessionReadErrorListener;
    }

    public void setSessionProcessErrorListener(SessionProcessErrorListener sessionProcessErrorListener) {
        this.sessionProcessErrorListener = sessionProcessErrorListener;
    }

    public void setSessionWriteCompleteListener(SessionWriteCompleteListener sessionWriteCompleteListener) {
        this.sessionWriteCompleteListener = sessionWriteCompleteListener;
    }

    public void setSessionWriteErrorListener(SessionWriteErrorListener sessionWriteErrorListener) {
        this.sessionWriteErrorListener = sessionWriteErrorListener;
    }

    public void setSessionClosedListener(SessionClosedListener sessionClosedListener) {
        this.sessionClosedListener = sessionClosedListener;
    }

    public void setSessionWriteWarnListener(SessionWriteWarnListener sessionWriteWarnListener) {
        this.sessionWriteWarnListener = sessionWriteWarnListener;
    }
}
