package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

public interface SessionAfterProcessListener {

    void run(Session session, Object msg);

}
