package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

public interface SessionReadCompleteListener {

    void run(Session session, int readSize);

}
