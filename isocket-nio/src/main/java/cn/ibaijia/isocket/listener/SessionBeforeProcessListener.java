package cn.ibaijia.isocket.listener;


import cn.ibaijia.isocket.session.Session;

import java.nio.ByteBuffer;

public interface SessionBeforeProcessListener {

    void run(Session session, Object msg);

}
