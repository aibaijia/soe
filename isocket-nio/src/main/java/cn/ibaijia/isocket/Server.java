package cn.ibaijia.isocket;

import cn.ibaijia.isocket.handler.Handler;
import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.protocol.Protocol;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author longzl
 */
public class Server extends Context {

    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    private int backlog = 1000;

    private ServerSocketChannel serverSocketChannel = null;

    private List<Handler> handlers = new ArrayList<Handler>();

    public Server(String host, int port, Protocol protocol, Processor processor) {
        this.host = host;
        this.port = port;
        this.addProtocol(protocol);
        this.processor = processor;
    }

    public Server(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public boolean start() {
        try {
            if (this.protocolList.isEmpty() || this.processor == null) {
                logger.error("protocol or processor can't be empty");
                return false;
            }
            System.setProperty(Consts.PROP_POOL_PAGE, threadNumber + "");
            startInfo();
            logger.info("start server host:{},port:{},readThreadNumber:{} writeThreadNumber:{}", this.host, this.port, readThreadNumber, writeThreadNumber);
            readPool = new ThreadPoolExecutor(readThreadNumber, readThreadNumber, 0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
                byte index = 0;

                @Override
                public Thread newThread(Runnable runnable) {
                    return new Thread(runnable, "is-nio-r" + (++index));
                }
            });
            if(writeThreadNumber > 0){
                writePool = new ThreadPoolExecutor(writeThreadNumber, writeThreadNumber, 0L, TimeUnit.MILLISECONDS,
                        new LinkedBlockingQueue<Runnable>(), new ThreadFactory() {
                    byte index = 0;

                    @Override
                    public Thread newThread(Runnable runnable) {
                        return new Thread(runnable, "is-nio-w" + (++index));
                    }
                });
            }
            this.serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            //bind host
            if (host != null) {
                serverSocketChannel.socket().bind(new InetSocketAddress(host, port), backlog);
            } else {
                serverSocketChannel.socket().bind(new InetSocketAddress(port), backlog);
            }
            selector = Selector.open();
            CountDownLatch readCountDownLatch = new CountDownLatch(readThreadNumber);
            initHandler(readCountDownLatch);
            readCountDownLatch.await();
            selectAndProcess();
            return true;
        } catch (Exception e) {
            logger.error("server start error!", e);
            close();
            return false;
        }
    }

    private void initHandler(CountDownLatch countDownLatch) {
        for (int i = 0; i < readThreadNumber; i++) {
            Handler handler = new Handler(i, countDownLatch);
            readPool.execute(handler);
            handlers.add(handler);
        }
    }

    private void selectAndProcess() {
        try {
            regAcceptKey();
            while (true) {
                logger.debug("select!");
                selector.select();
                Set<SelectionKey> readyKeys = selector.selectedKeys();
                logger.debug("readyKeys size:{}", readyKeys.size());
                Iterator<SelectionKey> it = readyKeys.iterator();
                while (it.hasNext()) {
                    SelectionKey key = it.next();
                    it.remove();
                    try {
                        if (key.isValid() && key.isAcceptable()) {
                            processAcceptKey(key);
                        }
                    } catch (Exception e) {
                        logger.error("process key error.", e);
                    }
                }
                if (this.closeStatus == CLOSING) {
                    close(true);
                }
            }
        } catch (Exception e) {
            logger.error("unknown error!", e);
            close();
        }
    }

    private void processAcceptKey(SelectionKey key) {
        SocketChannel socketChannel = null;
        try {
            serverSocketChannel = (ServerSocketChannel) key.channel();
            socketChannel = serverSocketChannel.accept();
//            socketChannel.socket().setTcpNoDelay(false);
            socketChannel.configureBlocking(false);
            logger.trace("key isAcceptable");
            //check blacklist
//            if (isBlackList(socketChannel)) {
//                return;
//            }
            if (socketChannel.isConnectionPending()) {
                socketChannel.finishConnect();
            }
            createSession(socketChannel);
        } catch (Exception e) {
            logger.error("processAcceptKey error", e);
        } finally {
            regAcceptKey();
        }
    }

    private void createSession(SocketChannel channel) {
        Session session = new Session(channel, this, getHandler());
        session.readWait();
        getSessionListener().create(session);
    }

    private String getRemoteAddress(SocketChannel channel) {
        try {
            return channel.socket().getRemoteSocketAddress().toString();
        } catch (Exception e) {
            logger.error("getRemoteAddress error!", e);
            return null;
        }
    }

    private void regAcceptKey() {
        try {
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        } catch (ClosedChannelException e) {
            logger.error("regAcceptKey error.", e);
        }
    }


    private boolean isBlackList(SocketChannel channel) {
        Set<String> blackList = getBlackList();
        if (blackList == null || blackList.isEmpty()) {
            return false;
        }
        try {
            String remoteIp = ((InetSocketAddress) channel.socket().getRemoteSocketAddress()).getHostName();
            boolean res = blackList.contains(remoteIp);
            logger.debug("remoteIp:{} is in blackList:{}", remoteIp, res);
            return res;
        } catch (Exception e) {
            logger.error("check isBlackList error.", e);
            return false;
        }
    }

    @Override
    public void close(boolean force) {
        if (force) {
            try {
                if (serverSocketChannel != null) {
                    serverSocketChannel.close();
                    serverSocketChannel = null;
                }
            } catch (IOException e) {
                logger.error("serverSocketChannel close error!", e);
            }
            this.closeStatus = CLOSED;
            this.readPool.shutdownNow();
            this.writePool.shutdownNow();
        } else {
            this.closeStatus = CLOSING;
            this.readPool.shutdown();
            this.writePool.shutdown();
        }
    }

    private Handler getHandler() {
        Handler minHandler = handlers.get(0);
        int minScore = minHandler.getScore();
        if (minScore > 0) {
            for (int i = 1; i < readThreadNumber; i++) {
                if (handlers.get(i).getScore() == 0) {
                    return handlers.get(i);
                } else {
                    if (handlers.get(i).getScore() < minScore) {
                        minScore = handlers.get(i).getScore();
                        minHandler = handlers.get(i);
                    }
                }
            }
        }
        return minHandler;
    }

    public Server setBacklog(int backlog) {
        if (backlog > 0) {
            this.backlog = backlog;
        }
        return this;
    }

    public void setThreadNumber(int threadNumber) {
        if (threadNumber > MIN_THREAD_NUMBER) {
            this.threadNumber = threadNumber;
        }
    }


}




