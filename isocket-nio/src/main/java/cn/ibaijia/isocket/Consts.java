package cn.ibaijia.isocket;

public class Consts {

    public static final String PROP_READ_FIRST = "isocket.readFirst";
    public static final String PROP_USE_DIRECT_BUFF = "isocket.useDirectBuff";
    public static final String PROP_USE_UNSAFE_BUFF = "isocket.useUnsafe";
    public static final String PROP_USE_POOL = "isocket.usePool";
    public static final String PROP_POOL_PAGE_SIZE = "isocket.poolPageSize";
    public static final String PROP_POOL_PAGE = "isocket.poolPage";
    public static final String PROP_BUFF_SIZE = "isocket.buffSize";

}
