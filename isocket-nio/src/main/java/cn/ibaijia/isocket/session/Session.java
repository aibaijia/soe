package cn.ibaijia.isocket.session;

import cn.ibaijia.isocket.Context;
import cn.ibaijia.isocket.handler.Handler;
import cn.ibaijia.isocket.protocol.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author longzl
 */
public class Session {

    private static final Logger logger = LoggerFactory.getLogger(Session.class);

    private String sessionId;
    private Context context;
    private SocketChannel channel;
    private ByteBuffer readBuffer;
    private InetSocketAddress localAddress;
    private InetSocketAddress remoteAddress;
    private Map<String, Object> attributeMap = new HashMap<>(4);

    /**
     * 响应消息缓存队列。 queue ehcache redis?
     */
    private Queue<ByteBuffer> writeCacheQueue;
    private boolean needClose = false;
    private Handler handler;

    public Session(SocketChannel channel, Context context, Handler handler) {
        if (channel == null) {
            throw new RuntimeException("channel can't be null.");
        }
        this.channel = channel;
        this.context = context;
        this.handler = handler;
        try {
            this.sessionId = SessionManager.genId(channel);
            writeCacheQueue = new ConcurrentLinkedQueue<>();
            readBuffer = handler.getPooledByteBuff().get();
            logger.trace("initSelectionKey");
            SessionManager.put(this);
            logger.trace("initSelectionKey complete.");
        } catch (Exception e) {
            logger.error("create session error!", e);
        }
    }

    public void readNext() {
        if (this.context.getProtocolList().isEmpty() || this.context.getProcessor() == null) {
            throw new RuntimeException("protocol or processor not config");
        }
        if (!writeCacheQueue.isEmpty()) {
            writeWait();
        } else {
            logger.trace("readNext writeCacheQueue empty.");
            readWait();
        }
    }

    private boolean check() {
        int cacheSize = writeCacheQueue.size();
        logger.trace("check:{} WriteWarnLimit:{}", cacheSize, context.getWriteWarnLimit());
        if (cacheSize > context.getWriteWarnLimit()) {
            return context.getSessionListener().writeWarn(this, cacheSize);
        }
        return true;
    }

    public void close() {
        close(true);
    }

    public void close(boolean force) {
        logger.trace("close force :{}", force);
        if (writeCacheQueue.isEmpty()) {
            force = true;
        }
        if (force) {
            try {
                if (channel.isConnected()) {
                    channel.socket().getOutputStream().flush();
                }
                channel.close();
            } catch (Exception e) {
                logger.error("close session exception", e);
            }
            context.getSessionListener().closed(this);
        } else {
            needClose = true;
        }
        handler.getPooledByteBuff().release(readBuffer);
        for (ByteBuffer byteBuffer : writeCacheQueue) {
            handler.getPooledByteBuff().release(byteBuffer);
        }
    }

    public void readBuffer() {
        logger.trace("readBuffer");
        try {
            int readLength = channel.read(readBuffer);
            logger.trace("read length:{}", readLength);
            context.getSessionListener().readComplete(this, readLength);
            if (readLength == -1) {
                logger.trace("session:{} read complete.length:{}", getSessionID(), readLength);
                SessionManager.close(this);
            } else {
                processReadBuffer();
            }
        } catch (Exception e) {
            logger.trace("readBuffer error:{}", e);
            context.getSessionListener().readError(this, readBuffer, e);
            SessionManager.close(this);
        }
    }

    private void processReadBuffer() {
        if (this.context.getProtocolList().isEmpty() || this.context.getProcessor() == null) {
            throw new RuntimeException("protocol or processor not config");
        }
        readBuffer.flip();
        while (true) {
            if (!readBuffer.hasRemaining()) {
                logger.trace("break no remaining.");
                break;
            }
            final Object dataEntry = decode();
            if (dataEntry == null) {
                logger.trace("break decode dataEntry is null.");
                break;
            }
            try {
                boolean success = context.getProcessor().process(this, dataEntry);
                if (!success) {
                    context.getSessionListener().processError(this, dataEntry, new RuntimeException("process fail."));
                }
            } catch (Exception e) {
                context.getSessionListener().processError(this, dataEntry, e);
            }
        }
        readBuffer.compact();
        readNext();
    }

    private Object decode() {
        List<Protocol> protocolList = context.getProtocolList();
        Object inputData = readBuffer;
        for (int i = protocolList.size() - 1; i >= 0; i--) {
            inputData = protocolList.get(i).decode(inputData, this);
            if (inputData == null) {
                return null;
            }
        }
        return inputData;
    }

    private ByteBuffer encode(Object data) {
        if (data == null) {
            throw new RuntimeException("data can't be null.");
        }
        List<Protocol> protocolList = context.getProtocolList();
        Object outputData = data;
        for (int i = 0; i < protocolList.size(); i++) {
            outputData = protocolList.get(i).encode(outputData, this);
        }
        if (outputData instanceof ByteBuffer) {
            return (ByteBuffer) outputData;
        } else {
            throw new RuntimeException("last protocol must encode to ByteBuffer,please check protocol chain.");
        }
    }

    public boolean write(Object data) {
//        if (check()) {
        ByteBuffer byteBuffer = encode(data);
        writeCacheQueue.add(byteBuffer);

//            if(this.handler.getThreadId() == Thread.currentThread().getId()){
//                writeBuffer();
//            }
        writeWait();
        return true;
//        } else {
//            return false;
//        }
    }

    private void writeWait() {
        logger.trace("writeWait");
        handler.writeWait(this);
    }

    public String getSessionID() {
        return this.sessionId;
    }

    public Context getContext() {
        return context;
    }

    public InetSocketAddress getLocalAddress() {
        if (this.localAddress == null && this.channel != null) {
            try {
                this.localAddress = (InetSocketAddress) channel.getLocalAddress();
            } catch (Exception e) {
                logger.error("getLocalAddress error!", e);
            }
        }
        return this.localAddress;
    }

    public InetSocketAddress getRemoteAddress() {
        if (this.remoteAddress == null && this.channel != null) {
            try {
                this.remoteAddress = (InetSocketAddress) channel.socket().getRemoteSocketAddress();
            } catch (Exception e) {
                logger.error("getRemoteAddress error!", e);
            }
        }
        return remoteAddress;
    }

    public void writeBufferAsync() {
        this.context.getWritePool().submit(new Runnable() {
            @Override
            public void run() {
                writeBuffer();
            }
        });
    }

    public void writeBuffer() {
        logger.trace("writeBuffer");
        try {
            while (!writeCacheQueue.isEmpty()) {
                ByteBuffer byteBuffer = writeCacheQueue.peek();
                try {
                    int writeLength = channel.write(byteBuffer);
                    logger.trace("pos2:{} limit:{}", byteBuffer.position(), byteBuffer.limit());
                    if (byteBuffer.hasRemaining()) {
                        writeWait();
                        break;
                    } else {
                        writeCacheQueue.poll();
                        handler.getPooledByteBuff().release(byteBuffer);
                        context.getSessionListener().writeComplete(this, writeLength);
                    }
                } catch (Exception e) {
                    logger.error("writeBuffer write error.", e);
                    context.getSessionListener().writeError(this, byteBuffer, e);
                    SessionManager.close(this);
                    handler.getPooledByteBuff().release(byteBuffer);
                    break;
                }
            }
            if (writeCacheQueue.isEmpty() && needClose) {
                logger.trace("writeBuffer close.");
                SessionManager.close(this);
            } else {
                readWait();
            }
        } catch (Exception e) {
            logger.error("writeBuffer writeCacheQueue error.", e);
        }
    }

    public void readWait() {
        logger.trace("readWait");
        handler.readWait(this);
    }

    public SocketChannel getChannel() {
        return channel;
    }

    public void setAttribute(String key, Object value) {
        attributeMap.put(key, value);
    }

    public Object getAttribute(String key) {
        return attributeMap.get(key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Session session = (Session) o;

        return sessionId != null ? sessionId.equals(session.sessionId) : session.sessionId == null;
    }

    @Override
    public int hashCode() {
        return sessionId != null ? sessionId.hashCode() : 0;
    }

    public int getWriteQueueSize() {
        return writeCacheQueue.size();
    }

    public int getReadBufferPosition() {
        return readBuffer.position();
    }

    public int getReadBufferLimit() {
        return readBuffer.limit();
    }

    public int getReadBufferCapacity() {
        return readBuffer.capacity();
    }

    public boolean isOpen() {
        return !needClose;
    }

    public Handler getHandler() {
        return handler;
    }
}
