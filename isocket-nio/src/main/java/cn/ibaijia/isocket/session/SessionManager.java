package cn.ibaijia.isocket.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.SocketChannel;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SessionManager {
    private static final Logger logger = LoggerFactory.getLogger(SessionManager.class);
    private static Map<String, Session> sessionMap = new ConcurrentHashMap<String, Session>();

    public static int getSessionCount(){
        return sessionMap.size();
    }

    public static void put(Session session) {
        sessionMap.put(session.getSessionID(), session);
    }

    public static Session get(String sessionId) {
        return sessionMap.get(sessionId);
    }

    public static Session get(SocketChannel channel) {
        String sessionId = genId(channel);
        if (sessionId == null) {
            logger.error("sessionId is null");
            return null;
        }
        return sessionMap.get(sessionId);
    }

    public static boolean close(String sessionId) {
        Session session = sessionMap.remove(sessionId);
        return close(session);
    }

    public static boolean close(SocketChannel channel) {
        String sessionId = genId(channel);
        Session session = sessionMap.remove(sessionId);
        return close(session);
    }

    public static boolean close(Session session) {
        if (session == null) {
            logger.error("session is null");
            return false;
        }
        logger.trace("sessionMap remove session");
        sessionMap.remove(session.getSessionID());
        session.close(true);
        return true;

    }

    public static String genId(SocketChannel channel) {
        String sessionId = String.valueOf(channel.hashCode());
        if (channel != null && channel.isOpen()) {
            try {
                sessionId = String.format("%s_%s",channel.getLocalAddress(),channel.socket().getRemoteSocketAddress());
            } catch (Exception e) {
                logger.debug("genId error:{}", e.getMessage());
            }
        }
        return sessionId;
    }

}
