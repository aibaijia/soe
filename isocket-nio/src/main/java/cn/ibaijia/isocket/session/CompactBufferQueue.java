package cn.ibaijia.isocket.session;

import cn.ibaijia.isocket.util.BufferUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class CompactBufferQueue implements Queue<ByteBuffer> {
    private static final Logger logger = LoggerFactory.getLogger(CompactBufferQueue.class);
    //        private ConcurrentLinkedQueue<ByteBuffer> queue = new ConcurrentLinkedQueue<ByteBuffer>();
    private Queue<ByteBuffer> queue = new LinkedList<ByteBuffer>();

    private AtomicInteger size = new AtomicInteger(0);

//    private AtomicInteger totalSize = new AtomicInteger(0);
//    private AtomicInteger getSize = new AtomicInteger(0);

    private int bufferSize = 256 * 1024;

    private ReentrantLock lock = new ReentrantLock();

    private ByteBuffer firstBuffer = null;

    public CompactBufferQueue() {

    }

    public CompactBufferQueue(int bufferSize) {
        if (bufferSize > 100) {
            this.bufferSize = bufferSize;
        }
    }

    private ByteBuffer getFirstBuffer() {
        if (this.firstBuffer == null) {
            try {
                if (queue.isEmpty()) {
                    return null;
                }
                List<ByteBuffer> list = new ArrayList();
                int length = 0;
                while (true) {
                    ByteBuffer buffer = queue.peek();
                    if (buffer == null) {
                        break;
                    }
                    if (length == 0 && buffer.capacity() > bufferSize) { //如果第一个长度就大于了 就返回第一个
                        queue.poll();
                        this.size.getAndDecrement();
                        length += buffer.capacity();
                        list.add(buffer);
                        break;
                    }
                    if (buffer.capacity() + length < bufferSize) {//和当前 长度加起来 小于 bufferSize 则可取出
                        queue.poll();
                        this.size.getAndDecrement();
                        length += buffer.capacity();
                        list.add(buffer);
                    } else {
                        break;
                    }
                }
                if (list.isEmpty()) {
                    this.firstBuffer = null;
                } else if (list.size() == 1) {
                    this.firstBuffer = list.get(0);
                } else {
                    this.firstBuffer = BufferUtil.merge(length, list);
                }
//                this.getSize.getAndAdd(length);
            } catch (Exception e) {
                logger.error("getFirstBuffer error.", e);
                return null;
            }
        }
        return firstBuffer;
    }

    @Override
    public ByteBuffer poll() {
        try {
            lock.lock();
            return getFirstBuffer();
        } catch (Exception e) {
            logger.error("poll error.", e);
            return null;
        } finally {
            this.firstBuffer = null;
            lock.unlock();
        }

    }

    @Override
    public ByteBuffer element() {
        try {
            lock.lock();
            return queue.element();
        } catch (Exception e) {
            logger.error("element error.", e);
            return null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public ByteBuffer peek() {
        try {
            lock.lock();
            return getFirstBuffer();
        } catch (Exception e) {
            logger.error("peek error.", e);
            return null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isEmpty() {
        try {
            lock.lock();
            return this.size() == 0;
        } catch (Exception e) {
            logger.error("isEmpty error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean contains(Object o) {
        try {
            lock.lock();
            return queue.contains(o);
        } catch (Exception e) {
            logger.error("contains error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Iterator<ByteBuffer> iterator() {
        try {
            lock.lock();
            return queue.iterator();
        } catch (Exception e) {
            logger.error("contains error.", e);
            return null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Object[] toArray() {
        try {
            lock.lock();
            return queue.toArray();
        } catch (Exception e) {
            logger.error("toArray error.", e);
            return null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public <T> T[] toArray(T[] a) {
        try {
            lock.lock();
            return queue.toArray(a);
        } catch (Exception e) {
            logger.error("toArray error.", e);
            return null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean add(ByteBuffer byteBuffer) {
        try {
            lock.lock();
//            this.totalSize.getAndAdd(byteBuffer.capacity());
            this.size.getAndIncrement();
            return queue.add(byteBuffer);
        } catch (Exception e) {
            logger.error("add error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean remove(Object o) {
        try {
            lock.lock();
            return queue.remove(o);
        } catch (Exception e) {
            logger.error("remove error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        try {
            lock.lock();
            return queue.containsAll(c);
        } catch (Exception e) {
            logger.error("containsAll error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean addAll(Collection<? extends ByteBuffer> c) {
        try {
            lock.lock();
            return queue.addAll(c);
        } catch (Exception e) {
            logger.error("addAll error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        try {
            lock.lock();
            return queue.removeAll(c);
        } catch (Exception e) {
            logger.error("removeAll error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        try {
            lock.lock();
            return queue.retainAll(c);
        } catch (Exception e) {
            logger.error("retainAll error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void clear() {
        try {
            lock.lock();
            queue.clear();
        } catch (Exception e) {
            logger.error("clear error.", e);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean offer(ByteBuffer byteBuffer) {
        try {
            lock.lock();
            return queue.offer(byteBuffer);
        } catch (Exception e) {
            logger.error("offer error.", e);
            return false;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public ByteBuffer remove() {
        try {
            lock.lock();
            return queue.remove();
        } catch (Exception e) {
            logger.error("remove error.", e);
            return null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int size() {
        try {
            lock.lock();
//            logger.debug("total size:{},get size{}", totalSize.get(), getSize.get());
            int inc = 0;
            if (this.firstBuffer != null) {
                inc = 1;
            }
            return this.size.get() + inc;
        } catch (Exception e) {
            logger.error("size error.", e);
            return -1;
        } finally {
            lock.unlock();
        }
    }

}
