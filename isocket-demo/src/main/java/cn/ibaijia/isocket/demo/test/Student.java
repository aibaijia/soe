package cn.ibaijia.isocket.demo.test;

public class Student {

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String name;

    public int score;

    public int getScore() {
        System.out.println("getScore");
        return score;
    }
}
