package cn.ibaijia.isocket.demo.client;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.listener.SessionCreateListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.protocol.IntegerProtocol;
import cn.ibaijia.isocket.demo.client.processor.IntegerProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;

public class IntegerClient {
    private static final Logger logger = LoggerFactory.getLogger(IntegerClient.class);

    private static CountDownLatch countDownLatch = new CountDownLatch(1);

    public static void main(String[] args) {
        Client client = new Client("127.0.0.1", 7004);
        client.addProtocol(new IntegerProtocol());
        IntegerProcessor integerProcessor = new IntegerProcessor();
        client.setProcessor(integerProcessor);
//        client.setUseCompactQueue(true);
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        client.setSessionCreateListener(new SessionCreateListener() {
            @Override
            public void run(Session session) {
                countDownLatch.countDown();
            }
        });
        client.start();

        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Session session = client.getSession();
        long startTime = System.currentTimeMillis();
        integerProcessor.setWriteStartTime(startTime);
        for (int i = 0; i < 200000; i++) {
            session.write(i);
        }
        long endTime = System.currentTimeMillis();
        integerProcessor.setWriteEndTime(endTime);
        System.out.println("write total time:"+ (endTime-startTime));

    }

    public static Client createClient() {
        Client client = new Client("127.0.0.1", 7004);
        client.addProtocol(new IntegerProtocol());
        client.setProcessor(new IntegerProcessor());
        client.setUseCompactQueue(true);
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        client.setSessionCreateListener(new SessionCreateListener() {
            @Override
            public void run(Session session) {
                countDownLatch.countDown();
            }
        });
        client.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return client;
    }
}
