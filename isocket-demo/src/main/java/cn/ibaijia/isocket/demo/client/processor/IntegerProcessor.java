package cn.ibaijia.isocket.demo.client.processor;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.processor.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntegerProcessor implements Processor<Integer> {

    private static final Logger logger = LoggerFactory.getLogger(IntegerProcessor.class);

    private long writeStartTime;
    private long writeEndTime;
    private long readStartTime;
    private long readEndTime;

    @Override
    public boolean process(Session session, Integer msg) {
        logger.info("vo:{}", msg);
        if (msg == 0) {
            readStartTime = System.currentTimeMillis();
        }
        if (msg == 199999) {
            readEndTime = System.currentTimeMillis();
            System.out.println("write total time:" + (writeEndTime - writeStartTime));
            System.out.println("read total time:" + (readEndTime - readStartTime));
            System.out.println("write read total time:" + (readEndTime - writeStartTime));
        }
        return true;
    }

    @Override
    public void processError(Session session, Integer object, Throwable throwable) {

    }

    public long getWriteStartTime() {
        return writeStartTime;
    }

    public void setWriteStartTime(long writeStartTime) {
        this.writeStartTime = writeStartTime;
    }

    public long getWriteEndTime() {
        return writeEndTime;
    }

    public void setWriteEndTime(long writeEndTime) {
        this.writeEndTime = writeEndTime;
    }
}
