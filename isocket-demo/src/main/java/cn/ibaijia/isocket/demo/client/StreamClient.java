package cn.ibaijia.isocket.demo.client;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.demo.client.processor.ByteBufferProcessor;
import cn.ibaijia.isocket.listener.SessionCreateListener;
import cn.ibaijia.isocket.listener.SessionListener;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;

public class StreamClient {
    private static final Logger logger = LoggerFactory.getLogger(StreamClient.class);

    public static void main(String[] args) {
        Client client = new Client("127.0.0.1", 7004);
        client.addProtocol(new ByteBufferProtocol());
        client.setProcessor(new ByteBufferProcessor());
//        client.setUseCompactQueue(true);
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        client.setSessionCreateListener(new SessionCreateListener() {
            @Override
            public void run(Session session) {
                countDownLatch.countDown();
            }
        });
        client.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Session session = client.getSession();
//        for(int i=0;i<2000;i++){
//            String msg = "jack-"+i;
//            logger.info("write:{}",msg);
//            ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
//            session.write(buffer);
//        }
        try {
//            String fileName = "D:\\test\\server.pdf";
            String fileName = "D:\\test\\test.zip";
            FileInputStream fis = new FileInputStream(new File(fileName));
            int len = 0;
            byte[] buff = new byte[4024];
            while ((len = fis.read(buff)) != -1) {
                System.out.println("len:" + len);
                ByteBuffer byteBuffer = ByteBuffer.wrap(buff, 0, len);
                byte[] newArr = (new byte[len]);
                byteBuffer.get(newArr);
                byteBuffer = ByteBuffer.wrap(newArr);
                session.write(byteBuffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

    }
}
