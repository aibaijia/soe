package cn.ibaijia.isocket.demo.server;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.demo.server.processor.FixLengthStringProcessor;
import cn.ibaijia.isocket.protocol.FixLengthStringProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FixLengthStringServer {
    private static final Logger logger = LoggerFactory.getLogger(FixLengthStringServer.class);

    public static void main(String[] args) {
        Server server = new Server("127.0.0.1", 7004);
        server.addProtocol(new FixLengthStringProtocol());
        server.setProcessor(new FixLengthStringProcessor());
//        server.setUseCompactQueue(true);
        server.setThreadNumber(5);
//        server.showStatus(10000);
        server.start();
    }
}
