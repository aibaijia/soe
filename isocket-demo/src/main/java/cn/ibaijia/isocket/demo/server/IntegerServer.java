package cn.ibaijia.isocket.demo.server;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.demo.server.processor.IntegerProcessor;
import cn.ibaijia.isocket.protocol.IntegerProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntegerServer {
    private static final Logger logger = LoggerFactory.getLogger(IntegerServer.class);

    public static void main(String[] args) {
        Server server = new Server("127.0.0.1", 7004);
        server.addProtocol(new IntegerProtocol());
        server.setProcessor(new IntegerProcessor());
//        server.setUseCompactQueue(true);
        server.setThreadNumber(2);
        server.start();
    }
}
