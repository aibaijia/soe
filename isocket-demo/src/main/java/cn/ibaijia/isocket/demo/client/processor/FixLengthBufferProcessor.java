package cn.ibaijia.isocket.demo.client.processor;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class FixLengthBufferProcessor<B extends Buffer> implements Processor<ByteBuffer> {

    private static final Logger logger = LoggerFactory.getLogger(FixLengthBufferProcessor.class);

    @Override
    public boolean process(Session session, ByteBuffer byteBuffer) {
        String msg = new String(byteBuffer.array());
        logger.info("recv:{}",msg);
        return true;
    }

    @Override
    public void processError(Session session, ByteBuffer object, Throwable throwable) {

    }
}
