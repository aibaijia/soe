package cn.ibaijia.isocket.demo.server;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.demo.server.processor.PlanTextProcessor;
import cn.ibaijia.isocket.demo.server.protocol.SimpleHttpProtocol;
import cn.ibaijia.isocket.listener.SessionProcessErrorListener;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpBootstrap {
    private static final Logger logger = LoggerFactory.getLogger(HttpBootstrap.class);

    public static void main(String[] args) {
        Server server = new Server("127.0.0.1", 8080);
        server.addProtocol(new SimpleHttpProtocol());
        server.setProcessor(new PlanTextProcessor());
        server.setSessionProcessErrorListener(new SessionProcessErrorListener() {
            @Override
            public void run(Session session, Object o, Throwable throwable) {
                logger.error("11 error", throwable);
            }
        });
//        server.setUseDirectBuffer(true);
//        server.setUseCompactQueue(true);
//        server.setThreadNumber(16);
        server.start();
    }

}
