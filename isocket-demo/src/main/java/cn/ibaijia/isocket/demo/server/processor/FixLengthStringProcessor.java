package cn.ibaijia.isocket.demo.server.processor;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.processor.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FixLengthStringProcessor implements Processor<String> {

    private static final Logger logger = LoggerFactory.getLogger(FixLengthStringProcessor.class);

    @Override
    public boolean process(Session session, String msg) {
        logger.info("server recv:{}",msg);
        session.write("hello:"+msg);
        return true;
    }

    @Override
    public void processError(Session session, String object, Throwable throwable) {

    }

}
