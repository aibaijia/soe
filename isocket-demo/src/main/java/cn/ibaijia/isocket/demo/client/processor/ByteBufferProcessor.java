package cn.ibaijia.isocket.demo.client.processor;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicInteger;

public class ByteBufferProcessor implements Processor<ByteBuffer> {

    private static final Logger logger = LoggerFactory.getLogger(ByteBufferProcessor.class);

    private AtomicInteger size = new AtomicInteger(0);

    @Override
    public boolean process(Session session, ByteBuffer byteBuffer) {
        this.size.getAndAdd(byteBuffer.capacity());
        logger.info("length:{} size:{}", byteBuffer.capacity(), size);
        //write to new File
        writeToFile(byteBuffer);
        return true;
    }

    @Override
    public void processError(Session session, ByteBuffer object, Throwable throwable) {

    }

    private void writeToFile(ByteBuffer byteBuffer) {
//        String fileName = "D:\\test\\recv.pdf";
        String fileName = "D:\\test\\recv.zip";
        try {
            RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
            long fileLength = randomFile.length();
            randomFile.seek(fileLength);
//            byteBuffer.flip();
            randomFile.write(byteBuffer.array());
            randomFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }
}
