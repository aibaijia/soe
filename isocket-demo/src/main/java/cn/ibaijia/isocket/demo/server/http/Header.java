package cn.ibaijia.isocket.demo.server.http;

public class Header {

    public String name;
    public String value;

    public Header(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
