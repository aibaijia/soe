package cn.ibaijia.isocket.demo.test;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class ByteBufferTest {
    private static final Logger logger = LoggerFactory.getLogger(ByteBufferTest.class);

    @Test
    public void testPriorityQueue() {

        Queue<Student> queue = new PriorityQueue<Student>(4, new Comparator<Student>() {
            @Override
            public int compare(Student student1, Student student2) {
                return student1.score > student2.score ? 1 : (student1.score < student2.score ? -1 : 0);
            }
        });

        Student student1 = new Student("第1名", 100);
        Student student2 = new Student("第6名", 60);
        Student student3 = new Student("第7名", 55);
        Student student4 = new Student("第3名", 80);
        Student student5 = new Student("第4名", 70);

        queue.add(student1);
        queue.add(student2);
        queue.add(student3);
        queue.add(student4);
        queue.add(student5);

//        while (!queue.isEmpty()) {
//            Student student = queue.poll();
//            System.out.println(student.name);
//        }
        student3.score = 110;
        queue.remove(student3);
        queue.add(student3);

        while (!queue.isEmpty()) {
            Student student = queue.poll();
            System.out.println(student.name);
        }
    }

    @Test
    public void testFinal() {
        final Student student1 = new Student("第1名", 100);
        System.out.println(JSON.toJSONString(student1));
        show(student1);
        System.out.println(JSON.toJSONString(student1));

        synchronized (student1) {
            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        show(student1);
                    }
                });
                student1.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void show(final Student student) {
        System.out.println(JSON.toJSONString(student));
        student.name = "111";
        System.out.println(JSON.toJSONString(student));
    }

    @Test
    public void testArrayBlockingQueue() {
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(2);
        try {
            queue.put("123");
            queue.put("123");
            queue.put("123");
//            queue.put("123");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(queue.size());
    }

    @Test
    public void testByteBuffer() {

        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        ByteBuffer readBuffer = ByteBuffer.allocate(100);
        readBuffer.putInt(1);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", readBuffer.position(), readBuffer.limit(), readBuffer.capacity(), readBuffer.remaining());
        readBuffer.flip();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", readBuffer.position(), readBuffer.limit(), readBuffer.capacity(), readBuffer.remaining());
        byteBuffer.put(readBuffer);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", readBuffer.position(), readBuffer.limit(), readBuffer.capacity(), readBuffer.remaining());
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        readBuffer.compact();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", readBuffer.position(), readBuffer.limit(), readBuffer.capacity(), readBuffer.remaining());
        readBuffer.putInt(2);
        readBuffer.flip();
        readBuffer.position(2);
        byteBuffer.put(readBuffer);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());


        byteBuffer.putInt(3);
//        ByteBuffer byteBuffer1 = byteBuffer.slice();//新的大小来 去掉原pos后的 remain长度
//        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer1.position(), byteBuffer1.limit(), byteBuffer1.capacity(), byteBuffer1.remaining());
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
//        ByteBuffer byteBuffer = ByteBuffer.wrap("123".getBytes());//pos 0 other max
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
//        byteBuffer.flip(); //如里写的 大小超过limit会 报错
//        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(),byteBuffer.remaining());
        byteBuffer.putInt(3);
//        byteBuffer.putLong(4,4L); //pos不会变
//        System.out.println(byteBuffer.getLong(4));
        byte[] bytes = new byte[4];
//        byteBuffer.put(bytes);
        byteBuffer.put(bytes, 0, 2);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.flip();// limit to pos, pos to 0
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        long res = byteBuffer.getInt();
//        res = byteBuffer.getInt();
//        long res = byteBuffer.getInt(0);//输入index，获取后下标不动， 超过下标了也不会报错
        System.out.println(res);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byte[] bytes1 = new byte[2];
        byteBuffer.get(bytes1);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.compact();// pos 变为remain，limit 变为 capacity,通常用于继续读
        logger.info("22 pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
//        byteBuffer.reset();//必须要有mark ,不然报错
//        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(),byteBuffer.remaining());
//        byteBuffer.rewind();// pos to 0, limit 不变 清空flip状态下的pos 重0读
//        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(),byteBuffer.remaining());
        byteBuffer.putInt(3);
//        byteBuffer.clear();//清空数据 pos0 limit max capacity max
        ByteBuffer byteBufferCopy = byteBuffer.duplicate();
        byteBufferCopy.putInt(2);
        logger.info("byteBufferCopy pos:{},limit:{},capacity:{},remain:{}", byteBufferCopy.position(), byteBufferCopy.limit(), byteBufferCopy.capacity(), byteBufferCopy.remaining());
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        ByteBuffer buffer = ByteBuffer.allocate(10);
        System.out.println(buffer.slice().array().length);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        buffer.put("123".getBytes());
        System.out.println(buffer.slice().array().length);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        System.out.println(buffer.capacity());
        buffer.flip();
        System.out.println(buffer.array().length);
        buffer.position(2);
        byteBuffer.put(buffer);//pos开始 limit 结束
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());

    }

    @Test
    public void testSlice(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.putInt(4);
        byteBuffer.flip();
        byteBuffer.get();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        ByteBuffer newBuffer = byteBuffer.slice(); //新的大小来 去掉原pos后的 remain长度
        logger.info("pos:{},limit:{},capacity:{},remain:{}", newBuffer.position(), newBuffer.limit(), newBuffer.capacity(), newBuffer.remaining());
    }

    @Test
    public void testDuplicate(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.putInt(4);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        ByteBuffer byteBuffer1 = byteBuffer.duplicate();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer1.position(), byteBuffer1.limit(), byteBuffer1.capacity(), byteBuffer1.remaining());
        byteBuffer1.flip();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer1.position(), byteBuffer1.limit(), byteBuffer1.capacity(), byteBuffer1.remaining());
    }

    @Test
    public void testPutPos(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.putInt(4);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.putInt(8,5);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
    }


    @Test
    public void testGetPos(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.putInt(4);
        byteBuffer.flip();
        byteBuffer.get();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.get(3);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
    }


    @Test
    public void testFlipPut(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.putInt(4);
        byteBuffer.flip();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.putInt(5);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.putInt(5);
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
    }


    @Test
    public void testGetArray(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.putInt(4);
        System.out.println(byteBuffer.array().length);
        byteBuffer.flip();
        System.out.println(byteBuffer.array().length);//array获取的是原始数组大小
    }


    @Test
    public void testCompact(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.putInt(4);
        byteBuffer.flip();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.get();
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());
        byteBuffer.compact(); //pos 变为剩下的长度 remain，limit 变为 capacity,通常用于继续读
        logger.info("pos:{},limit:{},capacity:{},remain:{}", byteBuffer.position(), byteBuffer.limit(), byteBuffer.capacity(), byteBuffer.remaining());

    }

}
