package cn.ibaijia.isocket.demo.server;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.demo.client.processor.ByteBufferProcessor;
import cn.ibaijia.isocket.listener.SessionListener;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class StreamServer {
    private static final Logger logger = LoggerFactory.getLogger(StreamServer.class);

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        Server server = new Server("127.0.0.1", 7004);
        server.addProtocol(new ByteBufferProtocol());
        server.setProcessor(new ByteBufferProcessor());
//        server.setUseCompactQueue(true);
//        server.setThreadNumber(3);
        server.start();
    }
}
