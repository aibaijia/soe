package cn.ibaijia.isocket.demo.server.processor;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.processor.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntegerProcessor implements Processor<Integer> {

    private static final Logger logger = LoggerFactory.getLogger(IntegerProcessor.class);

    @Override
    public boolean process(Session session, Integer msg) {
        logger.info("recv:{}",msg);
        session.write(msg);
        return true;
    }

    @Override
    public void processError(Session session, Integer object, Throwable throwable) {

    }
}
