package cn.ibaijia.isocket.demo.client;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.listener.SessionCreateListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.demo.client.processor.FixLengthStringProcessor;
import cn.ibaijia.isocket.protocol.FixLengthStringProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;

public class FixLengthStringClient {
    private static final Logger logger = LoggerFactory.getLogger(FixLengthStringClient.class);

    public static void main(String[] args) {
        Client client = new Client("127.0.0.1", 7004);
        client.addProtocol(new FixLengthStringProtocol());
        client.setProcessor(new FixLengthStringProcessor());
//        client.setUseCompactQueue(true);
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        client.setSessionCreateListener(new SessionCreateListener() {
            @Override
            public void run(Session session) {
                countDownLatch.countDown();
            }
        });
        client.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        client.showStatus(10000);
        Session session = client.getSession();
        for (int i = 0; i < 200000; i++) {
            String msg = "jack-" + i;
            session.write(msg);
        }

    }
}
