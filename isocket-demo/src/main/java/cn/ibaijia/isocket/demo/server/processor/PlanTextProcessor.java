package cn.ibaijia.isocket.demo.server.processor;

import cn.ibaijia.isocket.demo.server.http.HttpEntity;
import cn.ibaijia.isocket.demo.server.http.HttpRequestEntity;
import cn.ibaijia.isocket.demo.server.http.HttpResponseEntity;
import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PlanTextProcessor implements Processor<HttpEntity> {
    private static final Logger logger = LoggerFactory.getLogger(PlanTextProcessor.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
    private static final String SERVER_NAME = "isocket-nio-tfb";

    @Override
    public boolean process(Session session, HttpEntity httpEntity) {
        HttpRequestEntity httpRequestEntity = (HttpRequestEntity) httpEntity;
//        logger.info("url:{}", httpRequestEntity.url);
        if (httpRequestEntity.url.contains("/plaintext")) {
            HttpResponseEntity httpResponseEntity = new HttpResponseEntity();
            httpResponseEntity.setHeader("Content-Type", "text/plain; charset=UTF-8");
            httpResponseEntity.setHeader("Server", SERVER_NAME);
            httpResponseEntity.setHeader("Date", dateFormat.format(new Date()));
            httpResponseEntity.body = "Hello, World!";
            session.write(httpResponseEntity);
        } else if (httpRequestEntity.url.contains("/json")) {
            HttpResponseEntity httpResponseEntity = new HttpResponseEntity();
            httpResponseEntity.setHeader("Content-Type", "application/json; charset=UTF-8");
            httpResponseEntity.setHeader("Server", SERVER_NAME);
            httpResponseEntity.setHeader("Date", dateFormat.format(new Date()));
            httpResponseEntity.body = "{\"message\":\"Hello, World!\"}";
            session.write(httpResponseEntity);
        } else {
            HttpResponseEntity httpResponseEntity = new HttpResponseEntity();
            httpResponseEntity.setHeader("Content-Type", "text/plain");
            httpResponseEntity.setHeader("Server", SERVER_NAME);
            httpResponseEntity.setHeader("Date", dateFormat.format(new Date()));
            httpResponseEntity.body = "hi";
        }
        String connection = httpRequestEntity.getHeader("Connection");
        if (connection == null || "close".equals(connection)) {
//            session.close(false); //TODO
        }
        return true;
    }

    @Override
    public void processError(Session session, HttpEntity object, Throwable throwable) {

    }
}
