package cn.ibaijia.isocket.demo.test;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.*;

public class TestPriorityQueue {

    public static void main(String[] args) {
        System.out.println((int) ('\r'));
        System.out.println((int) ('\n'));
        System.out.println((int) (' '));
        System.out.println((int) (':'));
//        PriorityQueue<Student> q1 = new PriorityQueue<Student>(4, new Comparator<Student>() {
//            public int compare(Student e1, Student e2) {
//                return e1.getScore() - e2.getScore();
//            }
//        });
//        Student student1 = new Student("1", 1);
//        Student student2 = new Student("2", 2);
//        Student student3 = new Student("3", 3);
//        Student student4 = new Student("4", 4);
//        q1.add(student1);
//        q1.add(student2);
//        q1.add(student3);
//        q1.add(student4);
//        System.out.println(JSON.toJSONString(q1));
//        student3.score = 1;
//        q1.poll();
//        System.out.println(JSON.toJSONString(q1));
    }

    @Test
    public void testChange() {
        String a = "123";
        byte[] b = new byte[1];
        change(a);
        System.out.println(a);
        changeArr(b);
        System.out.println(b[0]);
    }

    private void change(String str) {
        System.out.println(str);
        str = "333";
    }

    private void changeArr(byte[] b) {
        System.out.println(b[0]);
        b[0] = 2;
    }

    private Map<byte[], String> headers = new HashMap<>();

    @Test
    public void testByteMap() {
        byte[] arr = new byte[2];
        arr[0] = 1;
        arr[1] = 2;
        headers.put(arr, "hello");

        byte[] arr2 = new byte[2];
        arr2[0] = 1;
        arr2[1] = 2;
        System.out.println(headers.get(arr));
        System.out.println(getHeader(arr2));
        System.out.println(getHeader("12".getBytes()));
        System.out.println("12".getBytes().length);

    }

    private String getHeader(byte[] name) {
        for (Map.Entry<byte[], String> entry : headers.entrySet()) {
            if (Arrays.equals(entry.getKey(), name)) {
                return entry.getValue();
            }
        }
        return null;
    }

}
