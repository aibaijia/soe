package cn.ibaijia.isocket.demo.server;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.demo.server.processor.FixLengthBufferProcessor;
import cn.ibaijia.isocket.protocol.FixLengthBufferProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FixLengthBufferServer {
    private static final Logger logger = LoggerFactory.getLogger(FixLengthBufferServer.class);

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        Server server = new Server("127.0.0.1", 7004);
        server.addProtocol(new FixLengthBufferProtocol());
//        server.addProtocol(new FixLengthBigBufferProtocol());
        server.setProcessor(new FixLengthBufferProcessor());
        server.setThreadNumber(4);
        server.start();
    }
}
