package cn.ibaijia.isocket.demo.utils;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;

public class Util {

    public static int availableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }

    public static boolean getBooleanProperty(String key) {
        return getBooleanProperty(key, false);
    }

    public static boolean getBooleanProperty(String key, boolean defaultValue) {
        String v = System.getProperty(key);
        if (!isNullOrBlank(v)) {
            try {
                return isTrueValue(v);
            } catch (Throwable e) {
            }
        }
        return defaultValue;
    }

    public static int getIntProperty(String key) {
        return getIntProperty(key, 0);
    }

    public static int getIntProperty(String key, int defaultValue) {
        String v = System.getProperty(key);
        if (!isNullOrBlank(v)) {
            try {
                return Integer.parseInt(v);
            } catch (Throwable e) {
            }
        }
        return defaultValue;
    }

    public static String getStringProperty(String key) {
        return getStringProperty(key, null);
    }

    public static String getStringProperty(String key, String defaultValue) {
        String v = System.getProperty(key);
        if (!isNullOrBlank(v)) {
            return v;
        }
        return defaultValue;
    }

    public static String getValueFromArray(String[] args, int index) {
        return getValueFromArray(args, index, null);
    }

    public static String getValueFromArray(String[] args, int index, String defaultValue) {
        if (index < 0 || args == null) {
            return defaultValue;
        }
        if (index >= args.length) {
            return defaultValue;
        }
        return args[index];
    }

    public static int indexOf(CharSequence sb, char ch) {
        return indexOf(sb, ch, 0);
    }

    public static int indexOf(CharSequence sb, char ch, int index) {
        int count = sb.length();
        for (int i = index; i < count; i++) {
            if (ch == sb.charAt(i)) {
                return i;
            }
        }
        return -1;
    }

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.size() == 0;
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.size() == 0;
    }

    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isNullOrBlank(String value) {
        return value == null || value.length() == 0;
    }

    public static boolean isTrueValue(String value) {
        return "true".equals(value) || "1".equals(value);
    }

    public static Throwable trySetAccessible(AccessibleObject object) {
        try {
            object.setAccessible(true);
            return null;
        } catch (Throwable e) {
            return e;
        }
    }

    public static Field getDeclaredField(Class<?> clazz, String name) {
        if (clazz == null) {
            return null;
        }
        Field[] fs = clazz.getDeclaredFields();
        for (Field f : fs) {
            if (f.getName().equals(name)) {
                return f;
            }
        }
        return null;
    }

}
