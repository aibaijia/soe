package cn.ibaijia.isocket.demo.client;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.demo.client.processor.FixLengthBufferProcessor;
import cn.ibaijia.isocket.listener.SessionCreateListener;
import cn.ibaijia.isocket.protocol.FixLengthBigBufferProtocol;
import cn.ibaijia.isocket.protocol.FixLengthBufferProtocol;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;

public class FixLengthBufferClient {
    private static final Logger logger = LoggerFactory.getLogger(FixLengthBufferClient.class);

    public static void main(String[] args) {
        Client client = new Client("127.0.0.1", 7004);
        client.addProtocol(new FixLengthBufferProtocol());
//        client.addProtocol(new FixLengthBigBufferProtocol());
        client.setProcessor(new FixLengthBufferProcessor());
//        client.setThreadNumber(5);
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        client.setSessionCreateListener(new SessionCreateListener() {
            @Override
            public void run(Session session) {
                countDownLatch.countDown();
            }
        });
        client.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Session session = client.getSession();
        for (int i = 0; i < 10; i++) {
            String msg = "jack-" + i;
            logger.info("write:{}", msg);
            ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
            session.write(buffer);
        }

    }
}
