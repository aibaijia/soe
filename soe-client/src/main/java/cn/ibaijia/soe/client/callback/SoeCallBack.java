package cn.ibaijia.soe.client.callback;

import cn.ibaijia.soe.client.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SoeCallBack {
	private static Logger logger = LoggerFactory.getLogger(SoeCallBack.class);
	public SoeObject soeObject;
	protected void beforeRecv(SoeObject soeObject) {
	}

	public boolean doRecv(SoeObject soeObject) {
		this.soeObject = soeObject;
		this.beforeRecv(soeObject);
		boolean res =  recv(soeObject);
		this.afterRecv(soeObject);
		return res;
	}

	protected abstract boolean recv(SoeObject soeObject);

	protected void afterRecv(SoeObject soeObject) {
	}

	public void onTimeOut(SoeObject soeObject, long maxWaitTime) {
		logger.error("recv data time out["+maxWaitTime+"]"+soeObject.getLogStr());
	}

}
