package cn.ibaijia.soe.client.processor;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.soe.client.Consts;
import cn.ibaijia.soe.client.Context;
import cn.ibaijia.soe.client.callback.SoeCallBack;
import cn.ibaijia.soe.client.callback.SoeCloseableCallBack;
import cn.ibaijia.soe.client.callback.SoeListenerCallBack;
import cn.ibaijia.soe.client.callback.SoeStreamCallBack;
import cn.ibaijia.soe.client.listener.SoeSessionListener;
import cn.ibaijia.soe.client.message.*;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.client.session.SoeSession;
import cn.ibaijia.soe.client.session.SoeSessionManager;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SoeObjectProcessor implements Processor<SoeObject> {

    private static Logger logger = LoggerFactory.getLogger(SoeObjectProcessor.class);

    private Context context;

    public SoeObjectProcessor(Context context) {
        this.context = context;
    }

    @Override
    public boolean process(Session session, final SoeObject soeObject) {
        Short serviceId = soeObject.getServiceId();
        if (isInnerMessage(serviceId)) {
            processInnerMessage(soeObject, session);
        } else {
            SoeCallBack callBack = context.getCallBacks().get(soeObject.getMsgId());
            if (callBack != null) {
                if (callBack instanceof SoeCloseableCallBack) {//暂时分开写吧
                    callBack.doRecv(soeObject);
                } else if (callBack instanceof SoeListenerCallBack) {
                    callBack.doRecv(soeObject);
                } else if (callBack instanceof SoeStreamCallBack) {
                    callBack.doRecv(soeObject);
                } else {
                    context.getCallBacks().remove(soeObject.getMsgId());
                    callBack.doRecv(soeObject);
                    synchronized (callBack) {
                        callBack.notifyAll();
                    }
                }
            } else {//provider
                //find providers by serviceId
                final BaseService serviceConfig = context.servicesMap.get(soeObject.getServiceId());
                if (serviceConfig != null) {
                    serviceConfig.setContext(context);
                    context.executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            serviceConfig.doService(soeObject);
                        }
                    });
                } else {
                    logger.error("unknown info:" + soeObject.getLogStr());
                }
            }
        }
        return true;
    }

    @Override
    public void processError(Session session, SoeObject object, Throwable throwable) {
        logger.error("processError!", throwable);
    }

    private void processInnerMessage(final SoeObject soeObject, Session session) {
        Short serviceId = soeObject.getServiceId();
        byte[] bytes = soeObject.getBody();
        String body = new String(bytes, StandardCharsets.UTF_8);
        logger.debug("inner {} serviceId:{} body:{}", Consts.FUNCID.codeToDesc(serviceId), serviceId, body);
        if (serviceId == Consts.FUNCID.LOGIN_HELLO.code()) {
            ClientLoginReq req = new ClientLoginReq();
            req.appId = context.getAppId();
            req.appKey = context.getPassword();
            req.serviceInfos = genServiceInfo(context.servicesMap);
            SoeObject resp = new SoeObject(Consts.FUNCID.LOGIN_REQ.code(), req);
            session.write(resp);
        } else if (serviceId == Consts.FUNCID.LOGIN_RESP.code()) {
//            save server login vo
            ServerLoginResp resp = JSON.parseObject(body, ServerLoginResp.class);
            final SoeSessionListener sessionListener = (SoeSessionListener) session.getContext().getSessionListener();
            if (resp == null || !resp.status) {
                logger.info("login false.");
                sessionListener.loginFailed(context.getAppId());
                SessionManager.close(session);
                return;
            }
            final SoeSession soeSession = new SoeSession(session);
            soeSession.setId(context.getAppId());
            soeSession.setSn(resp.sn);
            this.context.setFullName(soeSession.getFullName());
            SoeSessionManager.put(soeSession);
            context.executorService.execute(new Runnable() {
                @Override
                public void run() {
                    sessionListener.loginSuccess(soeSession);
                }
            });
        } else if (serviceId == Consts.FUNCID.HEARTBEAT_RESP.code()) {
//            logger.info(body);
//        } else if (serviceId == Consts.FUNCID.RELOAD_RESP.code()) {
//            ServerLoginResp respBody = reload(session);
//            soeObject.setServiceId(Consts.FUNCID_RELOAD_RESP);
//            soeObject.setBody(respBody);
//            session.write(soeObject.getBuffer());
        } else {
            logger.error("unknown InnerMessage error. serviceId:{}", serviceId);
        }

    }

    private List<ServiceInfo> genServiceInfo(Map<Short, BaseService> servicesMap) {
        List<ServiceInfo> list = new ArrayList<>();
        for (BaseService baseService : servicesMap.values()) {
            ServiceInfo serviceInfo = new ServiceInfo();
            serviceInfo.appId = context.getAppId();
            serviceInfo.serviceId = baseService.getId();
            serviceInfo.name = baseService.getName();
            list.add(serviceInfo);
        }
        return list;
    }

//    private ServerLoginResp reload(Session session) {
////        SoeSession soeSession = session.getAttachment();
//        //TODO reload config
//        ServerLoginResp resp = new ServerLoginResp();
//        resp.status = true;
//        resp.backupHost = new ArrayList<HostInfo>();
//        resp.appInfos = new ArrayList<AppInfo>();
//        return resp;
//    }

    private boolean isInnerMessage(Short serviceId) {
        if (serviceId > -100 && serviceId < 0) {
            return true;
        }
        return false;
    }

}
