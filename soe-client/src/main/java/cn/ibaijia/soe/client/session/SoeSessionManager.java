package cn.ibaijia.soe.client.session;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.client.listener.SoeSessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class SoeSessionManager {

    private static final Logger logger = LoggerFactory.getLogger(SoeSessionManager.class);
    private static Map<String, SoeSession> nameMap = new HashMap<String, SoeSession>();
    private static Map<Short, LinkedList<SoeSession>> idMap = new HashMap<Short, LinkedList<SoeSession>>();

    public static synchronized void put(SoeSession session) {
        if (session.getId() == null || session.getSn() == null) {
            logger.error("put SoeSession: id and sn,must not be null.");
            return;
        }
        String name = session.getId() + "." + session.getSn();
        nameMap.put(name, session);

        LinkedList<SoeSession> idQueue = idMap.get(session.getId());
        if (idQueue == null) {
            idQueue = new LinkedList<SoeSession>();
            idMap.put(session.getId(), idQueue);
        }
        idQueue.add(session);
//        idMap.put(session.getId(),idQueue);
    }

    public static synchronized SoeSession get(Short id, Byte sn) {
        if (sn == null || sn == 0) {
            LinkedList<SoeSession> idQueue = idMap.get(id);
            if (idQueue != null) {
                SoeSession soeSession = idQueue.poll();
                idQueue.add(soeSession);
                return soeSession;
            }
        } else {
            String name = id + "." + sn;
            return nameMap.get(name);
        }
        return null;
    }

    private static synchronized boolean close(Short id, Byte sn) {
        if (id == null || sn == null) {
            logger.error("close SoeSession: id and sn,must not be null.");
            return false;
        }
        String name = id + "." + sn;
        SoeSession soeSession = nameMap.remove(name);
        LinkedList<SoeSession> idQueue = idMap.get(id);
        if (idQueue != null) {
            idQueue.remove(soeSession);
            soeSession.close();
            return true;
        }
        return false;
    }

    public static synchronized boolean close(SoeSession soeSession) {
        if (soeSession == null) {
            logger.warn("close soeSession is null");
            return false;
        }
        return close(soeSession.getId(), soeSession.getSn());
    }

    public static SoeSession get(Session session) {
        for (SoeSession soeSession : nameMap.values()) {
            if(soeSession == null || soeSession.getSession() == null){
                continue;
            }
            if (soeSession.getSession().equals(session)) {
                return soeSession;
            }
        }
        return null;
    }
}
