package cn.ibaijia.soe.client.provider;

import cn.ibaijia.soe.client.Context;
import cn.ibaijia.soe.client.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseService {

	private short id;

    private String name;

    public BaseService(short id, String name) {
        this.id = id;
        this.name = name;
    }

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
	protected Context context;
	public void response(SoeObject soeObject){
		if(context != null){
			context.send(soeObject);
		}
	}
	public void setContext(Context context) {
		this.context = context;
	}

    public short getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public abstract void doService(SoeObject soeObject);
}
