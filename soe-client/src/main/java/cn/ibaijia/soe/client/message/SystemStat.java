package cn.ibaijia.soe.client.message;

public class SystemStat {
//    @FieldAnn(required = false, comments = "操作系统名称")
    public String osName;
//    @FieldAnn(required = false, comments = "操作系统版本")
    public String osVersion;
//    @FieldAnn(required = false, comments = "操作系统类型")
    public String osArch;
//    @FieldAnn(required = false, comments = "用户")
    public String userName;
//    @FieldAnn(required = false, comments = "用户目录")
    public String userHome;
//    @FieldAnn(required = false, comments = "目录")
    public String userDir;
//    @FieldAnn(required = false, comments = "用户时区")
    public String userTimezone;
//    @FieldAnn(required = false, comments = "用户语言")
    public String userLanguage;
//    @FieldAnn(required = false, comments = "临时目录")
    public String tmpDir;
//    @FieldAnn(required = false, comments = "运行环境名")
    public String runtimeName;
//    @FieldAnn(required = false, comments = "运行环境版本")
    public String runtimeVersion;
//    @FieldAnn(required = false, comments = "JVM名")
    public String jvmName;
//    @FieldAnn(required = false, comments = "JVM版本")
    public String jvmVersion;
//    @FieldAnn(required = false, comments = "JavaHome")
    public String javaHome;
//    @FieldAnn(required = false, comments = "Java版本")
    public String javaVersion;
//    @FieldAnn(required = false, comments = "文件编码")
    public String fileEncoding;
//    @FieldAnn(required = false, comments = "CatalinaHome")
    public String catalinaHome;
//    @FieldAnn(required = false, comments = "总内存")
    public Long totalMemory;
//    @FieldAnn(required = false, comments = "剩余内存")
    public Long freeMemory;
//    @FieldAnn(required = false, comments = "已用内存")
    public Long useMemory;
//    @FieldAnn(required = false, comments = "CPU使用率")
    public Integer cpuRatio;
//    @FieldAnn(required = false, comments = "线程数")
    public Integer threadCount;
//    @FieldAnn(required = false, comments = "服务ID")
    public String clusterId;

}
