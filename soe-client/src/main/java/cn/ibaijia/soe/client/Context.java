package cn.ibaijia.soe.client;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.soe.client.callback.DefaultCallBack;
import cn.ibaijia.soe.client.callback.SoeCallBack;
import cn.ibaijia.soe.client.listener.DefaultSoeSessionListener;
import cn.ibaijia.soe.client.listener.SoeClosedListener;
import cn.ibaijia.soe.client.listener.SoeLoginFailedListener;
import cn.ibaijia.soe.client.listener.SoeLoginSuccessListener;
import cn.ibaijia.soe.client.message.SystemStat;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.client.utils.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.*;
import java.util.concurrent.*;

public abstract class Context {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    private static final byte DEFAULT_SN = 0;
    protected String host;
    protected int port;
    protected short appId;
    protected String password;
    protected Client client;
    protected String fullName;
    //consumers
    protected Map<Integer, SoeCallBack> callBacks = new ConcurrentHashMap<Integer, SoeCallBack>();
    protected long getTimeOut = 60 * 1000L;
    //services
    public Map<Short, BaseService> servicesMap = new TreeMap<>();

    public Object reconnectLock = new Object();

    protected void waitReconnect() {
        logger.debug("waitReconnect");
        synchronized (this.reconnectLock) {
            try {
                this.reconnectLock.wait();
            } catch (InterruptedException e) {
                logger.error("", e);
            }
            logger.debug("waitReconnect restart");
            this.client.restart();
            this.waitReconnect();
        }
    }

    public void notifyReconnect() {
        logger.debug("notifyReconnect");
        synchronized (this.reconnectLock) {
            try {
                this.reconnectLock.notifyAll();
            } catch (Exception e) {
                logger.error("", e);
            }
        }
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public short getAppId() {
        return appId;
    }

    public void setAppId(short appId) {
        this.appId = appId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getGetTimeOut() {
        return getTimeOut;
    }

    public void setGetTimeOut(long getTimeOut) {
        this.getTimeOut = getTimeOut;
    }

    public Map<Integer, SoeCallBack> getCallBacks() {
        return callBacks;
    }

    protected Timer timer = null;

    public ExecutorService executorService = null;

    protected void initThreadPool() {
        if (executorService == null) {
            executorService = Executors.newCachedThreadPool(new ThreadFactory() {
                byte index = 0;

                @Override
                public Thread newThread(Runnable runnable) {
                    return new Thread(runnable, "soe-p-" + (++index));
                }
            });
        }
    }

    //只发不收
    public void send(short toId, byte toSn, short serviceId, byte type, Object data) {
        SoeObject soeObject = new SoeObject(toId, toSn, serviceId, type, 1, data);
        client.getSession().write(soeObject);
    }

    public void send(String toName, short serviceId, byte type, Object data) {
        String[] names = toName.split(".");
        if (names.length != 2) {
            logger.error("fullName error:{}", toName);
            return;
        }
        Short toId = Short.valueOf(names[0]);
        Byte toSn = Byte.valueOf(names[1]);
        send(toId, toSn, serviceId, type, data);
    }

    public void send(Short toId, short serviceId, byte type, Object data) {
        byte toSn = DEFAULT_SN;
        send(toId, toSn, serviceId, type, data);
    }

    public void send(Short toId, short serviceId, Object data) {
        byte toSn = DEFAULT_SN;
        byte type = Consts.MESSAGE_TYPE.STRING.code();
        send(toId, toSn, serviceId, type, data);
    }

    public void send(SoeObject soeObject) {
        client.getSession().write(soeObject);
    }

    public void sendInputStream(short toId, byte toSn, short serviceId, byte type, Integer msgId, InputStream inputStream, SoeCallBack callBack) throws Exception {
        logger.debug("sendInputStream SoeObject by msgId:{}", msgId);
        callBacks.put(msgId, callBack);
        byte[] readBuffer = new byte[512];
        while (true) {
            int len = inputStream.read(readBuffer);
            if (len != -1) {//close
                break;
            }
            if (len > 0) {
                SoeObject soeObject = new SoeObject(toId, toSn, serviceId, type, msgId, Arrays.copyOf(readBuffer, len));
                client.getSession().write(soeObject);
            }
        }
        if (callBacks.containsKey(msgId)) {
            synchronized (callBack) {
                callBacks.remove(msgId);
                callBack.notifyAll();
            }
        }
    }

    public SoeObject sendSync(short toId, byte toSn, short serviceId, byte type, int msgId, Object data, SoeCallBack callBack) {
        logger.debug("sendSync SoeObject by msgId:{}", msgId);
        SoeObject soeObject = new SoeObject(toId, toSn, serviceId, type, msgId, data);
        client.getSession().write(soeObject);
        callBacks.put(soeObject.getMsgId(), callBack);
        synchronized (callBack) {
            try {
                callBack.wait(getTimeOut);
            } catch (InterruptedException e) {
                logger.error("", e);
            }
        }
        if (callBacks.containsKey(soeObject.getMsgId())) {
            callBack.onTimeOut(soeObject, getTimeOut);
            synchronized (callBack) {
                callBacks.remove(soeObject.getMsgId());
                callBack.notifyAll();
            }
        }
        return callBack.soeObject;
    }

    public void sendAndListenOnMsgId(SoeObject soeObject, SoeCallBack callBack) {
        logger.debug("sendAndListenOnMsgId SoeObject by msgId:{}", soeObject.getMsgId());
//        SoeObject soeObject = new SoeObject(toId, toSn, serviceId, type, msgId, data);
        client.getSession().write(soeObject);
        callBacks.put(soeObject.getMsgId(), callBack);
    }

    public void stopListenOnMsLgId(Integer msgId) {
        SoeCallBack callBack = callBacks.get(msgId);
        if (callBack != null) {
            callBacks.remove(msgId);
        }
    }

    public SoeObject sendSync(String toName, short serviceId, byte type, int msgId, Object data, SoeCallBack callBack) {
        String[] names = toName.split(".");
        if (names.length != 2) {
            logger.error("fullName error:{}", toName);
            return null;
        }
        Short toId = Short.valueOf(names[0]);
        Byte toSn = Byte.valueOf(names[1]);
        return sendSync(toId, toSn, serviceId, type, msgId, data, callBack);
    }

    public SoeObject sendSync(Short toId, short serviceId, byte type, int msgId, Object data, SoeCallBack callBack) {
        byte toSn = DEFAULT_SN;
        return sendSync(toId, toSn, serviceId, type, msgId, data, callBack);
    }

    public SoeObject sendSync(Short toId, short serviceId, int msgId, Object data, SoeCallBack callBack) {
        byte toSn = DEFAULT_SN;
        byte type = Consts.MESSAGE_TYPE.STRING.code();
        return sendSync(toId, toSn, serviceId, type, msgId, data, callBack);
    }

    public SoeObject sendSync(SoeObject soeObject, SoeCallBack callBack) {
        client.getSession().write(soeObject);
        callBacks.put(soeObject.getMsgId(), callBack);
        synchronized (callBack) {
            try {
                callBack.wait(getTimeOut);
            } catch (InterruptedException e) {
                logger.error("", e);
            }
        }
        if (callBacks.containsKey(soeObject.getMsgId())) {
            callBack.onTimeOut(soeObject, getTimeOut);
            synchronized (callBack) {
                callBacks.remove(soeObject.getMsgId());
                callBack.notifyAll();
            }
        }
        return callBack.soeObject;
    }

    public SoeObject sendSync(short toId, byte toSn, short serviceId, byte type, int msgId, Object data) {
        return sendSync(toId, toSn, serviceId, type, msgId, data, new DefaultCallBack());
    }

    public SoeObject sendSync(String toName, short serviceId, byte type, int msgId, Object data) {
        return sendSync(toName, serviceId, type, msgId, data, new DefaultCallBack());
    }

    public SoeObject sendSync(Short toId, short serviceId, byte type, int msgId, Object data) {
        return sendSync(toId, serviceId, type, msgId, data, new DefaultCallBack());
    }

    public SoeObject sendSync(Short toId, short serviceId, int msgId, Object data) {
        return sendSync(toId, serviceId, msgId, data, new DefaultCallBack());
    }

    public SoeObject sendSync(SoeObject soeObject) {
        return sendSync(soeObject, new DefaultCallBack());
    }

    public void shutdown() {
        try {
            if (this.client != null) {
                client.close(true);
            }
            if (this.timer != null) {
                this.timer.cancel();
                this.timer.purge();
            }
            if (this.executorService != null) {
                this.executorService.shutdownNow();
            }
        } catch (Exception e) {
            logger.error("shutdown error.", e);
        } finally {
            this.timer = null;
            this.client = null;
            this.executorService = null;
        }

    }

    public void heartBeat() {
        try {
            SystemStat systemStat = SystemUtil.getSystemStat();
            SoeObject soeObject = new SoeObject(Consts.FUNCID.HEARTBEAT_REQ.code(), systemStat);
            logger.debug("send heartbeat.");
            this.send(soeObject);
        } catch (Exception e) {
            logger.error("heartBeat error.", e);
            this.notifyReconnect();
        }
    }

    protected DefaultSoeSessionListener soeSessionListener = new DefaultSoeSessionListener();

    public void setSoeLoginSuccessListener(SoeLoginSuccessListener listener) {
        this.soeSessionListener.setSoeLoginSuccessListener(listener);
    }

    public void setSoeLoginFailedListener(SoeLoginFailedListener listener) {
        this.soeSessionListener.setSoeLoginFailedListener(listener);
    }

    public void setSoeClosedListener(SoeClosedListener listener) {
        this.soeSessionListener.setSoeClosedListener(listener);
    }

    public DefaultSoeSessionListener getSoeListener() {
        return soeSessionListener;
    }

    public abstract boolean start();
}
