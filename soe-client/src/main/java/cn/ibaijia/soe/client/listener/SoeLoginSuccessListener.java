package cn.ibaijia.soe.client.listener;

import cn.ibaijia.soe.client.session.SoeSession;

public interface SoeLoginSuccessListener {

    void run(SoeSession session);

}
