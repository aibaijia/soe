package cn.ibaijia.soe.client.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ java.lang.annotation.ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface SoeMapper {
	
	
	/**
	 * @return 需要绑定的Server Id
	 */
	public short id();
	
	/**
	 * @return 需要绑定的Server sn
	 */
	public short sn() default 0;
	
	/**
	 * @return 需要绑定的function Id
	 */
	public short serviceId();
	
	/**
	 * @return 是否使用缓存
	 */
	public boolean useCache() default false;
	
	/**
	 * @return 过期时间秒
	 */
	public int expireSeconds() default 0;
	
	/**
	 * @return 自定义缓存key
	 */
	public String cacheKey() default "";
	
	
}
