package cn.ibaijia.soe.client.callback;

import cn.ibaijia.soe.client.Context;
import cn.ibaijia.soe.client.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 可手动remove监听的回调，用于发一次 响应多条数据的情况 1:n
 */
public abstract class SoeCloseableCallBack extends SoeCallBack {
    public Context context;

    public SoeCloseableCallBack(Context context) {
        this.context = context;
    }

    public void close() {
        if (context != null) {
            context.getCallBacks().remove(this);
        }
        synchronized (this) {
            this.notifyAll();
        }
    }

}
