package cn.ibaijia.soe.client.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


/**
 * @author longzl / @createOn 2010-9-3
 */
public class FileUtil {
    private static Logger log = LoggerFactory.getLogger(FileUtil.class);

    public static File getResource(String filename) {
        try {
            return ResourceUtil.getFile(filename);
        } catch (FileNotFoundException e) {
            log.error("file not found:" + filename, e);
        }
        return null;
    }

    public static InputStream getResourceAsStream(String filename) {
        try {
            return ClassUtil.getDefaultClassLoader().getResourceAsStream(ResourceUtil.getResourceURI(filename));
        } catch (Exception e) {
            log.error("file not found:" + filename, e);
        }
        return null;
    }

    public static File createNewFile(String filePath) {
        filePath = filePath.replace('\\', '/').trim();
        new File(filePath.substring(0, filePath.lastIndexOf('/'))).mkdirs();
        return new File(filePath);
    }

    public static File getFile(String filename) {
        return new File(filename);
    }
}
