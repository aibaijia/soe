package cn.ibaijia.soe.client.listener;

import cn.ibaijia.soe.client.session.SoeSession;

public interface SoeSessionListener {

    void loginSuccess(SoeSession session);

    void loginFailed(Short appId);

    void closed(SoeSession session);

    void setSoeLoginSuccessListener(SoeLoginSuccessListener listener);

    void setSoeLoginFailedListener(SoeLoginFailedListener listener);

    void setSoeClosedListener(SoeClosedListener listener);
}
