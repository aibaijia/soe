package cn.ibaijia.soe.client.callback;

import cn.ibaijia.soe.client.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 创建输出流回调
 */
public abstract class SoeStreamCallBack extends SoeCallBack {
    private Logger logger = LoggerFactory.getLogger(SoeStreamCallBack.class);
    public OutputStream outputStream;

    public SoeStreamCallBack(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    protected boolean recv(SoeObject soeObject){
        try {
            if(outputStream != null){
                outputStream.write(soeObject.getBody());
            }
            return true;
        } catch (IOException e) {
           logger.error("write to output stream error.",e);
           return false;
        }
    }

}
