package cn.ibaijia.soe.client;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.protocol.FixLengthBigBufferProtocol;
import cn.ibaijia.soe.client.processor.SoeObjectProcessor;
import cn.ibaijia.soe.client.protocol.SoeProtocol;
import cn.ibaijia.soe.client.provider.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 提供服务
 */
public class SoeClient extends Context {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public SoeClient(String host, int port, short appId, String password) {
        this.host = host;
        this.port = port;
        this.appId = appId;
        this.password = password;
    }

    @Override
    public boolean start() {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    startAndWait();
                }
            });
            thread.setName("soe-client");
            thread.start();
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }

    private void startHeartBeat() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (client.isOpen()) {
                        heartBeat();
                    } else {
                        notifyReconnect();
                    }
                }

            }, 20000, 60000);
        }
    }

    public void addService(BaseService baseService) {
        this.servicesMap.put(baseService.getId(), baseService);
    }

    private void startAndWait() {
        initThreadPool();
        client = new Client(host, port);
        client.addProtocol(new SoeProtocol());
        client.addProtocol(new FixLengthBigBufferProtocol());
        client.setProcessor(new SoeObjectProcessor(this));
        client.setSessionListener(soeSessionListener);
        client.start();
        startHeartBeat();
        waitReconnect();
    }

}
