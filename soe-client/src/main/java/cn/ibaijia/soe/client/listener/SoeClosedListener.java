package cn.ibaijia.soe.client.listener;

import cn.ibaijia.soe.client.session.SoeSession;

public interface SoeClosedListener {

    void run(SoeSession session);

}
