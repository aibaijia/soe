package cn.ibaijia.soe.client.listener;

public interface SoeLoginFailedListener {

    void run(Short appId);

}
