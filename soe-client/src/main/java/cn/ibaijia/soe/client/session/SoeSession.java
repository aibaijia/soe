package cn.ibaijia.soe.client.session;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.soe.client.Consts;
import cn.ibaijia.soe.client.listener.SoeSessionListener;
import cn.ibaijia.soe.client.protocol.SoeObject;

public class SoeSession {

    private Session session;

    private Short id;//client or provider's id
    private Byte sn;//同一应用多开的id 同一应用唯一

    private String fullName;

    public SoeSession(Session session) {
        this.session = session;
        this.session.setAttribute(Consts.SOE_SESSION_KEY,this);
    }

    public void write(SoeObject soeObject) {
        this.session.write(soeObject);
    }

    public void close() {
        SoeSessionListener soeSessionListener = (SoeSessionListener) session.getContext().getSessionListener();
        soeSessionListener.closed(this);
        SessionManager.close(this.session);
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public Byte getSn() {
        return sn;
    }

    public void setSn(Byte sn) {
        this.sn = sn;
    }

    public Session getSession() {
        return session;
    }

    public String getFullName() {
        if (fullName == null) {
            this.fullName = (this.id + "." + this.sn);
        }
        return fullName;
    }
}
