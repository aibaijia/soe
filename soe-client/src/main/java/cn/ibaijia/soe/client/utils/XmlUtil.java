package cn.ibaijia.soe.client.utils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class XmlUtil {
	private static Logger log = LoggerFactory.getLogger(XmlUtil.class);
	
	/**
	 * 
	 * @param xmlStr xmlStr
	 * @return Document
	 */
	public static Document parseText(String xmlStr){
		try {
			return DocumentHelper.parseText(xmlStr);
		} catch (DocumentException e) {
			log.error("parse xml error:"+xmlStr, e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param document document
	 * @return String
	 */
	public static String toXml(Document document){
		
		return document.asXML();
	}

	public static Document createNewDoc(){
		return createNewDoc("GB2312");
	}
	public static Document createNewDoc(String encode){
		Document doc = DocumentHelper.createDocument();
		doc.setXMLEncoding(encode);
		return doc;
	}
	public static Document loadXml(String xmlPath){
		SAXReader sax = new SAXReader();
		try {
			return sax.read(new File(xmlPath));
		} catch (DocumentException e) {
			log.error("loadXml erorr:"+xmlPath, e);
			return null;
		}
	}
	public static Document loadClassPathXml(String xmlPath){
		SAXReader sax = new SAXReader();
		try {
			return sax.read(FileUtil.getResourceAsStream(xmlPath));
//			return sax.read(ClassLoader.getSystemResourceAsStream(xmlPath));
//			return sax.read(XmlUtil.class.getClass().getResourceAsStream(xmlPath));
		} catch (DocumentException e) {
			log.error("loadXml erorr:"+xmlPath, e);
			return null;
		}
	}
	
	public static void main(String[] args){
		System.out.println(createNewDoc().asXML());
	}
}


