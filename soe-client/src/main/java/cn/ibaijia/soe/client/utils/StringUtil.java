package cn.ibaijia.soe.client.utils;


public class StringUtil {
	
	public static String replace(String str,String oldChar,String newChar){
		return str == null?null:str.replace(oldChar, newChar);
	}

	public static boolean isBlank(String str){
		if(str == null){
			return true;
		}else{
			return isEmpty(str.trim());
		}
	}

	public static boolean isEmpty(Object object){
		if(object == null){
			return true;
		}else{
			return isEmpty(object.toString());
		}
	}
	public static boolean isEmpty(String str){
		if(str == null){
			return true;
		}else{
			return str.length()==0;
		}
	}
}
