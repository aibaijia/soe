package cn.ibaijia.soe.client.protocol;

import cn.ibaijia.isocket.protocol.Protocol;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class SoeProtocol implements Protocol<ByteBuffer,SoeObject> {

    private static Logger logger = LoggerFactory.getLogger(SoeProtocol.class);

    @Override
    public SoeObject decode(ByteBuffer data, Session session) {
        if(data == null){
            return null;
        }
        SoeObject soeObject = new SoeObject(data);
        logger.info("recv:{}",soeObject.getLogStr());
        return soeObject;
    }

    @Override
    public ByteBuffer encode(SoeObject data, Session session) {
        if(data == null){
            return null;
        }
        logger.info("send:{}",data.getLogStr());
        return data.getBuffer();
    }
}
