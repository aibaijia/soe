package cn.ibaijia.soe.client.message;

import java.util.List;

public class AppInfo {

    public Short id;

    /**
     * 如果是 openAll == true 可以不用判断serviceIds
     */
    public boolean openAll;

    /**
     * openAll == false时需要判断
     */
    public List<Short> serviceIds;


}
