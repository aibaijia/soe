package cn.ibaijia.soe.client.callback;

import cn.ibaijia.soe.client.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 创建监听一次，一直等待
 */
public abstract class SoeListenerCallBack extends SoeCallBack {
    private Logger logger = LoggerFactory.getLogger(SoeListenerCallBack.class);

    protected boolean recv(SoeObject soeObject){

        return true;
    }

}
