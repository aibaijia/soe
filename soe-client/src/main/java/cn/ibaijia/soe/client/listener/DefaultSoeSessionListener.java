package cn.ibaijia.soe.client.listener;

import cn.ibaijia.isocket.listener.DefaultSessionListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.client.session.SoeSession;
import cn.ibaijia.soe.client.session.SoeSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultSoeSessionListener extends DefaultSessionListener implements SoeSessionListener {
    private static final Logger logger = LoggerFactory.getLogger(DefaultSoeSessionListener.class);

    private SoeLoginSuccessListener soeLoginSuccessListener = new SoeLoginSuccessListener() {
        @Override
        public void run(SoeSession session) {

        }
    };

    private SoeLoginFailedListener soeLoginFailedListener = new SoeLoginFailedListener() {
        @Override
        public void run(Short appId) {

        }
    };

    private SoeClosedListener soeClosedListener = new SoeClosedListener() {
        @Override
        public void run(SoeSession session) {

        }
    };

    @Override
    public void loginSuccess(SoeSession session) {
        logger.info("loginSuccess:{}", session.getFullName());
        soeLoginSuccessListener.run(session);
    }

    @Override
    public void loginFailed(Short appId) {
        logger.info("loginFailed:{}", appId);
        soeLoginFailedListener.run(appId);
    }

    @Override
    public void closed(SoeSession session) {
        logger.info("SoeSession closed:{}", session.getFullName());
        soeClosedListener.run(session);
    }

    @Override
    public void closed(Session session) {
        logger.info("Session closed:{}", session.getSessionID());
//        this.closed(SoeSessionManager.get(session));
    }

    @Override
    public void readComplete(Session session, int readSize) {
        super.readComplete(session, readSize);
    }

    @Override
    public void writeComplete(Session session, int writeSize) {
        super.writeComplete(session, writeSize);
    }

    @Override
    public void setSoeLoginSuccessListener(SoeLoginSuccessListener listener) {
        this.soeLoginSuccessListener = listener;
    }

    @Override
    public void setSoeLoginFailedListener(SoeLoginFailedListener listener) {
        this.soeLoginFailedListener = listener;
    }

    @Override
    public void setSoeClosedListener(SoeClosedListener listener) {
        this.soeClosedListener = listener;
    }
}
