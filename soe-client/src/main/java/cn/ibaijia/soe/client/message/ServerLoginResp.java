package cn.ibaijia.soe.client.message;


import java.util.List;

public class ServerLoginResp {

    public boolean status;

    /**
     * 登录成功的sn,同一个app内 唯一
     */
    public byte sn;

    /**
     * 备用服务器信息 前端服务器挂了，自动去连下一个服务器。
     */
    public List<HostInfo> backupHost;

    /**
     * 当前可访问的appInfos
     */
    public List<AppInfo> appInfos;

}
