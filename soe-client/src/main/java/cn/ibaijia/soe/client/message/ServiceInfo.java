package cn.ibaijia.soe.client.message;

/**
 * tableName:service_info_t
 */

public class ServiceInfo {

    public Short appId;
    public Short serviceId;
    public String name;
}