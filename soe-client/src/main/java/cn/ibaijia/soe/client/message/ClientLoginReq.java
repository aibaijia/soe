package cn.ibaijia.soe.client.message;

import java.util.List;

public class ClientLoginReq {

    public Short appId;
    /**
     * md5(appKey+randomKey)
     */
    public String appKey;


    public List<ServiceInfo> serviceInfos;

}
