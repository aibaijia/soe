package cn.ibaijia.soe.demo.test;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class Proxy2SourceProcessor implements Processor<ByteBuffer> {

    private static final Logger logger = LoggerFactory.getLogger(Proxy2SourceProcessor.class);

    @Override
    public boolean process(Session session, ByteBuffer msg) {
//        logger.debug("recv:{}", JSONObject.toJSONString(msg.array()));
        Session sourceSession = (Session) session.getAttribute("sourceSession");
        if (sourceSession == null) {//识别代理协议 创建proxyClient
            logger.error("sourceSession null.");
        } else {
            logger.info("recv:{}", msg.remaining());
            sourceSession.write(msg);
        }
        return true;
    }

    @Override
    public void processError(Session session, ByteBuffer object, Throwable throwable) {

    }

}
