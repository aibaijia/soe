package cn.ibaijia.soe.demo;


import cn.ibaijia.soe.router.Router;

public class RouterStarter {

    public static void main(String[] args) {
        String host = "127.0.0.1";
        int port = 5664;
        short consoleId = 1;
        String consoleKey = "111111";
        String consoleIp = "127.0.0.1";

        Router router = new Router(host, port, consoleId, consoleKey, consoleIp);
        router.start();

    }

}
