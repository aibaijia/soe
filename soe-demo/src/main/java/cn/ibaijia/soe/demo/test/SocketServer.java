package cn.ibaijia.soe.demo.test;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;

public class SocketServer {

    public static void main(String[] args) {
        java.security.Security.setProperty("networkaddress.cache.ttl", "86400");
        Server server = new Server("127.0.0.1", 9080);
        server.addProtocol(new ByteBufferProtocol());
        server.setProcessor(new Source2ProxyProcessor());
        server.setSessionListener(new SourceServerListener());
//        server.setUseCompactQueue(true);
        server.setThreadNumber(10);
//        server.showStatus(5000);
        server.start();
    }


}