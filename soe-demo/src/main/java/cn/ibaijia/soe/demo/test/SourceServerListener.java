package cn.ibaijia.soe.demo.test;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.listener.DefaultSessionListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class SourceServerListener extends DefaultSessionListener {
    private static final Logger logger = LoggerFactory.getLogger(SourceServerListener.class);

    @Override
    public void readError(Session session, ByteBuffer byteBuffer, Throwable e) {
        logger.error("readError:{}", session.getSessionID(), e.getCause());
    }

    @Override
    public void closed(Session session) {
        try {
            Client proxyClient = (Client) session.getAttribute("proxyClient");
            if (proxyClient != null) {
                logger.debug("SourceServerListener close proxyClient.");
                proxyClient.close();
            }
        } catch (Exception e) {
            logger.error("SourceServerListener close proxyClient error.", e);
        }
    }
}
