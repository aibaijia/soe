package cn.ibaijia.soe.demo.test;

import cn.ibaijia.isocket.listener.DefaultSessionListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class ProxyClientListener extends DefaultSessionListener {
    private static final Logger logger = LoggerFactory.getLogger(ProxyClientListener.class);


    @Override
    public void readError(Session session, ByteBuffer byteBuffer, Throwable e) {
        logger.error("readError:{}", session.getSessionID(), e.getCause());
    }

    @Override
    public void closed(Session session) {
        try {
            Session sourceSession = (Session) session.getAttribute("sourceSession");
            if (sourceSession != null) {
                logger.debug("ProxyClientListener close sourceSession.");
                SessionManager.close(sourceSession);
            }
        } catch (Exception e) {
            logger.error("ProxyClientListener close sourceSession error.", e);
        }
    }
}
