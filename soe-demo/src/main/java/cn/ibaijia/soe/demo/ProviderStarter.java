package cn.ibaijia.soe.demo;

import cn.ibaijia.soe.client.SoeClient;
import cn.ibaijia.soe.client.listener.SoeLoginFailedListener;
import cn.ibaijia.soe.client.listener.SoeLoginSuccessListener;
import cn.ibaijia.soe.client.session.SoeSession;
import cn.ibaijia.soe.demo.providers.HelloService;
import cn.ibaijia.soe.demo.providers.TestFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

public class ProviderStarter {
    private static final Logger logger = LoggerFactory.getLogger(ProviderStarter.class);
    private static final CountDownLatch countDownLatch = new CountDownLatch(1);

    public static void main(String[] args) {
        String host = "47.105.108.150";
//        String host = "127.0.0.1";
        int port = 5664;
        Short appId = 2;
        String appKey = "111111";
        final SoeClient soeClient = new SoeClient(host,port,appId,appKey);
        soeClient.setSoeLoginSuccessListener(new SoeLoginSuccessListener() {
            @Override
            public void run(SoeSession session) {
                logger.info("login ok");
                countDownLatch.countDown();
            }
        });
        soeClient.setSoeLoginFailedListener(new SoeLoginFailedListener() {
            @Override
            public void run(Short appId) {
                logger.info("login not ok");
                countDownLatch.countDown();
            }
        });
        soeClient.addService(new HelloService((short)2001,"哈罗"));
        soeClient.addService(new TestFileService((short)2,"文件测试"));
        soeClient.start();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                soeClient.notifyReconnect();
//            }
//        }, 2000, 3000);
    }

}
