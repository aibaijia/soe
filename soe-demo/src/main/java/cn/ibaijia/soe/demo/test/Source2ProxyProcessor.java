package cn.ibaijia.soe.demo.test;

import cn.ibaijia.isocket.Client;
import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.protocol.ByteBufferProtocol;
import cn.ibaijia.isocket.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class Source2ProxyProcessor implements Processor<ByteBuffer> {

    private static final Logger logger = LoggerFactory.getLogger(Source2ProxyProcessor.class);

    private static final int SOCKS_PROTOCOL_4 = 0X04;
    private static final int SOCKS_PROTOCOL_5 = 0X05;
    private static final int DEFAULT_BUFFER_SIZE = 1024;
    private static final byte TYPE_IPV4 = 0x01;
    private static final byte TYPE_IPV6 = 0X02;
    private static final byte TYPE_HOST = 0X03;
    private static final byte ALLOW_PROXY = 0X5A;
    private static final byte DENY_PROXY = 0X5B;

    @Override
    public boolean process(Session session, ByteBuffer msg) {
//        logger.debug("recv:{}", JSONObject.toJSONString(msg.array()));
        Client proxyClient = (Client) session.getAttribute("proxyClient");
        if (proxyClient == null) {//识别代理协议 创建proxyClient
            //从协议头中获取socket的类型
            if (msg.hasRemaining()) {
                Integer socksType = null;
                try {
                    //先取session中的socksType
                    socksType = (Integer) session.getAttribute("socksType");
                    if (socksType != null && socksType == 5) {//socks5 第二步
                        proxyClient = convertToSocket5Step2(session, msg);
                    } else {
                        int protocol = msg.get();
                        if (SOCKS_PROTOCOL_4 == protocol) {
                            socksType = 4;
                            session.setAttribute("socksType", socksType);
                            proxyClient = convertToSocket4(session, msg);
                        } else if (SOCKS_PROTOCOL_5 == protocol) {
                            socksType = 5;
                            session.setAttribute("socksType", socksType);
                            convertToSocket5Step1(session, msg);
                            return true;
                        } else {
                            logger.info("socks协议,不是Socket4或者Socket5");
                        }
                    }
                } catch (Exception e) {
                    logger.error("创建代理出错.", e);
                } finally {
                    msg.clear();
                }
                if (null != proxyClient) {
                    session.setAttribute("proxyClient", proxyClient);
                }
                logger.info("创建代理,socksType:{} ,status:{} ", socksType, (null != proxyClient));
            } else {
                logger.info("msg buffer empty.");
            }
        } else {
            //socket转换
            logger.info("send:{}",msg.remaining());
            proxyClient.getSession().write(msg);
        }
        return true;
    }

    @Override
    public void processError(Session session, ByteBuffer object, Throwable throwable) {

    }


    private String getSocks4Host(byte type, ByteBuffer byteBuffer) throws IOException {
        byte[] tmp = new byte[byteBuffer.remaining() - 1];
        byteBuffer.get(tmp);
        String host = new String(tmp);
        return host;
    }

    private String getHost(byte type, ByteBuffer byteBuffer) throws IOException {
        String host = null;
        byte[] tmp = null;
        switch (type) {
            case TYPE_IPV4:
                tmp = new byte[4];
                byteBuffer.get(tmp);
                host = InetAddress.getByAddress(tmp).getHostAddress();
                break;
            case TYPE_IPV6:
                tmp = new byte[16];
                byteBuffer.get(tmp);
                host = InetAddress.getByAddress(tmp).getHostAddress();
                break;
            case TYPE_HOST:
                int count = byteBuffer.get();
                tmp = new byte[count];
                byteBuffer.get(tmp);
                host = new String(tmp);
            default:
                break;
        }
        return host;
    }

    /**
     * 1) CONNECT
     * <p>
     * The client connects to the SOCKS server and sends a CONNECT request when
     * it wants to establish a connection to an application server. The client
     * includes in the request packet the IP address and the port number of the
     * destination host, and userid, in the following format.
     * <p>
     * +----+----+----+----+----+----+----+----+----+----+....+----+
     * | VN | CD | DSTPORT |      DSTIP        | USERID       |NULL|
     * +----+----+----+----+----+----+----+----+----+----+....+----+
     * # of bytes:	   1    1      2              4           variable       1
     * <p>
     * VN is the SOCKS protocol version number and should be 4. CD is the
     * SOCKS command code and should be 1 for CONNECT request. NULL is a byte
     * of all zero bits.
     * <p>
     * The SOCKS server checks to see whether such a request should be granted
     * based on any combination of source IP address, destination IP address,
     * destination port number, the userid, and information it may obtain by
     * consulting IDENT, cf. RFC 1413.  If the request is granted, the SOCKS
     * server makes a connection to the specified port of the destination host.
     * A reply packet is sent to the client when this connection is established,
     * or when the request is rejected or the operation fails.
     * <p>
     * +----+----+----+----+----+----+----+----+
     * | VN | CD | DSTPORT |      DSTIP        |
     * +----+----+----+----+----+----+----+----+
     * # of bytes:	   1    1      2              4
     * <p>
     * VN is the version of the reply code and should be 0. CD is the result
     * code with one of the following values:
     * <p>
     * 90: request granted
     * 91: request rejected or failed
     * 92: request rejected becasue SOCKS server cannot connect to
     * identd on the client
     * 93: request rejected because the client program and identd
     * report different user-ids
     * <p>
     * The remaining fields are ignored.
     * <p>
     * The SOCKS server closes its connection immediately after notifying
     * the client of a failed or rejected request. For a successful request,
     * the SOCKS server gets ready to relay traffic on both directions. This
     * enables the client to do I/O on its connection as if it were directly
     * connected to the application server.
     *
     * @param session
     * @param msg
     * @return
     * @throws IOException
     */
    private Client convertToSocket4(Session session, ByteBuffer msg) throws IOException {
        //socks4 协议 http://ftp.icm.edu.pl/packages/socks/socks4/SOCKS4.protocol
        Client proxyClient = null;
        byte[] tmp = new byte[8];
        msg.get(tmp);
        // 请求协议|VN1|CD1|DSTPORT2|DSTIP4|NULL1|
        int port = ByteBuffer.wrap(tmp, 1, 2).asShortBuffer().get() & 0xFFFF;
        String host = getSocks4Host((byte) 0x01, msg);
        logger.info("host:{}", host);
        msg.get();
        //返回一个8字节的响应协议: |VN1|CD1|DSTPORT2|DSTIP 4|
        byte[] response = new byte[8];
        try {
//            proxySocket = new Socket(host, port);
            proxyClient = new Client(host, port);
            proxyClient.addProtocol(new ByteBufferProtocol());
            proxyClient.setProcessor(new Proxy2SourceProcessor());
            proxyClient.setSessionListener(new ProxyClientListener());
//            proxyClient.showStatus();
            boolean res = proxyClient.start();
//            proxyClient.showStatus(5000);
            if (res) {
                proxyClient.getSession().setAttribute("sourceSession", session);
                response[1] = ALLOW_PROXY;
                logger.info("connect " + tmp[1] + "host: " + host + " ,port: " + port);
            } else {
                response[1] = DENY_PROXY;
                logger.info("connect false,host: " + host + " ,port: " + port);
            }
        } catch (Exception e) {
            response[1] = DENY_PROXY;
            logger.info("connect error,host: " + host + " ,port: " + port);
        }
        session.write(ByteBuffer.wrap(response));
        return proxyClient;
    }

    /**
     * When a TCP-based client wishes to establish a connection to an object
     * that is reachable only via a firewall (such determination is left up
     * to the implementation), it must open a TCP connection to the
     * appropriate SOCKS port on the SOCKS server system.  The SOCKS service
     * is conventionally located on TCP port 1080.  If the connection
     * request succeeds, the client enters a negotiation for the
     * authentication method to be used, authenticates with the chosen
     * method, then sends a relay request.  The SOCKS server evaluates the
     * request, and either establishes the appropriate connection or denies
     * it.
     * <p>
     * Unless otherwise noted, the decimal numbers appearing in packet-
     * format diagrams represent the length of the corresponding field, in
     * octets.  Where a given octet must take on a specific value, the
     * syntax X'hh' is used to denote the value of the single octet in that
     * field. When the word 'Variable' is used, it indicates that the
     * corresponding field has a variable length defined either by an
     * associated (one or two octet) length field, or by a data type field.
     * <p>
     * The client connects to the server, and sends a version
     * identifier/method selection message:
     * <p>
     * +----+----------+----------+
     * |VER | NMETHODS | METHODS  |
     * +----+----------+----------+
     * | 1  |    1     | 1 to 255 |
     * +----+----------+----------+
     * <p>
     * The VER field is set to X'05' for this version of the protocol.  The
     * NMETHODS field contains the number of method identifier octets that
     * appear in the METHODS field.
     * <p>
     * The server selects from one of the methods given in METHODS, and
     * sends a METHOD selection message:
     * <p>
     * +----+--------+
     * |VER | METHOD |
     * +----+--------+
     * | 1  |   1    |
     * +----+--------+
     * <p>
     * If the selected METHOD is X'FF', none of the methods listed by the
     * client are acceptable, and the client MUST close the connection.
     * <p>
     * The values currently defined for METHOD are:
     * <p>
     * o  X'00' NO AUTHENTICATION REQUIRED
     * o  X'01' GSSAPI
     * o  X'02' USERNAME/PASSWORD
     * o  X'03' to X'7F' IANA ASSIGNED
     * o  X'80' to X'FE' RESERVED FOR PRIVATE METHODS
     * o  X'FF' NO ACCEPTABLE METHODS
     * <p>
     * The client and server then enter a method-specific sub-negotiation.
     *
     * @param session
     * @param msg
     * @throws IOException
     */
    private void convertToSocket5Step1(Session session, ByteBuffer msg) throws IOException {
        //Socks5 协议: https://www.ietf.org/rfc/rfc1928.txt
        byte[] tmp = new byte[2];
        msg.get(tmp);
        byte method = tmp[1];
        if (0x02 == tmp[0]) {
            method = 0x00;// X'00' NO AUTHENTICATION REQUIRED
            msg.get();
        }
        tmp = new byte[]{0x05, method};
        session.write(ByteBuffer.wrap(tmp));
    }

    /**
     * 4.  Requests
     * <p>
     * Once the method-dependent subnegotiation has completed, the client
     * sends the request details.  If the negotiated method includes
     * encapsulation for purposes of integrity checking and/or
     * confidentiality, these requests MUST be encapsulated in the method-
     * dependent encapsulation.
     * <p>
     * The SOCKS request is formed as follows:
     * <p>
     * +----+-----+-------+------+----------+----------+
     * |VER | CMD |  RSV  | ATYP | DST.ADDR | DST.PORT |
     * +----+-----+-------+------+----------+----------+
     * | 1  |  1  | X'00' |  1   | Variable |    2     |
     * +----+-----+-------+------+----------+----------+
     * <p>
     * Where:
     * <p>
     * o  VER    protocol version: X'05'
     * o  CMD
     * o  CONNECT X'01'
     * o  BIND X'02'
     * o  UDP ASSOCIATE X'03'
     * o  RSV    RESERVED
     * o  ATYP   address type of following address
     * o  IP V4 address: X'01'
     * o  DOMAINNAME: X'03'
     * o  IP V6 address: X'04'
     * o  DST.ADDR       desired destination address
     * o  DST.PORT desired destination port in network octet
     * order
     * <p>
     * The SOCKS server will typically evaluate the request based on source
     * and destination addresses, and return one or more reply messages, as
     * appropriate for the request type.
     * <p>
     * 6.  Replies
     * <p>
     * The SOCKS request information is sent by the client as soon as it has
     * established a connection to the SOCKS server, and completed the
     * authentication negotiations.  The server evaluates the request, and
     * returns a reply formed as follows:
     * <p>
     * +----+-----+-------+------+----------+----------+
     * |VER | REP |  RSV  | ATYP | BND.ADDR | BND.PORT |
     * +----+-----+-------+------+----------+----------+
     * | 1  |  1  | X'00' |  1   | Variable |    2     |
     * +----+-----+-------+------+----------+----------+
     * <p>
     * Where:
     * <p>
     * o  VER    protocol version: X'05'
     * o  REP    Reply field:
     * o  X'00' succeeded
     * o  X'01' general SOCKS server failure
     * o  X'02' connection not allowed by ruleset
     * o  X'03' Network unreachable
     * o  X'04' Host unreachable
     * o  X'05' Connection refused
     * o  X'06' TTL expired
     * o  X'07' Command not supported
     * o  X'08' Address type not supported
     * o  X'09' to X'FF' unassigned
     * o  RSV    RESERVED
     * o  ATYP   address type of following address
     *
     * @param session
     * @param msg
     * @return
     * @throws IOException
     */
    private Client convertToSocket5Step2(Session session, ByteBuffer msg) throws IOException {
        byte cmd = 0;
        byte[] tmp = new byte[4];
        msg.get(tmp);
        logger.debug("convertToSocket5Step2 header :" + Arrays.toString(tmp));
        cmd = tmp[1];
        String host = getHost(tmp[3], msg);
        tmp = new byte[2];
        msg.get(tmp);
        int port = ByteBuffer.wrap(tmp).asShortBuffer().get() & 0xFFFF;
        logger.info("connect host: " + host + " :port:" + port);
        byte rep = (byte) 0x00; //succeeded
        try {
            if (0x01 == cmd) {//TCP
            } else if (0x02 == cmd) {//UDP TODO
//                resultTmp = new ServerSocket(port);
            } else {
                rep = 0x05;
            }
        } catch (Exception e) {
            rep = 0x05;
        }
        Client proxyClient = new Client(host, port);
        proxyClient.addProtocol(new ByteBufferProtocol());
        proxyClient.setProcessor(new Proxy2SourceProcessor());
        proxyClient.setSessionListener(new ProxyClientListener());
//        proxyClient.showStatus();
        boolean res = proxyClient.start();
        if (res) {
            proxyClient.getSession().setAttribute("sourceSession", session);
        } else {
            rep = 0x05;
        }

        ByteBuffer respBuff = ByteBuffer.allocate(10);
        respBuff.put((byte) 0x05);
        respBuff.put(rep);
        respBuff.put((byte) 0x00);
        respBuff.put((byte) 0x01);
//        respBuff.put(session.getChannel().socket().getLocalAddress().getAddress());
//        Short localPort = (short) ((session.getChannel().socket().getLocalPort()) & 0xFFFF);
//        respBuff.putShort(localPort);
        respBuff.flip();
        session.write(respBuff);
        return proxyClient;
    }

}
