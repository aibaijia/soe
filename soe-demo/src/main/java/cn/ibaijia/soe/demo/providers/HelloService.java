package cn.ibaijia.soe.demo.providers;


import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloService extends BaseService {
    private static Logger log = LoggerFactory.getLogger(HelloService.class);

    public HelloService(short id, String name) {
        super(id, name);
    }

    @Override
    public void doService(SoeObject soeObject) {
        log.info("recv:" + soeObject.getBodyAsString());
        SoeObject respDto = soeObject.makeResponse("i got it!1" + soeObject.getBodyAsString());
        response(respDto);
    }
}
