package cn.ibaijia.soe.demo;

import cn.ibaijia.soe.client.SoeClient;
import cn.ibaijia.soe.client.listener.SoeLoginFailedListener;
import cn.ibaijia.soe.client.listener.SoeLoginSuccessListener;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.session.SoeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerStarter {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerStarter.class);

    public static void main(String[] args) {
//        String host = "118.24.114.130";
        String host = "127.0.0.1";
        int port = 5664;
        Short appId = 3;
        String appKey = "111111";
        final SoeClient soeClient = new SoeClient(host, port, appId, appKey);
        soeClient.setSoeLoginSuccessListener(new SoeLoginSuccessListener() {
            @Override
            public void run(SoeSession session) {
                logger.info("login ok");
//                soeClient.send(toId,serviceId,"你好!");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        short toId = 2;
                        short serviceId = 2001;
                        SoeObject resp = soeClient.sendSync(toId, serviceId, 3, "你好!4");
                        System.out.println("111:" + resp.getBodyAsString());
                    }
                }).start();
            }
        });
        soeClient.setSoeLoginFailedListener(new SoeLoginFailedListener() {
            @Override
            public void run(Short appId) {
                logger.info("login not ok");
            }
        });
        soeClient.start();


//        short toId = 2;
//        short serviceId = 2001;
//        soeClient.send(toId,serviceId,"你好!");
//        SoeObject soeObject1 = soeClient.sendSync(toId,serviceId,123,"你好呀!");
//        System.out.println(soeObject1.getBodyAsString());
//        short serviceId2 = 2002;
//        SoeObject req = new SoeObject(toId,serviceId2);
//        String filePath = "E:\\piano\\拔萝卜.png";
//        req.bodyReadFromFile(filePath);
//        req.initBuffer();
//        SoeObject soeObject2 = soeClient.sendSync(req);
//        System.out.println(soeObject2.getBodyAsString());

    }

}
