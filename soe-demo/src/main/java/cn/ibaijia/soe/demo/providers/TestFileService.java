package cn.ibaijia.soe.demo.providers;


import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestFileService extends BaseService {
    private static Logger log = LoggerFactory.getLogger(TestFileService.class);

    public TestFileService(short id, String name) {
        super(id, name);
    }

    @Override
    public void doService(SoeObject soeObject) {
        String filepath = "E:\\a.jpg";
        soeObject.bodyWriteToFile(filepath);
        SoeObject respDto = soeObject.makeResponse("save path:" + filepath);
        response(respDto);
    }
}
