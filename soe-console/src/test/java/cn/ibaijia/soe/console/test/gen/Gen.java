package cn.ibaijia.soe.console.test.gen;

import cn.ibaijia.jsm.service.GenService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Gen {

    @Resource
    private GenService genService;

    @Test
    public void gen() {
         genService.gen("user_role_t", false);
         genService.gen("role_permission_t", false);
    }

}
