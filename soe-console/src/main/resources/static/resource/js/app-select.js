function renderTable(appId) {
    var url = "/soe-console/v1/app-info/page-list-service?appId=" + appId;
    WebJs.renderTable({
        elem: '#pageData'
        , url: url
        , title: '服务列表'
        , cols: [[
            {type: 'checkbox'}
            , {field: 'name', title: '名称', width: 200,}
            , {field: 'appId', title: 'APPID', width: 100,}
            , {
                field: 'authType', title: '授权类型', width: 100, templet: function (d) {
                    return d.authType == 1 ? '全部授权' : '部分授权';
                }
            }
            , {field: 'serviceInfoListHtml', title: '服务项', templet: "#serviceInfoList"}
        ]]
    });
    // setTimeout(function () {
    //     console.log($(".serviceFuncId").length);
    //     $(".serviceFuncId").change(function () {
    //         console.log($(this).attr("data"));
    //     });
    // },1000)

}

var authInfoList = [];
var appInfo;
var table;
layui.use(['table', 'form'], function () {
    table = layui.table;
    var form = layui.form;
    appInfo = sessionStorage.getItem("appInfo");
    appInfo = JSON.parse(appInfo);
    console.log(appInfo);
    getAuthInfoList(appInfo.appId, function () {
        renderTable(appInfo.appId);
    });
    //头工具栏事件
    table.on('tool(pageData)', function (obj) {
        switch (obj.event) {
            case 'config':
                selectServiceItem();
                break;
        }
    });

    table.on('checkbox(pageData)', function (obj) {
        console.log(obj);
        if (obj.type == "all") {//TODO
            var checkStatus = table.checkStatus('pageData')
                , listData = checkStatus.data;
            if (listData.length > 0) {
                for (var i in listData) {
                    var item = {};
                    item.consumerAppId = appInfo.appId;
                    item.serviceAppId = listData[i].appId;
                    item.serviceName = listData[i].name;
                    var authType = listData[i].authType;
                    if (authType != 1) {
                        item.serviceFuncIds =  listData[i].serviceFuncIds;
                    }
                    updateAuthInfo(item);
                }
            } else {
                // console.log(table.cache.pageData);
                listData = table.cache.pageData;
                for (var i in listData) {
                    var item = {};
                    item.consumerAppId = appInfo.appId;
                    item.serviceAppId = listData[i].appId;
                    item.serviceName = listData[i].name;
                    removeAuthInfo(item);
                }
            }

        } else {
            var item = {};
            item.consumerAppId = appInfo.appId;
            item.serviceAppId = obj.data.appId;
            item.serviceName = obj.data.name;
            var authType = obj.data.authType;
            if (authType != 1) {
                item.serviceFuncIds = obj.data.serviceFuncIds;
            }
            if (obj.checked) {//add to authInfoList
                addAuthInfo(item);
            } else {//remove from authInfoList
                removeAuthInfo(item);
            }
        }
    });

    //监听提交
    form.on('submit(saveConfig)', function () {
        saveAuthInfo(JSON.stringify(authInfoList), appInfo.appId);
        return false;
    });

});

function updateFuncIds(serviceAppId, serviceFuncId) {
    var listData = table.cache.pageData;
    var item = {};
    item.consumerAppId = appInfo.appId;
    item.serviceAppId = serviceAppId;
    for (var i in listData) {
        if (listData[i].appId == serviceAppId) {
            if (!listData[i].serviceFuncIds) {
                listData[i].serviceFuncIds = ",";
            }
            if (listData[i].serviceFuncIds.indexOf("," + serviceFuncId + ",") > -1) {//有
                listData[i].serviceFuncIds = listData[i].serviceFuncIds.replace("," + serviceFuncId + ",",",")
            } else {
                listData[i].serviceFuncIds = listData[i].serviceFuncIds + serviceFuncId + ",";
            }
            item.serviceName =  listData[i].name;
            item.serviceFuncIds = listData[i].serviceFuncIds;
        }
    }
    updateSelectedAuthInfo(item);
}

function getAuthInfoList(appId, cb) {
    var url = "/soe-console/v1/app-auth/auth/" + appId;
    WebJs.ajaxGet(url, {}, function (data) {
        console.log(data);
        authInfoList = data.result;
        cb();
    });
}

function addAuthInfo(item) {
    authInfoList.push(item);
}

function removeAuthInfo(item) {
    for (var i in authInfoList) {
        if (item.consumerAppId == authInfoList[i].consumerAppId && item.serviceAppId == authInfoList[i].serviceAppId) {
            authInfoList.splice(i, 1);
            return true;
        }
    }
    return false;
}

function updateAuthInfo(item) {
    removeAuthInfo(item);
    authInfoList.push(item);
}


function updateSelectedAuthInfo(item) {
    if(removeAuthInfo(item)){
        authInfoList.push(item);
    }
}


function saveAuthInfo(authInfoList, appId) {
    var url = "/soe-console/v1/app-auth/auth/" + appId;
    WebJs.ajaxPut(url, authInfoList, function (data) {
        console.log(data);
        layer.msg(data.message);
        if (data.code == '1001') {
            parent.location.href = 'app-list.html';
        }
    });
}

function selectServiceItem() {
    layer.open({
        type: 2,
        title: '选择服务项',
        shadeClose: true,
        shade: 0.8,
        area: ['450px', '420px'],
        content: '/soe-console/html/app-add.html'
    });
}