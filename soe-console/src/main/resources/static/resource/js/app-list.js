function renderTable() {
    var url = "/soe-console/v1/app-info/page-list";
    WebJs.renderTable({
        elem: '#pageData'
        , url: url
        , toolbar: '#toolBar'
        , title: '用户数据表'
        , cols: [[
            {field: 'name', title: '名称', width: 200,}
            , {field: 'appId', title: 'APPID', width: 80,}
            , {field: 'password', title: '密码', width: 100}
            , {
                field: 'type', title: '应用类型', width: 120, templet: function (d) {
                    return d.type == 1 ? '消费者' : '生产者';
                }
            }
            , {
                field: 'status', title: '状态', width: 120, templet: function (d) {
                    return d.status == 0 ? '禁用' : '启用';
                }
            }
            , {
                field: 'authType', title: '授权类型', width: 120, templet: function (d) {
                    return d.authType == 1 ? '全部授权' : '部分授权';
                }
            }
            , {field: 'createTime', title: '创建时间', width: 200}
            , {fixed: 'right', title: '操作', toolbar: '#optBar'}
        ]]
    });
}

layui.use('table', function () {
    var table = layui.table;
    renderTable();
    //头工具栏事件
    table.on('toolbar(pageData)', function (obj) {
        switch (obj.event) {
            case 'addApp':
                addAppInfo();
                break;
            case 'syncAll':
                syncAll();
                break;
            case 'reconnect':
                reconnect();
                break;
        }
        ;
    });

    //监听行工具事件
    table.on('tool(pageData)', function (obj) {
        var data = obj.data;
        console.log(data)
        if (obj.event === 'sync') {
            layer.confirm('确认同步吗', function (index) {
                syncApp(data.id);
            });
        } else if (obj.event === 'edit') {
            sessionStorage.setItem("appInfo", JSON.stringify(data));
            editAppInfo();
        } else if (obj.event === 'config') {
            sessionStorage.setItem("appInfo", JSON.stringify(data));
            configService();
        }
    });
});

function configService() {
    layer.open({
        type: 2,
        title: '配置服务',
        shadeClose: true,
        shade: 0.8,
        area: ['1000px', '520px'],
        // btn: ['确定', '关闭'],
        // btnAlign: 'l',
        // yes: function () {
        //     layui.use('table', function () {
        //         var table = layui.table;
        //         var checkStatus = table.checkStatus('pageData');
        //         console.log();
        //     });
        // },
        // btn2: function () {
        //     layer.closeAll();
        // },
        content: '/soe-console/html/app-select.html'
    });
}

function addAppInfo() {
    layer.open({
        type: 2,
        title: '添加应用',
        shadeClose: true,
        shade: 0.8,
        area: ['450px', '420px'],
        content: '/soe-console/html/app-add.html'
    });
}

function editAppInfo() {
    layer.open({
        type: 2,
        title: '修改应用',
        shadeClose: true,
        shade: 0.8,
        area: ['450px', '420px'],
        content: '/soe-console/html/app-edit.html'
    });
}

function syncAll() {
    var url = "/soe-console/v1/app-info/sync-all";
    WebJs.ajaxPut(url, {}, function (data) {
        console.log(data);
        layer.msg(data.message)
    })
}

function syncApp(id) {
    var url = "/soe-console/v1/app-info/sync/" + id;
    WebJs.ajaxPut(url, {}, function (data) {
        console.log(data);
        layer.msg(data.message)
    })
}

function reconnect() {
    var url = "/soe-console/v1/app-info/reconnect";
    WebJs.ajaxPut(url, {}, function (data) {
        console.log(data);
        layer.msg(data.message)
    })
}