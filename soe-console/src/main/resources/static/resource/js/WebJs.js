var WebJs = {
    ajaxPost: function (url, data, callback) {
        $.ajax({
            url: url,
            type: "POST",
            data: data ? data : {},
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (data != null && (data.code == '1101' || data.code == '1201')) {
                    WebJs.toLogin();
                }
                callback(data);
            }
        });
    },
    ajaxPut: function (url, data, callback) {
        $.ajax({
            url: url,
            type: "PUT",
            data: data ? data : {},
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (data != null && (data.code == '1101' || data.code == '1201')) {
                    WebJs.toLogin();
                }
                callback(data);
            }
        });
    },
    ajaxGet: function (url, data, callback) {
        $.ajax({
            url: url,
            type: "GET",
            data: data ? data : {},
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (data != null && (data.code == '1101' || data.code == '1201')) {
                    WebJs.toLogin();
                }
                callback(data);
            }
        });
    },
    ajaxDelete: function (url, data, callback) {
        $.ajax({
            url: url,
            type: "DELETE",
            data: data ? data : {},
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                if (data != null && (data.code == '1101' || data.code == '1201')) {
                    WebJs.toLogin();
                }
                callback(data);
            }
        });
    },
    toLogin: function () {
        if (parent) {
            parent.location.href = '/soe-console/html/login.html';
        } else {
            location.href = '/soe-console/html/login.html';
        }
    },
    renderTable: function (options) {
        layui.use('table', function () {
            var table = layui.table;
            table.render({
                elem: options.elem
                , url: options.url
                , toolbar: options.toolbar
                , title: options.title
                , cols: options.cols
                , page: true
                , request: {
                    pageName: 'pageNo',
                    limitName: 'pageSize'
                }
                , response: {
                    statusCode: 1001 //重新规定成功的状态码为 200，table 组件默认为 0
                }
                , parseData: function (data) { //将原始数据解析成 table 组件所规定的数据
                    if (data != null && (data.code == '1101' || data.code == '1201')) {
                        WebJs.toLogin();
                    }
                    var page = data.result;
                    var count = page ? page.totalCount : 0;
                    var list = page ? page.list : [];
                    return {
                        "code": data.code, //解析接口状态
                        "msg": data.message, //解析提示文本
                        "count": count, //解析数据长度
                        "data": list //解析数据列表
                    };
                }

            });

        });
    }
}