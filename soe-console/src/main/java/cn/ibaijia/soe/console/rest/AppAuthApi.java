package cn.ibaijia.soe.console.rest;

import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.dao.model.AppAuth;
import cn.ibaijia.soe.console.dao.model.AppInfo;
import cn.ibaijia.soe.console.rest.req.AppAuthReq;
import cn.ibaijia.soe.console.rest.req.AppInfoReq;
import cn.ibaijia.soe.console.rest.req.AppInfoUpdateReq;
import cn.ibaijia.soe.console.service.AppAuthService;
import cn.ibaijia.soe.console.service.AppInfoService;
import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.OptLogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/v1/app-auth")
public class AppAuthApi extends BaseRest {

    @Resource
    private AppAuthService appAuthService;

    @ApiOperation(value = "设置AppAuth", notes = "")
    @RequestMapping(value = "/auth/{appId}",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]设置AppAuth[#{logContent}][#{logStatus}]")
    public RestResp<Integer> auth(@PathVariable("appId") Short appId, @RequestBody List<AppAuthReq> req) {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent(StringUtil.toJson(req));
        RestResp<Integer> resp = new RestResp<>();
        resp.result = appAuthService.auth(appId,req);
        OptLogUtil.setLogStatus("成功");
        return resp;
    }

    @ApiOperation(value = "获取AppAuth", notes = "")
    @RequestMapping(value = "/auth/{appId}",method = RequestMethod.GET)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.READ)
    public RestResp<List<AppAuth>> getAuthInfo(@PathVariable("appId") Short appId) {
        RestResp<List<AppAuth>> resp = new RestResp<>();
        resp.result = appAuthService.findByConsumerId(appId);
        return resp;
    }

}
