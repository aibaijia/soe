package cn.ibaijia.soe.console.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;
import cn.ibaijia.jsm.utils.DateUtil;

import java.util.Date;

/**
 * tableName:user_info_t
 */
public class UserInfo extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:真实姓名
	 */
    @FieldAnn(required = false, maxLen = 50, name = "name", comments = "真实姓名")
	public String name;
	/**
	 * defaultVal:NULL
	 * comments:昵称
	 */
    @FieldAnn(required = false, maxLen = 50, name = "nickname", comments = "昵称")
	public String nickname;
	/**
	 * defaultVal:NULL
	 * comments:联系电话
	 */
    @FieldAnn(required = false, maxLen = 50, name = "mobile", comments = "联系电话")
	public String mobile;
	/**
	 * defaultVal:NULL
	 * comments:头像
	 */
    @FieldAnn(required = false, maxLen = 200, name = "headImg", comments = "头像")
	public String headImg;
	/**
	 * defaultVal:NULL
	 * comments:QQ
	 */
    @FieldAnn(required = false, maxLen = 50, name = "qq", comments = "QQ")
	public String qq;
	/**
	 * defaultVal:NULL
	 * comments:微信
	 */
    @FieldAnn(required = false, maxLen = 50, name = "wechat", comments = "微信")
	public String wechat;
	/**
	 * defaultVal:NULL
	 * comments:邮箱
	 */
    @FieldAnn(required = false, maxLen = 50, name = "email", comments = "邮箱")
	public String email;
	/**
	 * defaultVal:NULL
	 * comments:1 男\r\n            2 女
	 */
    @FieldAnn(required = false, name = "gender", comments = "1 男\r\n            2 女")
	public Integer gender;
	/**
	 * defaultVal:NULL
	 * comments:职位
	 */
    @FieldAnn(required = false, maxLen = 50, name = "title", comments = "职位")
	public String title;
	/**
	 * defaultVal:NULL
	 * comments:上次登录时间
	 */
    @FieldAnn(required = false, name = "loginTime", comments = "上次登录时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
	public Date loginTime;
	/**
	 * defaultVal:NULL
	 * comments:创建时间
	 */
    @FieldAnn(required = false, name = "createTime", comments = "创建时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
	public Date createTime;
	/**
	 * defaultVal:NULL
	 * comments:更新时间
	 */
    @FieldAnn(required = false, name = "updateTime", comments = "更新时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
	public Date updateTime;
	/**
	 * defaultVal:NULL
	 * comments:创建人ID
	 */
    @FieldAnn(required = false, name = "createUserId", comments = "创建人ID")
	public Long createUserId;
	/**
	 * defaultVal:NULL
	 * comments:修改人ID
	 */
    @FieldAnn(required = false, name = "updateUserId", comments = "修改人ID")
	public Long updateUserId;
	/**
	 * defaultVal:NULL
	 * comments:身份证号
	 */
    @FieldAnn(required = false, maxLen = 50, name = "idCardNo", comments = "身份证号")
	public String idCardNo;
}