package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.Permission;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:permission_t
 */
public interface PermissionMapper {

	/**
	 * 新增Permission
	 */
	@Insert("<script>insert into permission_t (name,code) values (#{name},#{code})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Permission permission);

	/**
	 * 批量新增Permission
	 */
	@Insert("<script>insert into permission_t (name,code) values <foreach collection='list' item='item' index='index' separator=','> (#{item.name},#{item.code})</foreach></script>")
	int addBatch(List<Permission> permissionList);

	/**
	 * 修改Permission(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update permission_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.code,code)'>,code=#{code}</if> where `id`=#{id}</script>")
	int update(Permission permission);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from permission_t t where t.id=#{id}</script>")
	Permission findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from permission_t t where t.id=#{id}</script>")
	Permission findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from permission_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from permission_t t order by t.id desc</script>")
	List<Permission> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<Permission> pageList(Page<Permission> page);
	
}