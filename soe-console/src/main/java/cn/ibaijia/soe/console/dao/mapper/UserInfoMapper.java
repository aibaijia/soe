package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.UserInfo;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:user_info_t
 */
public interface UserInfoMapper {

	/**
	 * 新增UserInfo
	 */
	@Insert("<script>insert into user_info_t (name,nickname,mobile,headImg,qq,wechat,email,gender,title,loginTime,createTime,updateTime,createUserId,updateUserId,idCardNo) values (#{name},#{nickname},#{mobile},#{headImg},#{qq},#{wechat},#{email},#{gender},#{title},#{loginTime},#{createTime},#{updateTime},#{createUserId},#{updateUserId},#{idCardNo})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(UserInfo userInfo);

	/**
	 * 批量新增UserInfo
	 */
	@Insert("<script>insert into user_info_t (name,nickname,mobile,headImg,qq,wechat,email,gender,title,loginTime,createTime,updateTime,createUserId,updateUserId,idCardNo) values <foreach collection='list' item='item' index='index' separator=','> (#{item.name},#{item.nickname},#{item.mobile},#{item.headImg},#{item.qq},#{item.wechat},#{item.email},#{item.gender},#{item.title},#{item.loginTime},#{item.createTime},#{item.updateTime},#{item.createUserId},#{item.updateUserId},#{item.idCardNo})</foreach></script>")
	int addBatch(List<UserInfo> userInfoList);

	/**
	 * 修改UserInfo(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update user_info_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.nickname,nickname)'>,nickname=#{nickname}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.mobile,mobile)'>,mobile=#{mobile}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.headImg,headImg)'>,headImg=#{headImg}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.qq,qq)'>,qq=#{qq}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.wechat,wechat)'>,wechat=#{wechat}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.email,email)'>,email=#{email}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.gender,gender)'>,gender=#{gender}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.title,title)'>,title=#{title}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.loginTime,loginTime)'>,loginTime=#{loginTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.createTime,createTime)'>,createTime=#{createTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.updateTime,updateTime)'>,updateTime=#{updateTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.createUserId,createUserId)'>,createUserId=#{createUserId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.updateUserId,updateUserId)'>,updateUserId=#{updateUserId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.idCardNo,idCardNo)'>,idCardNo=#{idCardNo}</if> where `id`=#{id}</script>")
	int update(UserInfo userInfo);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from user_info_t t where t.id=#{id}</script>")
	UserInfo findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from user_info_t t where t.id=#{id}</script>")
	UserInfo findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from user_info_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from user_info_t t order by t.id desc</script>")
	List<UserInfo> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<UserInfo> pageList(Page<UserInfo> page);
	
}