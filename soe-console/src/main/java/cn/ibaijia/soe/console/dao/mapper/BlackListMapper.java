package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.BlackList;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:black_list_t
 */
public interface BlackListMapper {

	/**
	 * 新增BlackList
	 */
	@Insert("<script>insert into black_list_t (ips,modifyTime,modifyUserId) values (#{ips},#{modifyTime},#{modifyUserId})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(BlackList blackList);

	/**
	 * 批量新增BlackList
	 */
	@Insert("<script>insert into black_list_t (ips,modifyTime,modifyUserId) values <foreach collection='list' item='item' index='index' separator=','> (#{item.ips},#{item.modifyTime},#{item.modifyUserId})</foreach></script>")
	int addBatch(List<BlackList> blackListList);

	/**
	 * 修改BlackList(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update black_list_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.ips,ips)'>,ips=#{ips}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.modifyTime,modifyTime)'>,modifyTime=#{modifyTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.modifyUserId,modifyUserId)'>,modifyUserId=#{modifyUserId}</if> where `id`=#{id}</script>")
	int update(BlackList blackList);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from black_list_t t where t.id=#{id}</script>")
	BlackList findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from black_list_t t where t.id=#{id}</script>")
	BlackList findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from black_list_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from black_list_t t order by t.id desc</script>")
	List<BlackList> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<BlackList> pageList(Page<BlackList> page);

	BlackList findOne();
}