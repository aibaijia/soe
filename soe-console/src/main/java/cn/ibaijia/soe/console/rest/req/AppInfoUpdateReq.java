package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

public class AppInfoUpdateReq implements ValidateModel {

    @FieldAnn
    public Long id;
    /**
     * defaultVal:NULL,
     * comments:
     */
    /**
     * defaultVal:NULL,
     * comments:
     */
    @FieldAnn
    public String name;

    /**
     * defaultVal:NULL,
     * comments:
     */
    @FieldAnn
    public String password;
    /**
     * defaultVal:NULL
     * comments:1 消费者 2 生产者
     */
    @FieldAnn
    public Integer type;
    /**
     * defaultVal:NULL
     * comments:1 启用 0 禁用
     */
    @FieldAnn
    public Integer status;
    /**
     * defaultVal:NULL
     * comments:1 全部授权 2 部分授权
     */
    @FieldAnn
    public Integer authType;
}