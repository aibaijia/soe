package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.dao.mapper.AppInfoMapper;
import cn.ibaijia.soe.console.rest.vo.IdxStatistics;
import cn.ibaijia.jsm.base.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class IndexService extends BaseService {

    @Resource
    private AppInfoMapper appInfoMapper;

    public IdxStatistics idxStatistics() {
        IdxStatistics idxStatistics = new IdxStatistics();
        idxStatistics.appCount = appInfoMapper.countAppInfo();
        idxStatistics.consumerCount = appInfoMapper.countAppInfoByType(AppConsts.APP_TYPE_CONSUMER);
        idxStatistics.providerCount = appInfoMapper.countAppInfoByType(AppConsts.APP_TYPE_PROVIDER);

        return idxStatistics;
    }
}
