package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

public class AppAuthReq implements ValidateModel {
    /**
     * defaultVal:NULL,
     * comments:
     */
    @FieldAnn
    public Short consumerAppId;

    @FieldAnn
    public Short serviceAppId;

    public String serviceFuncIds;
}