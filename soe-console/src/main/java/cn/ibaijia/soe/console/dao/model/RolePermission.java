package cn.ibaijia.soe.console.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;

/**
 * tableName:role_permission_t
 */
public class RolePermission extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:权限表_权限ID
	 */
    @FieldAnn(required = false, name = "permissionId", comments = "权限表_权限ID")
	public Long permissionId;


	/**
	 * defaultVal:NULL
	 * comments:角色表_角色ID
	 */
    @FieldAnn(required = false, name = "roleId", comments = "角色表_角色ID")
	public Long roleId;
}