package cn.ibaijia.soe.console.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;
import cn.ibaijia.jsm.utils.DateUtil;

import java.util.Date;
import java.util.List;

/**
 * tableName:app_info_t
 */
public class AppInfo extends BaseModel {
    private static final long serialVersionUID = 1L;

    /**
     * defaultVal:NULL
     * comments:应用名称
     */
    @FieldAnn(required = false, maxLen = 20, name = "name", comments = "应用名称")
    public String name;
    /**
     * defaultVal:NULL
     * comments:应用ID
     */
    @FieldAnn(required = false, name = "appId", comments = "应用ID")
    public Short appId;
    /**
     * defaultVal:NULL
     * comments:应用密码
     */
    @FieldAnn(required = false, maxLen = 20, name = "password", comments = "应用密码")
    public String password;
    /**
     * defaultVal:NULL
     * comments:应用类型 1 消费者 2 生产者
     */
    @FieldAnn(required = false, name = "type", comments = "应用类型 1 消费者 2 生产者 ")
    public Integer type;
    /**
     * defaultVal:NULL
     * comments:状态 1 启用 0 禁用
     */
    @FieldAnn(required = false, name = "status", comments = "状态 1 启用 0 禁用")
    public Integer status;
    /**
     * defaultVal:NULL
     * comments:授权类型 1 全部授权 2 部分授权
     */
    @FieldAnn(required = false, name = "authType", comments = "授权类型 1 全部授权 2 部分授权")
    public Integer authType;
    /**
     * defaultVal:NULL
     * comments:创建时间
     */
    @FieldAnn(required = false, name = "createTime", comments = "创建时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
    public Date createTime;
    /**
     * defaultVal:NULL
     * comments:创建人ID
     */
    @FieldAnn(required = false, name = "createUserId", comments = "创建人ID")
    public Long createUserId;
    /**
     * defaultVal:NULL
     * comments:更新时间
     */
    @FieldAnn(required = false, name = "updateTime", comments = "更新时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
    public Date updateTime;
    /**
     * defaultVal:NULL
     * comments:更新人ID
     */
    @FieldAnn(required = false, name = "updateUserId", comments = "更新人ID")
    public Long updateUserId;

    @FieldAnn(required = false, comments = "APP授权信息列表")
    public List<AppAuth> appAuthList;

    @FieldAnn(required = false, comments = "APP服务信息列表")
    public List<ServiceInfo> serviceInfoList;

    public boolean LAY_CHECKED;
}