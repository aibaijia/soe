package cn.ibaijia.soe.console.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;

/**
 * tableName:service_info_t
 */
public class ServiceInfo extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:应用ID
	 */
    @FieldAnn(required = false, name = "appId", comments = "应用ID")
	public Short appId;
	/**
	 * defaultVal:NULL
	 * comments:功能ID
	 */
    @FieldAnn(required = false, name = "serviceId", comments = "功能ID")
	public Short serviceId;
	/**
	 * defaultVal:NULL
	 * comments:服务名称
	 */
    @FieldAnn(required = false, maxLen = 50, name = "name", comments = "服务名称")
	public String name;
	/**
	 * defaultVal:NULL
	 * comments:对应类
	 */
    @FieldAnn(required = false, maxLen = 100, name = "clazz", comments = "对应类")
	public String clazz;
	/**
	 * defaultVal:NULL
	 * comments:对应类方法
	 */
    @FieldAnn(required = false, maxLen = 50, name = "method", comments = "对应类方法")
	public String method;
	/**
	 * defaultVal:NULL
	 * comments:状态 1 有效 0 无效
	 */
    @FieldAnn(required = false, name = "status", comments = "状态 1 有效 0 无效")
	public Integer status;
}