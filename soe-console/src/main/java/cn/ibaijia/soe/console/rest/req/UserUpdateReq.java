package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.annotation.FieldType;
import cn.ibaijia.jsm.base.ValidateModel;

import java.util.List;

public class UserUpdateReq implements ValidateModel {

    @FieldAnn
    public Long id;
    @FieldAnn(maxLen = 50, required = false)
    public String name;
    @FieldAnn(maxLen = 50, required = false)
    public String nickName;

    public Integer gender;
    @FieldAnn(maxLen = 50, required = false)
    public String mobile;
    @FieldAnn(type = FieldType.EMAIL, required = false)
    public String email;
    @FieldAnn(maxLen = 50, required = false)
    public String dept;
    @FieldAnn(maxLen = 50, required = false)
    public String title;

    @FieldAnn(maxLen = 50, required = false)
    public String qq;//QQ

    @FieldAnn(maxLen = 50, required = false)
    public String wechat;//微信

    @FieldAnn(maxLen = 250, required = false)
    public String headimg;//头像路径

    @FieldAnn(required = false)
    public Long companyId;//企业ID

    @FieldAnn(required = false)
    public List<Long> roleIds;//角色id
}
