package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

public class UserLoginReq implements ValidateModel {

	@FieldAnn
	public String captcha;
	@FieldAnn
	public  String username;
	@FieldAnn
	public String password;
	
}
