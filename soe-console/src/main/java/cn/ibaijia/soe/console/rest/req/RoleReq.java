package cn.ibaijia.soe.console.rest.req;


import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

import java.util.List;

public class RoleReq implements ValidateModel {

    @FieldAnn(maxLen = 20)
    public String name;

    public List<Long> permissionIds;

    @FieldAnn(maxLen = 200, required = false)
    public String description;

}
