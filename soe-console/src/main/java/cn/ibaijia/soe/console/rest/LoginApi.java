package cn.ibaijia.soe.console.rest;

import cn.ibaijia.soe.console.consts.AppPairConsts;
import cn.ibaijia.soe.console.rest.req.UserLoginReq;
import cn.ibaijia.soe.console.rest.vo.UserLoginResp;
import cn.ibaijia.soe.console.service.LoginService;
import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.jedis.JedisService;
import cn.ibaijia.jsm.rest.resp.FileResp;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.jsm.session.Session;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/v1")
public class LoginApi extends BaseRest {

	@Resource
	private LoginService loginService;
	@Resource
	private JedisService jedisService;

	@ApiOperation(value = "登录API", notes = "")
	@RestAnn(authType = AuthType.NONE, transaction = Transaction.WRITE)
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public RestResp<UserLoginResp> login(@RequestBody UserLoginReq req) {
		RestResp<UserLoginResp> baseResp = new RestResp<UserLoginResp>();
		String cacheCode = (String) WebContext.getRequest().getSession().getAttribute("validCode");
		Boolean useCaptcha = AppContext.getAsBoolean("use.captcha");
		if (useCaptcha && !req.captcha.equalsIgnoreCase(cacheCode)) {
			baseResp.setPair(AppPairConsts.VALID_CODE_ERROR);
			return baseResp;
		}
		baseResp = loginService.login(req,baseResp);
		return baseResp;
	}

	@ApiOperation(value = "获取验证码API", notes = "根据filename生成验证码")
	@RestAnn(authType = AuthType.NONE, transaction = Transaction.READ)
	@RequestMapping(value = "/captcha", method = RequestMethod.GET)
	public FileResp validCode() {
		FileResp resp = loginService.validCode("validCode");
		return resp;
	}

	@ApiOperation(value = "登出API", notes = "")
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.NONE)
	@RequestMapping(value = "/logout", method = RequestMethod.DELETE)
	public RestResp<Boolean> logout() {
		RestResp<Boolean> baseResp = new RestResp<>();
		Session session = WebContext.currentSession();
		if(session != null){
			session.invalid();
		}
		baseResp.result = true;
		return baseResp;
	}

}
