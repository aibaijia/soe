package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.client.utils.StringUtil;
import cn.ibaijia.soe.console.dao.mapper.BlackListMapper;
import cn.ibaijia.soe.console.dao.model.BlackList;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.utils.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

@Service
public class BlackListService extends BaseService {

    @Resource
    private BlackListMapper blackListMapper;
    @Resource
    private SoeClientService soeClientService;


    public BlackList listOne() {
        BlackList blackList = blackListMapper.findOne();
        return blackList;
    }

    public BlackList saveOrUpdate(BlackList blackList) {
        blackList.modifyTime = DateUtil.currentDate();
        blackList.modifyUserId = WebContext.currentUser().getUid();
        BlackList dbBlackList = blackListMapper.findOne();
        if(dbBlackList == null){
            blackListMapper.add(blackList);
            dbBlackList = blackList;
        }else {
            dbBlackList.ips = blackList.ips;
            dbBlackList.modifyTime = blackList.modifyTime;
            dbBlackList.modifyUserId = blackList.modifyUserId;
            blackListMapper.update(blackList);
        }
        return dbBlackList;
    }

    public void sync(BlackList blackList) {
        if(blackList.ips == null){
            return;
        }
        Set<String> blacklistSet = new HashSet<>();
        String[] ips = blackList.ips.split(";");
        if(ips.length > 0){
            for (String bl:ips){
                if(!StringUtil.isEmpty(bl)){
                    blacklistSet.add(bl);
                }
            }
        }
        soeClientService.putBlackList(blacklistSet);
    }
}