package cn.ibaijia.soe.console.rest.req;


import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

import java.util.List;

public class RoleUpdateReq implements ValidateModel {

    @FieldAnn
    public Long id;
    @FieldAnn(maxLen = 50)
    public String name;
    @FieldAnn
    public List<Long> permissionIds;
    @FieldAnn(maxLen = 250, required = false)
    public String description;

}
