package cn.ibaijia.soe.console.rest.vo;

public class ClientInfoVo {

	public short id;
	public byte sn;
	public boolean reading;
	public boolean writing;
	public int writeCacheQueueSize;
	public int readBufferPosition;
	public int readBufferLimit;
	public int readBufferCapacity;

}