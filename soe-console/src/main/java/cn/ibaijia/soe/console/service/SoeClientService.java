package cn.ibaijia.soe.console.service;

import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.context.AppContext;
import cn.ibaijia.jsm.service.JsmAppListenerService;
import cn.ibaijia.soe.client.Consts;
import cn.ibaijia.soe.client.SoeClient;
import cn.ibaijia.soe.client.listener.SoeLoginSuccessListener;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.session.SoeSession;
import cn.ibaijia.soe.client.utils.StringUtil;
import cn.ibaijia.soe.console.dao.model.AppInfo;
import cn.ibaijia.soe.console.providers.HeartbeatService;
import cn.ibaijia.soe.console.providers.RegisterService;
import cn.ibaijia.soe.console.providers.StatisticService;
import cn.ibaijia.soe.console.rest.vo.AllClientInfoVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletContextEvent;
import java.util.List;
import java.util.Set;

@Service
public class SoeClientService extends BaseService implements JsmAppListenerService {

    @Resource
    private SoeClient soeClient;

    @Resource
    private AppInfoService appInfoService;

    public void putAppInfoList(List<AppInfo> list) {
        Short toId = 0;
        soeClient.send(toId, Consts.FUNCID.CONSOLE_PUTAPPS_REQ.code(), list);
    }

    public void putAppInfo(AppInfo appInfo) {
        Short toId = 0;
        soeClient.send(toId, Consts.FUNCID.CONSOLE_PUTAPPS_REQ.code(), appInfo);
    }

    public void putBlackList(Set<String> blackList) {
        Short toId = 0;
        soeClient.send(toId, Consts.FUNCID.CONSOLE_PUTBLACKLIST_REQ.code(), blackList);
    }

    public AllClientInfoVo getClientInfo() {
        Short toId = 0;
        SoeObject soeObject = soeClient.sendSync(toId, Consts.FUNCID.CONSOLE_GET_CLIENT_LIST_REQ.code(), 1, "");
        if (soeObject == null) {
            logger.error("get client info error.");
            return null;
        }
        String res = soeObject.getBodyAsString();
        return JSON.parseObject(res, new TypeReference<AllClientInfoVo>() {
        });
    }

    private void init() {
        soeClient.addService(new RegisterService((short) 1001, "注册"));
        soeClient.addService(new StatisticService((short) 1002, "统计"));
        soeClient.addService(new HeartbeatService((short) 1003, "心跳"));
    }


    public void destroy() {
        logger.info("soeClient.close()");
        if (soeClient != null) {
            soeClient.shutdown();
        }
    }

    public boolean reconnect() {
        try {
            if (soeClient != null) {
                soeClient.notifyReconnect();
            }
            return true;
        } catch (Exception e) {
            logger.error("notifyReconnect error.", e);
            return false;
        }
    }

    @Override
    public void init(ServletContextEvent servletContextEvent) {
        this.init();
    }

    @Override
    public void close(ServletContextEvent servletContextEvent) {
        this.destroy();
    }
}
