package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

public class UserResetPwdReq implements ValidateModel {

	@FieldAnn
	public Long id;
	
}
