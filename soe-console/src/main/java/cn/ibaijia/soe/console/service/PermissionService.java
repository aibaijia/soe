package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.dao.mapper.PermissionMapper;
import cn.ibaijia.soe.console.dao.model.Permission;
import cn.ibaijia.jsm.base.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PermissionService extends BaseService {

	@Resource
	private PermissionMapper permissionMapper;
	
	public List<Permission> listPermission() {
		return permissionMapper.listAll();
	}

}
