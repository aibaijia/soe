package cn.ibaijia.soe.console;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan(basePackages={"cn.ibaijia.soe.console.dao.mapper"})
@ComponentScan(basePackages = {"cn.ibaijia.soe.console"})
@SpringBootApplication()
@EnableScheduling()
public class StartApp {

    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
    }

}
