package cn.ibaijia.soe.console.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;
import cn.ibaijia.jsm.utils.DateUtil;

import java.util.Date;

/**
 * tableName:app_statistics_t
 */
public class AppStatistics extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:发送ID
	 */
    @FieldAnn(required = false, name = "fromId", comments = "发送ID")
	public Short fromId;
	/**
	 * defaultVal:NULL
	 * comments:发送SN
	 */
    @FieldAnn(required = false, name = "fromSn", comments = "发送SN")
	public Byte fromSn;
	/**
	 * defaultVal:NULL
	 * comments:接收ID
	 */
    @FieldAnn(required = false, name = "toId", comments = "接收ID")
	public Short toId;
	/**
	 * defaultVal:NULL
	 * comments:接收SN
	 */
    @FieldAnn(required = false, name = "toSn", comments = "接收SN")
	public Byte toSn;
	/**
	 * defaultVal:NULL
	 * comments:消息长度
	 */
    @FieldAnn(required = false, name = "bytesCount", comments = "消息长度")
	public Long bytesCount;
	/**
	 * defaultVal:NULL
	 * comments:上次更新时间
	 */
    @FieldAnn(required = false, name = "lastTime", comments = "上次更新时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
	public Date lastTime;
}