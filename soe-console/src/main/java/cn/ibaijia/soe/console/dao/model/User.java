package cn.ibaijia.soe.console.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;

/**
 * tableName:user_t
 */
public class User extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:用户名
	 */
    @FieldAnn(required = false, maxLen = 50, name = "username", comments = "用户名")
	public String username;
	/**
	 * defaultVal:NULL
	 * comments:密码
	 */
    @FieldAnn(required = false, maxLen = 50, name = "password", comments = "密码")
	public String password;
	/**
	 * defaultVal:NULL
	 * comments:1 可用\r\n    2 不可用\r\n            
	 */
    @FieldAnn(required = false, name = "status", comments = "1 可用\r\n    2 不可用\r\n            ")
	public Integer status;
}