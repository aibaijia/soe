package cn.ibaijia.soe.console.dao.model;
/**
 * tableName:seq_t
 */
import cn.ibaijia.jsm.base.BaseModel;

public class Seq extends BaseModel {
private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:name
	 */
	public String name;
	/**
	 * defaultVal:NULL
	 * comments:prefix
	 */
	public String prefix;
	/**
	 * defaultVal:NULL
	 * comments:seqVal
	 */
	public Long seqVal;

	public Seq() {
		super();
	}
	public Seq(String name, String prefix, Long seqVal) {
		super();
		this.name = name;
		this.prefix = prefix;
		this.seqVal = seqVal;
	}
}