package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.dao.mapper.RoleMapper;
import cn.ibaijia.soe.console.dao.model.Role;
import cn.ibaijia.soe.console.dao.model.RolePermission;
import cn.ibaijia.soe.console.rest.req.RoleReq;
import cn.ibaijia.soe.console.rest.req.RoleUpdateReq;
import cn.ibaijia.soe.console.rest.req.RoleUpdateStatusReq;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.session.SessionUser;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.jsm.utils.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleService extends BaseService {

    @Resource
    private RoleMapper roleMapper;

    public List<Role> listRoles() {
        return roleMapper.listAll();
    }

    public Long addRole(RoleReq req) {
        Role role = new Role();
        BeanUtil.copy(req,role);
        roleMapper.add(role);
        return role.id;
    }

    public Integer updateRole(RoleUpdateReq req) {
        Role dbRole = roleMapper.findById(req.id);
        BeanUtil.copy(req,dbRole);
        return roleMapper.update(dbRole);
    }

    public Integer updateRoleStatus(RoleUpdateStatusReq req) {
        Role dbRole = roleMapper.findById(req.id);
        BeanUtil.copy(req,dbRole);
        return roleMapper.update(dbRole);
    }
}
