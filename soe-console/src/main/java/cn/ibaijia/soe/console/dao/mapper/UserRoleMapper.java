package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.UserRole;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;
import java.util.List;
/**
 * tableName:user_role_t
 */
public interface UserRoleMapper {

	
    @Insert("<script>insert into user_role_t (userId,roleId) values <foreach collection='list' item='item' index='index' separator=','> (#{item.userId},#{item.roleId})</foreach></script>")
	int addBatch(List<UserRole> userRoleList);

    @Select("<script>select t.* from user_role_t t where t.id=#{id}</script>")
	UserRole findByIdForUpdate(@Param("id") Long id);


    /**
	 * 新增UserRole
	 */
    @Insert("<script>insert into user_role_t (userId,roleId) values (#{userId},#{roleId})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(UserRole userRole);
	 
	/**
	 * 修改UserRole(全量更新，应先查询出数据库对象再进行，赋值更新)
	 */
    @Update("<script>update user_role_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.userId,userId)'>,userId=#{userId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.roleId,roleId)'>,roleId=#{roleId}</if> where `id`=#{id}</script>")
	int update(UserRole userRole); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from user_role_t t where t.id=#{id}</script>")
	UserRole findById(@Param("id") Long id);
	
	/**
	 * 删除ById
	 */
    @Delete("<script>delete from user_role_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id); 
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from user_role_t t order by t.id desc</script>")
	List<UserRole> listAll();
	
	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<UserRole> pageList(Page<UserRole> page);
	
}
