package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.AppInfo;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:app_info_t
 */
public interface AppInfoMapper {

	/**
	 * 新增AppInfo
	 */
	@Insert("<script>insert into app_info_t (name,appId,password,type,status,authType,createTime,createUserId,updateTime,updateUserId) values (#{name},#{appId},#{password},#{type},#{status},#{authType},#{createTime},#{createUserId},#{updateTime},#{updateUserId})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(AppInfo appInfo);

	/**
	 * 批量新增AppInfo
	 */
	@Insert("<script>insert into app_info_t (name,appId,password,type,status,authType,createTime,createUserId,updateTime,updateUserId) values <foreach collection='list' item='item' index='index' separator=','> (#{item.name},#{item.appId},#{item.password},#{item.type},#{item.status},#{item.authType},#{item.createTime},#{item.createUserId},#{item.updateTime},#{item.updateUserId})</foreach></script>")
	int addBatch(List<AppInfo> appInfoList);

	/**
	 * 修改AppInfo(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update app_info_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.appId,appId)'>,appId=#{appId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.password,password)'>,password=#{password}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.type,type)'>,type=#{type}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.status,status)'>,status=#{status}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.authType,authType)'>,authType=#{authType}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.createTime,createTime)'>,createTime=#{createTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.createUserId,createUserId)'>,createUserId=#{createUserId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.updateTime,updateTime)'>,updateTime=#{updateTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.updateUserId,updateUserId)'>,updateUserId=#{updateUserId}</if> where `id`=#{id}</script>")
	int update(AppInfo appInfo);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from app_info_t t where t.id=#{id}</script>")
	AppInfo findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from app_info_t t where t.id=#{id}</script>")
	AppInfo findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from app_info_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from app_info_t t order by t.id desc</script>")
	List<AppInfo> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<AppInfo> pageList(Page<AppInfo> page);

	List<AppInfo> pageListService(Page<AppInfo> page);

    int countAppInfo();

	int countAppInfoByType(@Param("type")int type);
}