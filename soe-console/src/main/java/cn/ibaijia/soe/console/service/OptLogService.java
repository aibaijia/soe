package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.dao.mapper.OptLogMapper;
import cn.ibaijia.jsm.dao.model.OptLog;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.service.LogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OptLogService implements LogService {
    @Resource
    private OptLogMapper optLogMapper;

    @Override
    public Long addLog(OptLog optLog) {
        return Long.valueOf(optLogMapper.add(optLog));
    }

    @Override
    public void pageList(Page<OptLog> page) {
        optLogMapper.pageList(page);
    }
}
