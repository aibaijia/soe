package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.AppAuth;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;

/**
 * tableName:app_auth_t
 */
public interface AppAuthMapper {

    /**
     * 新增AppAuth
     */
    @Insert("<script>insert into app_auth_t (serviceAppId,serviceFuncIds) values (#{serviceAppId},#{serviceFuncIds})</script>")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int add(AppAuth appAuth);

    /**
     * 批量新增AppAuth
     */
    @Insert("<script>insert into app_auth_t (serviceAppId,serviceFuncIds) values <foreach collection='list' item='item' index='index' separator=','> (#{item.serviceAppId},#{item.serviceFuncIds})</foreach></script>")
    int addBatch(List<AppAuth> appAuthList);

    /**
     * 修改AppAuth(配合findByIdForUpdate更佳)
     */
    @Update("<script>update app_auth_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.serviceAppId,serviceAppId)'>,serviceAppId=#{serviceAppId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.serviceFuncIds,serviceFuncIds)'>,serviceFuncIds=#{serviceFuncIds}</if> where `id`=#{id}</script>")
    int update(AppAuth appAuth);

    /**
     * 查询ById
     */
    @Select("<script>select t.* from app_auth_t t where t.id=#{id}</script>")
    AppAuth findById(@Param("id") Long id);

    /**
     * 查询ByIdForUpdate 查询的时候创建镜像
     */
    @Select("<script>select t.* from app_auth_t t where t.id=#{id}</script>")
    AppAuth findByIdForUpdate(@Param("id") Long id);

    /**
     * 删除ById
     */
    @Delete("<script>delete from app_auth_t where `id`=#{id}</script>")
    int deleteById(@Param("id") Long id);

    /**
     * 查询出所有（谨用）
     */
    @Select("<script>select t.* from app_auth_t t order by t.id desc</script>")
    List<AppAuth> listAll();

    /**
     * 分页查询（使用时修改对应queryMap参数即可）
     */
    List<AppAuth> pageList(Page<AppAuth> page);

    int deleteByConsumerId(@Param("consumerAppId") Short consumerAppId);

    List<AppAuth> findByConsumerId(@Param("consumerAppId") Short consumerAppId);

    List<Short> findAuthServiceAppId(@Param("consumerAppId") Short consumerAppId);
}