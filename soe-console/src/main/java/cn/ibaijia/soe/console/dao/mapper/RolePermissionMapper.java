package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.RolePermission;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;
import java.util.List;
/**
 * tableName:role_permission_t
 */
public interface RolePermissionMapper {

	
    @Insert("<script>insert into role_permission_t (roleId,permissionId) values <foreach collection='list' item='item' index='index' separator=','> (#{item.roleId},#{item.permissionId})</foreach></script>")
	int addBatch(List<RolePermission> rolePermissionList);

    @Select("<script>select t.* from role_permission_t t where t.id=#{id}</script>")
	RolePermission findByIdForUpdate(@Param("id") Long id);


    /**
	 * 新增RolePermission
	 */
    @Insert("<script>insert into role_permission_t (roleId,permissionId) values (#{roleId},#{permissionId})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(RolePermission rolePermission);
	 
	/**
	 * 修改RolePermission(全量更新，应先查询出数据库对象再进行，赋值更新)
	 */
    @Update("<script>update role_permission_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.roleId,roleId)'>,roleId=#{roleId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.permissionId,permissionId)'>,permissionId=#{permissionId}</if> where `id`=#{id}</script>")
	int update(RolePermission rolePermission); 
	
	/**
	 * 查询ById
	 */
    @Select("<script>select t.* from role_permission_t t where t.id=#{id}</script>")
	RolePermission findById(@Param("id") Long id);
	
	/**
	 * 删除ById
	 */
    @Delete("<script>delete from role_permission_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id); 
	
	/**
	 * 查询出所有（谨用）
	 */
    @Select("<script>select t.* from role_permission_t t order by t.id desc</script>")
	List<RolePermission> listAll();
	
	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<RolePermission> pageList(Page<RolePermission> page);
	
}
