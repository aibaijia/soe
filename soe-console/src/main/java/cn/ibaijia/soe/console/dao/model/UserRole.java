package cn.ibaijia.soe.console.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;

/**
 * tableName:user_role_t
 */
public class UserRole extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:用户表_用户ID
	 */
    @FieldAnn(required = false, name = "roleId", comments = "用户表_用户ID")
	public Long userId;
	/**
	 * defaultVal:NULL
	 * comments:角色表_角色ID
	 */
    @FieldAnn(required = false, name = "roleId", comments = "角色表_角色ID")
	public Long roleId;
}