package cn.ibaijia.soe.console.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.OptLogUtil;
import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.dao.model.AppInfo;
import cn.ibaijia.soe.console.rest.req.AppInfoReq;
import cn.ibaijia.soe.console.rest.req.AppInfoUpdateReq;
import cn.ibaijia.soe.console.service.AppInfoService;
import cn.ibaijia.soe.console.service.SoeClientService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/v1/app-info")
public class AppInfoApi extends BaseRest {

    @Resource
    private AppInfoService appInfoService;
    @Resource
    private SoeClientService soeClientService;

    @ApiOperation(value = "所有AppInfo列表", notes = "")
	@RequestMapping(value = "/page-list",method = RequestMethod.GET)
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.NONE)
	public RestResp<Page<AppInfo>> pageList(Page<AppInfo> page){
		RestResp<Page<AppInfo>> resp = new RestResp<>();
		appInfoService.pageList(page);
        resp.result = page;
		return resp;
	}

    @ApiOperation(value = "所有AppInfo列表", notes = "")
	@RequestMapping(value = "/page-list-service",method = RequestMethod.GET)
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.NONE)
	public RestResp<Page<AppInfo>> pageListService(Page<AppInfo> page,@RequestParam("appId") Short appId){
		RestResp<Page<AppInfo>> resp = new RestResp<>();
		appInfoService.pageListService(page,appId);
        resp.result = page;
		return resp;
	}

    @ApiOperation(value = "添加AppInfo", notes = "")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]添加AppInfo[#{logContent}][#{logStatus}]")
    public RestResp<Long> addAppInfo(@RequestBody AppInfoReq req) {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent(req.name);
        RestResp<Long> resp = new RestResp<Long>();
        resp.result = appInfoService.add(req);
        OptLogUtil.setLogStatus("成功");
        return resp;
    }

    @ApiOperation(value = "更新AppInfo", notes = "")
    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]更新AppInfo[#{logContent}][#{logStatus}]")
    public RestResp<Integer> updateAppInfo(@RequestBody AppInfoUpdateReq req) {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent(req.name);
        RestResp<Integer> resp = new RestResp<>();
        resp.result = appInfoService.update(req);
        OptLogUtil.setLogStatus("成功");
        return resp;
    }


    @ApiOperation(value = "同步所有AppInfo", notes = "")
    @RequestMapping(value = "/sync-all",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.READ,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]同步AppInfo[#{logContent}][#{logStatus}]")
    public RestResp<Integer> syncAll() {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent("all");
        RestResp<Integer> resp = new RestResp<>();
        resp.result = appInfoService.syncAll();
        OptLogUtil.setLogStatus("成功");
        return resp;
    }

    @ApiOperation(value = "同步AppInfo", notes = "")
    @RequestMapping(value = "/sync/{id}",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.READ,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]同步AppInfo[#{logContent}][#{logStatus}]")
    public RestResp<Integer> syn(@PathVariable("id") Long id) {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent("id:"+id);
        RestResp<Integer> resp = new RestResp<>();
        resp.result = appInfoService.sync(id);
        OptLogUtil.setLogStatus("成功");
        return resp;
    }

    @ApiOperation(value = "重连Router", notes = "")
    @RequestMapping(value = "/reconnect",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.NONE,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]重连Router[#{logContent}][#{logStatus}]")
    public RestResp<Boolean> syn() {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        RestResp<Boolean> resp = new RestResp<>();
        resp.result = soeClientService.reconnect();
        OptLogUtil.setLogStatus(resp.result?"成功":"失败");
        return resp;
    }

}
