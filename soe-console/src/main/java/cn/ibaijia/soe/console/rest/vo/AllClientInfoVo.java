package cn.ibaijia.soe.console.rest.vo;

import cn.ibaijia.jsm.base.ValidateModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AllClientInfoVo implements ValidateModel {

	public Map<String, Byte> snMap = new HashMap<>();
	public Map<String, List<ClientInfoVo>> idMap = new HashMap<>();
	public Map<String, ClientInfoVo> nameMap = new HashMap<>();

}