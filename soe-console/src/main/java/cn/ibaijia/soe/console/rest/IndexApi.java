package cn.ibaijia.soe.console.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.rest.resp.RestResp;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexApi extends BaseRest {

	@RequestMapping(value = "", method = RequestMethod.GET)
	@RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
	public String index() {
		return "redirect:./html/login.html";
	}

}

