package cn.ibaijia.soe.console.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;

/**
 * tableName:app_auth_t
 */
public class AppAuth extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL,
	 * comments:
	 */
	public Short consumerAppId;
	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, name = "serviceAppId", comments = "")
	public Short serviceAppId;
	/**
	 * defaultVal:NULL
	 * comments:
	 */
    @FieldAnn(required = false, maxLen = 250, name = "serviceFuncIds", comments = "")
	public String serviceFuncIds;
}