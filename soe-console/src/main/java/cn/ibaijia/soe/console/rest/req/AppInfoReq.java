package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

public class AppInfoReq implements ValidateModel {
    /**
     * defaultVal:NULL,
     * comments:
     */
    @FieldAnn
    public String name;
    /**
     * defaultVal:NULL,
     * comments:
     */
    @FieldAnn(required = false,minLen = 6,maxLen = 16)
    public String password;
    /**
     * defaultVal:NULL
     * comments:1 消费者 2 生产者
     */
    @FieldAnn
    public Integer type;

    /**
     * defaultVal:NULL
     * comments:1 启用 0 禁用
     */
    @FieldAnn
    public Integer status;

    /**
     * defaultVal:NULL
     * comments:1 全部授权 2 部分授权
     */
    @FieldAnn
    public Integer authType;
}