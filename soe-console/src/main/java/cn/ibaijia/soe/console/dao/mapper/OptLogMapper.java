package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.jsm.dao.model.OptLog;
import org.apache.ibatis.annotations.Param;
import cn.ibaijia.jsm.dao.model.Page;
import java.util.List;
/**
 * tableName:opt_log_t
 */
public interface OptLogMapper {

    /**
	 * 新增OptLog
	 */
	int add(OptLog optLog);
	 
	/**
	 * 修改OptLog(全量更新，应先查询出数据库对象再进行，赋值更新)
	 */
	int update(OptLog optLog); 
	
	/**
	 * 查询ById
	 */
	OptLog findById(@Param("id") Long id);
	
	/**
	 * 删除ById
	 */
	int deleteById(@Param("id") Long id); 
	
	/**
	 * 查询出所有（谨用）
	 */
	List<OptLog> listAll();
	
	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<OptLog> pageList(Page<OptLog> page);
	
}