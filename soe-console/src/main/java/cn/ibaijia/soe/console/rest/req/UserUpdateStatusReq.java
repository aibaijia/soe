package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

/**
 * 用户状态修改
 */
public class UserUpdateStatusReq implements ValidateModel {

	@FieldAnn
	public Long id;
	@FieldAnn
	public Integer status;

}
