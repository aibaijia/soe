package cn.ibaijia.soe.console.providers;

import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.console.dao.model.ServiceInfo;
import cn.ibaijia.soe.console.service.ServiceInfoService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.lang.reflect.Type;
import java.util.List;

public class RegisterService extends BaseService {

    public RegisterService(short id, String name) {
        super(id, name);
    }

    private Type serviceInfoListType = new TypeReference<List<ServiceInfo>>() {
    }.getType();

    /**
     * 更新启动注册functions
     */
    @Override
    public void doService(SoeObject soeObject) {
        String body = soeObject.getBodyAsString();
        logger.info("register:{}", body);
        List<ServiceInfo> serviceInfos = JSON.parseObject(body, serviceInfoListType);
        ServiceInfoService serviceInfoService = SpringContext.getBean(ServiceInfoService.class);
        serviceInfoService.updateServiceInfos(serviceInfos);
    }
}
