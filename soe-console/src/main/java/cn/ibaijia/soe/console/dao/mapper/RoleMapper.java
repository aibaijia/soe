package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.Role;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:role_t
 */
public interface RoleMapper {

	/**
	 * 新增Role
	 */
	@Insert("<script>insert into role_t (name,description,status,createTime,updateTime,createUserId,modifyUserId) values (#{name},#{description},#{status},#{createTime},#{updateTime},#{createUserId},#{modifyUserId})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(Role role);

	/**
	 * 批量新增Role
	 */
	@Insert("<script>insert into role_t (name,description,status,createTime,updateTime,createUserId,modifyUserId) values <foreach collection='list' item='item' index='index' separator=','> (#{item.name},#{item.description},#{item.status},#{item.createTime},#{item.updateTime},#{item.createUserId},#{item.modifyUserId})</foreach></script>")
	int addBatch(List<Role> roleList);

	/**
	 * 修改Role(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update role_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.description,description)'>,description=#{description}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.status,status)'>,status=#{status}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.createTime,createTime)'>,createTime=#{createTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.updateTime,updateTime)'>,updateTime=#{updateTime}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.createUserId,createUserId)'>,createUserId=#{createUserId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.modifyUserId,modifyUserId)'>,modifyUserId=#{modifyUserId}</if> where `id`=#{id}</script>")
	int update(Role role);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from role_t t where t.id=#{id}</script>")
	Role findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from role_t t where t.id=#{id}</script>")
	Role findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from role_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from role_t t order by t.id desc</script>")
	List<Role> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<Role> pageList(Page<Role> page);
	
}