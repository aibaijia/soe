package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.Seq;
import org.apache.ibatis.annotations.Param;

public interface SeqMapper {

	public int add(Seq seq);
	
	public Seq findByName(@Param("name") String name);

	public int update(Seq seq);
	
	int countTradeCode(@Param("code") String code);
}
