package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.dao.model.ServiceInfo;
import cn.ibaijia.soe.console.dao.mapper.ServiceInfoMapper;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.dao.model.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
@Service
public class ServiceInfoService extends BaseService {

    @Resource
    private ServiceInfoMapper serviceInfoMapper;

    @Transactional
    public void updateServiceInfos(List<ServiceInfo> serviceInfos) {
        if(serviceInfos == null || serviceInfos.isEmpty()){
            return;
        }
        Short appId = serviceInfos.get(0).appId;
        serviceInfoMapper.deleteByAppId(appId);
        for (ServiceInfo serviceInfo:serviceInfos){
            serviceInfo.status = AppConsts.BOOLEAN_YES;
            serviceInfoMapper.add(serviceInfo);
        }
    }
}