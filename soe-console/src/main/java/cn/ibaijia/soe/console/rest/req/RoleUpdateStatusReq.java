package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

/**
 * 角色状态修改
 */
public class RoleUpdateStatusReq implements ValidateModel {

	@FieldAnn
	public Long id;
	@FieldAnn
	public Integer status;

}
