package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.dao.mapper.RoleMapper;
import cn.ibaijia.soe.console.dao.mapper.UserInfoMapper;
import cn.ibaijia.soe.console.dao.mapper.UserMapper;
import cn.ibaijia.soe.console.dao.mapper.UserRoleMapper;
import cn.ibaijia.soe.console.dao.model.User;
import cn.ibaijia.soe.console.dao.model.UserInfo;
import cn.ibaijia.soe.console.dao.model.UserRole;
import cn.ibaijia.soe.console.rest.req.UserChangePasswordReq;
import cn.ibaijia.soe.console.rest.req.UserReq;
import cn.ibaijia.soe.console.rest.req.UserUpdateReq;
import cn.ibaijia.soe.console.rest.req.UserUpdateStatusReq;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.jsm.session.SessionUser;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.EncryptUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService extends BaseService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    public Long addUser(UserReq req) {
        User user = new User();
        user.username = req.username;
        user.password = EncryptUtil.md5("soe123");
        user.status = AppConsts.USER_STATUS_NORMAL;
        userMapper.add(user);

        logger.info("userId:{}", user.id);
        UserInfo userInfo = new UserInfo();
        BeanUtil.copy(req, userInfo);

        userInfo.id = user.id;
        userInfo.createTime = DateUtil.currentDate();
        SessionUser sessionUser = WebContext.currentUser();// 获取session
        userInfo.createUserId = sessionUser.getUid();
        userInfoMapper.add(userInfo);

        addUserRoles(req.roleIds, user.id);
        return user.id;
    }

    private void addUserRoles(List<Long> roleIds, Long userId) {
        if (roleIds != null && !roleIds.isEmpty()) {
            for (Long roleId : roleIds) {
                UserRole userRole = new UserRole();
                userRole.roleId = roleId;
                userRole.userId = userId;
                userRoleMapper.add(userRole);
            }
        }
    }

    public UserInfo updateUser(UserUpdateReq req) {
        UserInfo dbUserInfo = new UserInfo();
        BeanUtil.copy(req,dbUserInfo);
        userInfoMapper.update(dbUserInfo);
        return dbUserInfo;
    }

    public Integer updateUserPassword(UserChangePasswordReq req, Long userId) {
        User dbUser = userMapper.findById(userId);
        dbUser.password = EncryptUtil.md5(req.newPassword);
        return userMapper.update(dbUser);
    }

    public void pageList(Page<UserInfo> page) {
        userInfoMapper.pageList(page);
    }

    public Boolean updateUserStatus(UserUpdateStatusReq req) {
        User dbUser = userMapper.findById(req.id);
        dbUser.status = req.status;
        int res = userMapper.update(dbUser);
        return  res > 0;
    }

    public Integer resetPassword(Long userId) {
        User dbUser = userMapper.findById(userId);
        dbUser.password = EncryptUtil.md5("soe123");
        return userMapper.update(dbUser);

    }
}
