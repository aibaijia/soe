package cn.ibaijia.soe.console.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;
import cn.ibaijia.jsm.utils.DateUtil;

import java.util.Date;

/**
 * tableName:black_list_t
 */
public class BlackList extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, maxLen = 255, name = "ips", comments = "")
	public String ips;
	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, name = "modifyTime", comments = "")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
	public Date modifyTime;
	/**
	 * defaultVal:NULL,
	 * comments:
	 */
    @FieldAnn(required = false, name = "modifyUserId", comments = "")
	public Long modifyUserId;
}