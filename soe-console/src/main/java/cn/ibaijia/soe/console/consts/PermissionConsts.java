package cn.ibaijia.soe.console.consts;

import cn.ibaijia.jsm.consts.BasePermissionConsts;

public class PermissionConsts extends BasePermissionConsts {

	// 应用管理
	public static final String TASK_MANAGEMENT = "APP_MANAGEMENT";


	// 系统管理
	public static final String SYSTEM_MANAGEMENT = "SYSTEM_MANAGEMENT";
	//系统日志
	public static final String SYSTEM_LOG = "SYSTEM_LOG";
	//用户管理
	public static final String USER_MANAGEMENT = "USER_MANAGEMENT";
	//角色管理
	public static final String ROLE_MANAGEMENT = "ROLE_MANAGEMENT";
	

}
