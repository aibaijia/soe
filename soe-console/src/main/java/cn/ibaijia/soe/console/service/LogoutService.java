package cn.ibaijia.soe.console.service;

import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.jedis.JedisService;
import cn.ibaijia.jsm.session.Session;
import cn.ibaijia.jsm.session.SessionUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LogoutService extends BaseService {
	@Resource
	private JedisService jedisService;

	public String logout() {

		Session session = WebContext.currentSession();
		
		SessionUser sessionUser = WebContext.currentUser();
		if(sessionUser != null){
			logger.info("uid====:{}",sessionUser.getUid());
		}

		session.invalid();

		return "安全退出";

	}

}
