package cn.ibaijia.soe.console.dao.model;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;

/**
 * tableName:permission_t
 */
public class Permission extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:名称
	 */
    @FieldAnn(required = false, maxLen = 50, name = "name", comments = "名称")
	public String name;
	/**
	 * defaultVal:NULL
	 * comments:代码
	 */
    @FieldAnn(required = false, maxLen = 50, name = "code", comments = "代码")
	public String code;
}