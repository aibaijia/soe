package cn.ibaijia.soe.console.dao.model;

import com.alibaba.fastjson.annotation.JSONField;
import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.BaseModel;
import cn.ibaijia.jsm.utils.DateUtil;

import java.util.Date;

/**
 * tableName:role_t
 */
public class Role extends BaseModel {
    private static final long serialVersionUID = 1L;

	/**
	 * defaultVal:NULL
	 * comments:名称
	 */
    @FieldAnn(required = false, maxLen = 50, name = "name", comments = "名称")
	public String name;
	/**
	 * defaultVal:NULL
	 * comments:描述
	 */
    @FieldAnn(required = false, maxLen = 250, name = "description", comments = "描述")
	public String description;
	/**
	 * defaultVal:NULL
	 * comments:1 可用\r\n            2 不可用\r\n            
	 */
    @FieldAnn(required = false, name = "status", comments = "1 可用\r\n            2 不可用\r\n            ")
	public Integer status;
	/**
	 * defaultVal:NULL
	 * comments:创建时间
	 */
    @FieldAnn(required = false, name = "createTime", comments = "创建时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
	public Date createTime;
	/**
	 * defaultVal:NULL
	 * comments:更新时间
	 */
    @FieldAnn(required = false, name = "updateTime", comments = "更新时间")
    @JSONField(format = DateUtil.DATETIME_PATTERN)
	public Date updateTime;
	/**
	 * defaultVal:NULL
	 * comments:创建人ID
	 */
    @FieldAnn(required = false, name = "createUserId", comments = "创建人ID")
	public Long createUserId;
	/**
	 * defaultVal:NULL
	 * comments:修改人ID
	 */
    @FieldAnn(required = false, name = "modifyUserId", comments = "修改人ID")
	public Long modifyUserId;
}