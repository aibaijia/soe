package cn.ibaijia.soe.console.providers;

import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.soe.client.message.SystemStat;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.console.dao.model.ServiceInfo;
import cn.ibaijia.soe.console.service.ServiceInfoService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.lang.reflect.Type;
import java.util.List;

public class HeartbeatService extends BaseService {

    public HeartbeatService(short id, String name) {
        super(id, name);
    }

    /**
     * 更新启动注册functions
     */
    @Override
    public void doService(SoeObject soeObject) {
        String body = soeObject.getBodyAsString();
        logger.info("heartbeat:{}", body);
        SystemStat systemStat = JSON.parseObject(body, SystemStat.class);
    }
}
