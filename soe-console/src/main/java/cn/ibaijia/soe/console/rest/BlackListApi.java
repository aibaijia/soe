package cn.ibaijia.soe.console.rest;

import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.soe.console.dao.model.BlackList;
import cn.ibaijia.soe.console.service.BlackListService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/v1/black-list")
public class BlackListApi extends BaseRest {

    @Resource
    private BlackListService blackListService;

    @ApiOperation(value = "查询黑名单", notes = "")
	@RequestMapping(value = "/1",method = RequestMethod.GET)
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.NONE)
	public RestResp<BlackList> list(){
		RestResp<BlackList> resp = new RestResp<>();
        BlackList blackList = blackListService.listOne();
        resp.result = blackList;
		return resp;
	}


    @ApiOperation(value = "保存黑名单", notes = "")
    @RequestMapping(value = "/save",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE)
    public RestResp<BlackList> save(@RequestBody BlackList blackList){
        RestResp<BlackList> resp = new RestResp<>();
        BlackList resBlackList = blackListService.saveOrUpdate(blackList);
        resp.result = resBlackList;
        return resp;
    }

    @ApiOperation(value = "保存并同步黑名单", notes = "")
    @RequestMapping(value = "/save-sync",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE)
    public RestResp<BlackList> saveSync(@RequestBody BlackList blackList){
        RestResp<BlackList> resp = new RestResp<>();
        BlackList resBlackList = blackListService.saveOrUpdate(blackList);
        resp.result = resBlackList;

        //sync blacklist
        blackListService.sync(resBlackList);

        return resp;
    }

}
