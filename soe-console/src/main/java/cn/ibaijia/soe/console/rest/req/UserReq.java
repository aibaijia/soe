package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.annotation.FieldType;
import cn.ibaijia.jsm.base.ValidateModel;

import java.util.List;

public class UserReq implements ValidateModel {

    @FieldAnn
    public String username;
    @FieldAnn(maxLen = 50, required = false)
    public String name;
    @FieldAnn(maxLen = 50, required = false)
    public String nickName;

    public Integer gender;
    @FieldAnn(maxLen = 50, required = false)
    public String mobile;
    @FieldAnn(maxLen = 50, required = false)
    public String email;
    @FieldAnn(maxLen = 50, required = false)
    public String password;

    public Integer status;
    @FieldAnn(maxLen = 50, required = false)
    public String dept;//部门
    @FieldAnn(maxLen = 50, required = false)
    public String title;//职位

    @FieldAnn(maxLen = 50, required = false)
    public String qq;//QQ

    @FieldAnn(maxLen = 50, required = false)
    public String wechat;//微信

    @FieldAnn(maxLen = 250, required = false)
    public String headimg;//

    @FieldAnn(maxLen = 10, required = false, type = FieldType.NUMBER)
    public String companyId;//企业ID

    @FieldAnn(required = false)
    public List<Long> roleIds;//角色id
}
