package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.dao.model.AppAuth;
import cn.ibaijia.soe.console.dao.mapper.AppAuthMapper;
import cn.ibaijia.soe.console.rest.req.AppAuthReq;
import cn.ibaijia.soe.console.rest.req.AppInfoReq;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.utils.BeanUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
@Service
public class AppAuthService extends BaseService {

    @Resource
    private AppAuthMapper appAuthMapper;


    public Integer auth(Short appId, List<AppAuthReq> reqList) {
        appAuthMapper.deleteByConsumerId(appId);
        for(AppAuthReq req:reqList){
            AppAuth appAuth = new AppAuth();
            BeanUtil.copy(req,appAuth);
            appAuthMapper.add(appAuth);
        }
        return reqList.size();

    }

    public List<AppAuth> findByConsumerId(Short appId) {
        return appAuthMapper.findByConsumerId(appId);
    }
}