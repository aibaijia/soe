package cn.ibaijia.soe.console.consts;

public class AppConsts {

	public static final int USER_STATUS_NORMAL = 1;// normal
	public static final int USER_STATUS_LOCKED = 2;// locked

	// 通用boolean值
	public static final int BOOLEAN_YES = 1;// yes
	public static final int BOOLEAN_NO = 0;// no

	public static final String SESSION_USERNAME_KEY = "username";

	// 系统日志类型
	public static final String LOG_TPYE_USER = "用户管理";
	public static final String LOG_TPYE_ROLE = "角色管理";
	public static final String LOG_TPYE_APP = "应用管理";

    //1 消费者 2 生产者
	public static final int APP_TYPE_CONSUMER = 1;
	public static final int APP_TYPE_PROVIDER = 2;


}
