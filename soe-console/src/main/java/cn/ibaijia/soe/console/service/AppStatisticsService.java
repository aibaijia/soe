package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.dao.model.AppStatistics;
import cn.ibaijia.soe.console.dao.mapper.AppStatisticsMapper;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.dao.model.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
@Service
public class AppStatisticsService extends BaseService {

    @Resource
    private AppStatisticsMapper appStatisticsMapper;


    @Transactional
    public void saveOrUpdate(AppStatistics appStatistics) {
        int res = appStatisticsMapper.updateBytesCount(appStatistics);
        if(res == 0){
            appStatisticsMapper.add(appStatistics);
        }
    }
}