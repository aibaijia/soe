package cn.ibaijia.soe.console.rest;

import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.consts.AppPairConsts;
import cn.ibaijia.soe.console.dao.model.Role;
import cn.ibaijia.soe.console.rest.req.RoleReq;
import cn.ibaijia.soe.console.rest.req.RoleUpdateReq;
import cn.ibaijia.soe.console.rest.req.RoleUpdateStatusReq;
import cn.ibaijia.soe.console.service.RoleService;
import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.OptLogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/v1/role")
public class RoleApi extends BaseRest {

    @Resource
    private RoleService roleService;

    @ApiOperation(value = "所有角色列表", notes = "所有角色列表")
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	@RestAnn(authType = AuthType.NONE, transaction = Transaction.NONE)
	public RestResp<List<Role>> list(){
		RestResp<List<Role>> resp = new RestResp<>();
		resp.result = roleService.listRoles();
		return resp;
	}

    @ApiOperation(value = "添加角色", notes = "添加角色")
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]添加角色[#{logContent}][#{logStatus}]")
    public RestResp<Long> addRole(@RequestBody RoleReq req) {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent(req.name);
        OptLogUtil.setLogStatus("失败");
        RestResp<Long> resp = new RestResp<Long>();
        resp.result = roleService.addRole(req);
        OptLogUtil.setLogStatus("成功");
        return resp;
    }

    @ApiOperation(value = "更新角色", notes = "更新角色")
    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]更新角色[#{logContent}][#{logStatus}]")
    public RestResp<Integer> updateRole(@RequestBody RoleUpdateReq req) {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent(req.name);
        OptLogUtil.setLogStatus("失败");
        RestResp<Integer> resp = new RestResp<>();
        resp.result = roleService.updateRole(req);
        OptLogUtil.setLogStatus("成功");
        return resp;
    }

    @ApiOperation(value = "更新角色状态", notes = "更新角色状态")
    @RequestMapping(value = "/update-status",method = RequestMethod.PUT)
    @RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE,logType = AppConsts.LOG_TPYE_ROLE,log = "[#{logUser}]更新角色状态[#{logContent}][#{logStatus}]")
    public RestResp<Integer> updateRoleStatus(@RequestBody RoleUpdateStatusReq req) {
        OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
        OptLogUtil.setLogContent(StringUtil.toJson(req));
        OptLogUtil.setLogStatus("失败");
        RestResp<Integer> resp = new RestResp<>();
        resp.result = roleService.updateRoleStatus(req);
        OptLogUtil.setLogStatus("成功");
        return resp;
    }

}
