package cn.ibaijia.soe.console.rest;

import cn.ibaijia.soe.console.rest.vo.AllClientInfoVo;
import cn.ibaijia.soe.console.rest.vo.IdxStatistics;
import cn.ibaijia.soe.console.service.BlackListService;
import cn.ibaijia.soe.console.service.IndexService;
import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.soe.console.service.SoeClientService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/v1/statistics")
public class StatisticsApi extends BaseRest {

//    @Resource
//    private BlackListService blackListService;
    @Resource
    private IndexService indexService;
    @Resource
    private SoeClientService soeClientService;

    @ApiOperation(value = "应用统计", notes = "")
	@RequestMapping(value = "/",method = RequestMethod.GET)
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.NONE)
	public RestResp<IdxStatistics> list(){
		RestResp<IdxStatistics> resp = new RestResp<>();
        IdxStatistics idxStatistics = indexService.idxStatistics();
        resp.result = idxStatistics;
		return resp;
	}

    @ApiOperation(value = "所有ClientInfo", notes = "")
	@RequestMapping(value = "/all-client-info",method = RequestMethod.GET)
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.NONE)
	public RestResp<AllClientInfoVo> allClientInfo(){
		RestResp<AllClientInfoVo> resp = new RestResp<>();
        resp.result = soeClientService.getClientInfo();
		return resp;
	}

}
