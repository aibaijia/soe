package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.Permission;
import cn.ibaijia.soe.console.dao.model.Role;
import cn.ibaijia.soe.console.dao.model.User;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:user_t
 */
public interface UserMapper {

	/**
	 * 新增User
	 */
	@Insert("<script>insert into user_t (username,password,status) values (#{username},#{password},#{status})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(User user);

	/**
	 * 批量新增User
	 */
	@Insert("<script>insert into user_t (username,password,status) values <foreach collection='list' item='item' index='index' separator=','> (#{item.username},#{item.password},#{item.status})</foreach></script>")
	int addBatch(List<User> userList);

	/**
	 * 修改User(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update user_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.username,username)'>,username=#{username}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.password,password)'>,password=#{password}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.status,status)'>,status=#{status}</if> where `id`=#{id}</script>")
	int update(User user);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from user_t t where t.id=#{id}</script>")
	User findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from user_t t where t.id=#{id}</script>")
	User findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from user_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from user_t t order by t.id desc</script>")
	List<User> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<User> pageList(Page<User> page);

	User findByUsername(@Param("username")String username);

	List<Role> findRolesByUserId(@Param("id")Long id);

	List<Permission> findPermissionsByUserId(@Param("id")Long id);
}