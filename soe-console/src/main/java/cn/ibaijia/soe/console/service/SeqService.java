package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.dao.mapper.SeqMapper;
import cn.ibaijia.soe.console.dao.model.Seq;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.jedis.JedisService;
import cn.ibaijia.jsm.utils.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class SeqService extends BaseService {

    private Seq appId = new Seq("appId", "", 1L);

    @Resource
    private SeqMapper seqMapper;

    public Short getNextAppId() {
        try {
            Seq dbSeq = seqMapper.findByName(this.appId.name);
            if (dbSeq == null) {
                dbSeq = this.appId;
                seqMapper.add(dbSeq);
            } else {
                dbSeq.seqVal++;
                seqMapper.update(dbSeq);
            }
            return dbSeq.seqVal.shortValue();
        } catch (Exception e) {
            logger.error("getNextAppId error!", e);
            return null;
        }
    }

}
