package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;

public class UserChangePasswordReq implements ValidateModel {

    @FieldAnn(maxLen = 50)
    public String oldPassword;
    @FieldAnn(maxLen = 50)
    public String newPassword;

}
