package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.AppStatistics;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:app_statistics_t
 */
public interface AppStatisticsMapper {

	/**
	 * 新增AppStatistics
	 */
	@Insert("<script>insert into app_statistics_t (fromId,fromSn,toId,toSn,bytesCount,lastTime) values (#{fromId},#{fromSn},#{toId},#{toSn},#{bytesCount},#{lastTime})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(AppStatistics appStatistics);

	/**
	 * 批量新增AppStatistics
	 */
	@Insert("<script>insert into app_statistics_t (fromId,fromSn,toId,toSn,bytesCount,lastTime) values <foreach collection='list' item='item' index='index' separator=','> (#{item.fromId},#{item.fromSn},#{item.toId},#{item.toSn},#{item.bytesCount},#{item.lastTime})</foreach></script>")
	int addBatch(List<AppStatistics> appStatisticsList);

	/**
	 * 修改AppStatistics(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update app_statistics_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.fromId,fromId)'>,fromId=#{fromId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.fromSn,fromSn)'>,fromSn=#{fromSn}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.toId,toId)'>,toId=#{toId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.toSn,toSn)'>,toSn=#{toSn}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.bytesCount,bytesCount)'>,bytesCount=#{bytesCount}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.lastTime,lastTime)'>,lastTime=#{lastTime}</if> where `id`=#{id}</script>")
	int update(AppStatistics appStatistics);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from app_statistics_t t where t.id=#{id}</script>")
	AppStatistics findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from app_statistics_t t where t.id=#{id}</script>")
	AppStatistics findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from app_statistics_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from app_statistics_t t order by t.id desc</script>")
	List<AppStatistics> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<AppStatistics> pageList(Page<AppStatistics> page);

    int updateBytesCount(AppStatistics appStatistics);

}