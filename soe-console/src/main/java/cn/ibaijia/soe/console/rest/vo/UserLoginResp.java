package cn.ibaijia.soe.console.rest.vo;

import cn.ibaijia.soe.console.dao.model.Permission;
import cn.ibaijia.soe.console.dao.model.Role;
import cn.ibaijia.soe.console.dao.model.UserInfo;
import cn.ibaijia.jsm.rest.resp.LoginResp;

import java.util.List;

public class UserLoginResp extends LoginResp {
	public UserInfo userInfo;
	public List<Role> roles;
	public List<Permission> permissions;

}
