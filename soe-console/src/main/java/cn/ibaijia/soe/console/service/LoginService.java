package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.consts.AppPairConsts;
import cn.ibaijia.soe.console.dao.mapper.UserInfoMapper;
import cn.ibaijia.soe.console.dao.mapper.UserMapper;
import cn.ibaijia.soe.console.dao.model.User;
import cn.ibaijia.soe.console.dao.model.UserInfo;
import cn.ibaijia.soe.console.rest.req.UserLoginReq;
import cn.ibaijia.soe.console.rest.vo.UserLoginResp;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.rest.resp.FileResp;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.jsm.session.Session;
import cn.ibaijia.jsm.session.SessionUser;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.EncryptUtil;
import cn.ibaijia.jsm.utils.ValidCodeUtil;
import cn.ibaijia.jsm.utils.WebUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class LoginService extends BaseService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserInfoMapper userInfoMapper;

    public RestResp<UserLoginResp> login(UserLoginReq req, RestResp<UserLoginResp> baseResp) {
        User dbuser = userMapper.findByUsername(req.username);
        if (dbuser == null || !dbuser.password.equals(EncryptUtil.md5(req.password))) {
            baseResp.setPair(AppPairConsts.USER_OR_PASSWORD_ERROR);
            return baseResp;
        }

        UserInfo userInfo = userInfoMapper.findById(dbuser.id);
        // create session
        String token = WebUtil.genToken(dbuser.username);
        Session session = Session.create(token);
//		Session.expire(dbuser.id); TODO
        SessionUser sessionUser = new SessionUser();
        sessionUser.setUid(dbuser.id);
        session.setSessionUser(sessionUser);
        session.set(AppConsts.SESSION_USERNAME_KEY, dbuser.username);
        // update login time
        UserLoginResp loginResp = new UserLoginResp();
        loginResp.serverTime = DateUtil.currentTime();
        loginResp.userInfo = userInfo;
        baseResp.result = loginResp;
        return baseResp;
    }

    public FileResp validCode(String filename) {
        FileResp fileResp = null;
        try {
            ValidCodeUtil validCodeUtil = new ValidCodeUtil("", 0, 0, 0);
            fileResp = new FileResp();
            BufferedImage image = validCodeUtil.getImage();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());
            fileResp.outputObject = is;
            fileResp.fileName = filename;
            WebContext.getRequest().getSession().setAttribute(filename,validCodeUtil.getValidCode());
            return fileResp;
        } catch (IOException e) {
            logger.error("validCode error!", e);
        }
        return fileResp;
    }

}
