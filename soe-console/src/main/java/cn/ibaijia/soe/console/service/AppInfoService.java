package cn.ibaijia.soe.console.service;

import cn.ibaijia.soe.console.dao.mapper.AppAuthMapper;
import cn.ibaijia.soe.console.dao.mapper.ServiceInfoMapper;
import cn.ibaijia.soe.console.dao.model.AppInfo;
import cn.ibaijia.soe.console.dao.mapper.AppInfoMapper;
import cn.ibaijia.soe.console.rest.req.AppInfoReq;
import cn.ibaijia.soe.console.rest.req.AppInfoUpdateReq;
import cn.ibaijia.jsm.base.BaseService;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.utils.BeanUtil;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
@Service
public class AppInfoService extends BaseService {

    @Resource
    private AppInfoMapper appInfoMapper;
    @Resource
    private AppAuthMapper appAuthMapper;
    @Resource
    private ServiceInfoMapper serviceInfoMapper;
    @Resource
    private SeqService seqService;
    @Resource
    private SoeClientService soeClientService;


    public void pageList(Page<AppInfo> page) {
        appInfoMapper.pageList(page);
    }

    public Long add(AppInfoReq req) {
        AppInfo appInfo = new AppInfo();
        BeanUtil.copy(req,appInfo);
        appInfo.appId = seqService.getNextAppId(); //数据库自增
        if(appInfo.password == null){
            appInfo.password = StringUtil.genRandomString(6);
        }
        appInfo.createTime = DateUtil.currentDate();
        appInfo.createUserId = WebContext.currentUser().getUid();
        appInfoMapper.add(appInfo);
        return appInfo.id;
    }

    public Integer update(AppInfoUpdateReq req) {
        AppInfo dbAppInfo = appInfoMapper.findById(req.id);
        BeanUtil.copy(req,dbAppInfo);
        return appInfoMapper.update(dbAppInfo);
    }

    public Integer syncAll() {
        List<AppInfo> appInfoList = appInfoMapper.listAll();
        for(AppInfo appInfo:appInfoList){
            //授权的资源
            appInfo.appAuthList = appAuthMapper.findByConsumerId(appInfo.appId);
        }
        soeClientService.putAppInfoList(appInfoList);
        return appInfoList.size();
    }

    public Integer sync(Long id) {
        AppInfo appInfo = appInfoMapper.findById(id);
        if(appInfo != null){
            appInfo.appAuthList = appAuthMapper.findByConsumerId(appInfo.appId);
            soeClientService.putAppInfo(appInfo);
            return 1;
        }
        return 0;
    }

    public void pageListService(Page<AppInfo> page, Short consumerAppId ) {
        page.getQueryMap().put("consumerAppId",consumerAppId);
        appInfoMapper.pageListService(page);
        List<Short> appAuthList = appAuthMapper.findAuthServiceAppId(consumerAppId);
        for(AppInfo appInfo:page.getList()){
            appInfo.serviceInfoList = serviceInfoMapper.listByAppId(appInfo.appId);
            if(appAuthList.contains(appInfo.appId)){
                appInfo.LAY_CHECKED = true;
            }
        }
    }
}