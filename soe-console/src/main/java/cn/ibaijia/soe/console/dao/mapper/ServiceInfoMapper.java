package cn.ibaijia.soe.console.dao.mapper;

import cn.ibaijia.soe.console.dao.model.ServiceInfo;
import org.apache.ibatis.annotations.*;
import cn.ibaijia.jsm.dao.model.Page;

import java.util.List;
/**
 * tableName:service_info_t
 */
public interface ServiceInfoMapper {

	/**
	 * 新增ServiceInfo
	 */
	@Insert("<script>insert into service_info_t (appId,serviceId,name,clazz,method,status) values (#{appId},#{serviceId},#{name},#{clazz},#{method},#{status})</script>")
	@Options(useGeneratedKeys = true, keyProperty = "id")
	int add(ServiceInfo serviceInfo);

	/**
	 * 批量新增ServiceInfo
	 */
	@Insert("<script>insert into service_info_t (appId,serviceId,name,clazz,method,status) values <foreach collection='list' item='item' index='index' separator=','> (#{item.appId},#{item.serviceId},#{item.name},#{item.clazz},#{item.method},#{item.status})</foreach></script>")
	int addBatch(List<ServiceInfo> serviceInfoList);

	/**
	 * 修改ServiceInfo(配合findByIdForUpdate更佳)
	 */
	@Update("<script>update service_info_t set `id`=#{id}<if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.appId,appId)'>,appId=#{appId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.serviceId,serviceId)'>,serviceId=#{serviceId}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.name,name)'>,name=#{name}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.clazz,clazz)'>,clazz=#{clazz}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.method,method)'>,method=#{method}</if><if test='snapshot == null or @cn.ibaijia.jsm.utils.StringUtil@notEquals(snapshot.status,status)'>,status=#{status}</if> where `id`=#{id}</script>")
	int update(ServiceInfo serviceInfo);

	/**
	 * 查询ById
	 */
	@Select("<script>select t.* from service_info_t t where t.id=#{id}</script>")
	ServiceInfo findById(@Param("id") Long id);

	/**
	 * 查询ByIdForUpdate 查询的时候创建镜像
	 */
	@Select("<script>select t.* from service_info_t t where t.id=#{id}</script>")
	ServiceInfo findByIdForUpdate(@Param("id") Long id);

	/**
	 * 删除ById
	 */
	@Delete("<script>delete from service_info_t where `id`=#{id}</script>")
	int deleteById(@Param("id") Long id);

	/**
	 * 查询出所有（谨用）
	 */
	@Select("<script>select t.* from service_info_t t order by t.id desc</script>")
	List<ServiceInfo> listAll();

	/**
	 * 分页查询（使用时修改对应queryMap参数即可）
	 */
	List<ServiceInfo> pageList(Page<ServiceInfo> page);

    List<ServiceInfo> listByAppId(@Param("appId")Short appId);

    int deleteByAppId(@Param("appId")Short appId);
}