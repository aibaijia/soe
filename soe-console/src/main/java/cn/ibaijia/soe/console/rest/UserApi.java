package cn.ibaijia.soe.console.rest;

import cn.ibaijia.soe.console.consts.AppConsts;
import cn.ibaijia.soe.console.consts.AppPairConsts;
import cn.ibaijia.soe.console.dao.model.UserInfo;
import cn.ibaijia.soe.console.rest.req.*;
import cn.ibaijia.soe.console.service.UserService;
import cn.ibaijia.jsm.annotation.AuthType;
import cn.ibaijia.jsm.annotation.RestAnn;
import cn.ibaijia.jsm.annotation.Transaction;
import cn.ibaijia.jsm.base.BaseRest;
import cn.ibaijia.jsm.context.WebContext;
import cn.ibaijia.jsm.dao.model.Page;
import cn.ibaijia.jsm.rest.resp.RestResp;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.OptLogUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping("/v1/user")
public class UserApi extends BaseRest {

	@Resource
	private UserService userService;

	// 增加用户
	@ApiOperation(value = "增加用户", notes = "用户新增")
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE, logType = AppConsts.LOG_TPYE_USER, log = "[#{logUser}]增加用户[#{logContent}][#{logStatus}]")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public RestResp<Long> addUser(@RequestBody UserReq req) {
		OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
		OptLogUtil.setContentAndStatus(req.username,"失败");
		RestResp<Long> resp = new RestResp<Long>();
		resp.result = userService.addUser(req);
		OptLogUtil.setLogStatus("成功");
		return resp;
	}

	// 更改用户
	@ApiOperation(value = "更新用户", notes = "更新用户")
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE, logType = AppConsts.LOG_TPYE_USER, log = "[#{logUser}]更新用户[#{logContent}][#{logStatus}]")
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public RestResp<UserInfo> updateUser(@RequestBody UserUpdateReq req) {
		OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
		OptLogUtil.setContentAndStatus(StringUtil.toJson(req),"失败");
		RestResp<UserInfo> resp = new RestResp<UserInfo>();
		resp.result = userService.updateUser(req);
		OptLogUtil.setLogStatus("成功");
		return resp;
	}

	// 更改密码
	@ApiOperation(value = "更改密码", notes = "用户密码")
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE, logType = AppConsts.LOG_TPYE_USER, log = "[#{logUser}]更改密码[#{logContent}][#{logStatus}]")
	@RequestMapping(value = "/change-password", method = RequestMethod.PUT)
	public RestResp<Integer> changePassword(@RequestBody UserChangePasswordReq req) {
		OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
		Long userId = WebContext.currentUser().getUid();
		OptLogUtil.setContentAndStatus(userId+"","失败");
		RestResp<Integer> resp = new RestResp<Integer>();
		resp.result = userService.updateUserPassword(req, userId);
		OptLogUtil.setLogStatus("成功");
		return resp;
	}

	@ApiOperation(value = "用户分页列表", notes = "分页列表")
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.READ)
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public RestResp<Page<UserInfo>> pageList(Page<UserInfo> page) {
		if (page.getQueryMap().containsKey("startTime")) {
			Date startTime = DateUtil.parse(page.getQueryMap().get("startTime").toString());
			startTime = DateUtil.dayOfBegin(startTime);
			page.getQueryMap().put("startTime", startTime);
		}
		if (page.getQueryMap().containsKey("endTime")) {
			Date endTime = DateUtil.parse(page.getQueryMap().get("endTime").toString());
			endTime = DateUtil.dayOfEnd(endTime);
			page.getQueryMap().put("endTime", endTime);
		}
		RestResp<Page<UserInfo>> resp = new RestResp<Page<UserInfo>>();
		userService.pageList(page);
		resp.result = page;
		return resp;
	}

	@ApiOperation(value = "用户状态修改", notes = "1，启用 2，禁用;")
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE, logType = AppConsts.LOG_TPYE_USER, log = "[#{logUser}][更改用户状态#{logContent}][#{logStatus}]")
	@RequestMapping(value = "/change-status", method = RequestMethod.PUT)
	public RestResp<Boolean> changeStatus(@RequestBody UserUpdateStatusReq req) {
		OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
		Long userId = WebContext.currentUser().getUid();
		OptLogUtil.setContentAndStatus(userId+"更新用户"+req.id+"状态为"+req.status,"失败");
		RestResp<Boolean> resp = new RestResp<Boolean>();
		resp.result = userService.updateUserStatus(req);
		OptLogUtil.setLogStatus("成功");
		return resp;
	}

	@ApiOperation(value = "重置密码", notes = "根据用户id重置密码为初始密码")
	@RestAnn(authType = AuthType.WEB, transaction = Transaction.WRITE, logType = AppConsts.LOG_TPYE_USER, log = "[#{logUser}]重置密码[#{logContent}][#{logStatus}]")
	@RequestMapping(value = "/reset-password", method = RequestMethod.PUT)
	public RestResp<Integer> resetPassword(@RequestBody UserResetPwdReq req) {
		OptLogUtil.setLogUser((String) WebContext.currentSession().get(AppConsts.SESSION_USERNAME_KEY));
		Long userId = WebContext.currentUser().getUid();
		OptLogUtil.setContentAndStatus(userId+"重置用户"+req.id,"失败");
		RestResp<Integer> resp = new RestResp<Integer>();
		resp.result = userService.resetPassword(req.id);
		OptLogUtil.setLogStatus("成功");
		return resp;
	}

}
