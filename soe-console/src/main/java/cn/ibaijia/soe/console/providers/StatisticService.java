package cn.ibaijia.soe.console.providers;

import cn.ibaijia.jsm.context.SpringContext;
import cn.ibaijia.jsm.utils.DateUtil;
import cn.ibaijia.jsm.utils.StringUtil;
import cn.ibaijia.soe.client.message.SystemStat;
import cn.ibaijia.soe.client.protocol.SoeObject;
import cn.ibaijia.soe.client.provider.BaseService;
import cn.ibaijia.soe.console.dao.model.AppStatistics;
import cn.ibaijia.soe.console.service.AppStatisticsService;
import com.alibaba.fastjson.JSON;

public class StatisticService extends BaseService {

    public StatisticService(short id, String name) {
        super(id, name);
    }

    /**
     * 更新启动注册functions
     */
    @Override
    public void doService(SoeObject soeObject) {
        String body = soeObject.getBodyAsString();
        logger.info("statistic:{}", body);
        String[] arr = body.split(" ");
        AppStatistics appStatistics = new AppStatistics();
        String head = arr[0].replace("-->", ".");
        String[] headArr = head.split("\\.");
        appStatistics.fromId = Short.valueOf(headArr[0]);
        appStatistics.fromSn = Byte.valueOf(headArr[1]);
        appStatistics.toId = Short.valueOf(headArr[2]);
        appStatistics.toSn = Byte.valueOf(headArr[3]);
        Integer length = arr[4].length() - 8;
        if (length != 0) {
            appStatistics.bytesCount = length.longValue();
        } else {
            appStatistics.bytesCount = 0L;
        }
        appStatistics.lastTime = DateUtil.currentDate();
        AppStatisticsService appStatisticsService = SpringContext.getBean(AppStatisticsService.class);
        appStatisticsService.saveOrUpdate(appStatistics);
    }
}
