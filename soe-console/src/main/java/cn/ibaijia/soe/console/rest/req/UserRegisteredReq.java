package cn.ibaijia.soe.console.rest.req;

import cn.ibaijia.jsm.annotation.FieldAnn;
import cn.ibaijia.jsm.base.ValidateModel;


public class UserRegisteredReq implements ValidateModel {


    @FieldAnn(maxLen = 50)
    public String mobile;
    @FieldAnn
    public String code;
    @FieldAnn(minLen = 8)
    public String password;
    
    public Long proxyId;
    
    @FieldAnn
    public String name;
    @FieldAnn
    public String idCardNo;

}
