#!/bin/sh
# vim:sw=4:ts=4:et
# 版本号（每次发布更新,建议带上日期）版本更新开始时由PM或者QA定版本号
VERSION=1.0.0
ENV=$1
if [[ ENV != "prod" ]]
then
VERSION="${VERSION}_$(date "+%Y%m%d%H%M%S")"
fi
SERVICE_NAME="${ENV}_soe-console"
REGISTRY=10.0.0.220:5000
if [[ ${REPLICAS} != "" ]]
 then
    REPLICAS=${REPLICAS}
 else
    REPLICAS=1
fi
echo "REPLICAS ${REPLICAS}"
PORT=8088


createOrUpdate(){
    docker stop ${SERVICE_NAME}
    docker rm ${SERVICE_NAME}
    docker run -e TZ="Asia/Shanghai" --name ${SERVICE_NAME} -p${PORT}:8080 -dit --privileged=true --restart=always ${REGISTRY}/${SERVICE_NAME}:${VERSION}
# 检查是创建服务还是更新服务
#    service_list=`docker service ps ${SERVICE_NAME}|grep ${SERVICE_NAME}`
#    if [[ ${service_list} == "" ]]
#    then
#        # 发布服务  docker service rm ${SERVICE_NAME}
#        echo "docker service create --replicas ${REPLICAS} --name ${SERVICE_NAME} --publish ${PORT}:8080  ${REGISTRY}/${SERVICE_NAME}:${VERSION} begin."
#        docker service create --replicas ${REPLICAS} --name ${SERVICE_NAME} --publish ${PORT}:8080 ${REGISTRY}/${SERVICE_NAME}:${VERSION}
#        echo "docker service create --replicas ${REPLICAS} --name ${SERVICE_NAME} --publish ${PORT}:8080  ${REGISTRY}/${SERVICE_NAME}:${VERSION} complete."
#    else
#        # 更新服务
#        echo "docker service update ${SERVICE_NAME} --image ${REGISTRY}/${SERVICE_NAME}:${VERSION} begin."
#        docker service update ${SERVICE_NAME} --image ${REGISTRY}/${SERVICE_NAME}:${VERSION}
#        echo "docker service update ${SERVICE_NAME} --image ${REGISTRY}/${SERVICE_NAME}:${VERSION} complete."
#        echo "docker service scale ${SERVICE_NAME}=${REPLICAS} begin."
#        docker service scale ${SERVICE_NAME}=${REPLICAS}
#        echo "docker service scale ${SERVICE_NAME}=${REPLICAS} complete."
#    fi
}

buildAndDeploy(){
    # mvn 打包
    echo "mvn clean package -P ${ENV}"
    res=$?
    if [[ ${res} != 0 ]]
    then
        echo "mvn package failed.${res}"
        exit 1
    fi
    mvn clean package -P ${ENV} -Dmaven.test.skip=true
    # 构建镜像
    echo "docker build ${REGISTRY}/${SERVICE_NAME}:${VERSION} begin."
    # docker-compose -f docker-compose.yml build ${SERVICE_NAME}
    docker build -f Dockerfile -t ${REGISTRY}/${SERVICE_NAME}:${VERSION} .
    echo "docker build ${REGISTRY}/${SERVICE_NAME}:${VERSION} complete."
    # 推送镜像
    echo "docker push ${REGISTRY}/${SERVICE_NAME}:${VERSION} begin."
    # docker-compose -f docker-compose.yml push ${SERVICE_NAME}
#    docker push ${REGISTRY}/${SERVICE_NAME}:${VERSION}
    echo "docker push ${REGISTRY}/${SERVICE_NAME}:${VERSION} complete."
    createOrUpdate
}

echo "env:$1 type:$2 specific:$3"
VERSION="$1.${VERSION}"
case $2 in
    Deploy)
        echo "Deploy"
        if [[ $1 == "prod" ]] # 正式环境 检查新版本号
        then
            images_list=`docker images |grep ${SERVICE_NAME} |grep ${VERSION}`
            if [[ ${images_list} != "" ]]
            then
             echo "version exists,please update VERSION in deploy.sh. ${SERVICE_NAME}:${VERSION}"
             exit 1
            fi
        fi
        buildAndDeploy
        ;;
    DeploySpecific)
        echo "DeploySpecific"
        # 检查分支
        git_log=`git log |grep $3`
        if [[ ${git_log} == "" ]]
        then
         echo "commit not exists:$3"
         exit 1
        fi
        git checkout $3
        VERSION="${VERSION}-$3"
        buildAndDeploy
        ;;
    Rollback)
        echo "Rollback"
        echo "docker service rollback ${SERVICE_NAME} begin."
        docker service rollback ${SERVICE_NAME}
        echo "docker service rollback ${SERVICE_NAME} complete"
        ;;
    RollbackSpecific)
        echo "RollbackSpecific"
        VERSION=$3
        createOrUpdate
        ;;
    *)
        echo "unknown type."
        exit 1
        ;;
esac

