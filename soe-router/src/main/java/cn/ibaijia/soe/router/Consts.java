package cn.ibaijia.soe.router;

public class Consts {

    public static Integer APP_TYPE_CONSUMER = 1;
    public static Integer APP_TYPE_PROVIDER = 2;

    public enum MESSAGE_TYPE {
        BYTE((byte) 0, "byte type"),
        STRING((byte) 1, "string type"),
        JSON((byte) 2, "json type"),
        XML((byte) 3, "xml type"),
        FILE((byte) 4, "file type");
        private byte code;
        private String desc;

        MESSAGE_TYPE(byte code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public byte code() {
            return this.code;
        }

        public String desc() {
            return this.desc;
        }

        public static String codeToDesc(byte code) {
            MESSAGE_TYPE[] arr = values();
            for (MESSAGE_TYPE type : arr) {
                if (type.code == code) {
                    return type.desc;
                }
            }
            return null;
        }

        public static byte descToCode(String desc) {
            MESSAGE_TYPE[] arr = values();
            for (MESSAGE_TYPE type : arr) {
                if (type.desc.equals(desc)) {
                    return type.code;
                }
            }
            return 0;
        }
    }


    public enum FUNCID {
        LOGIN_HELLO((short) -1, "LOGIN_HELLO"),
        LOGIN_REQ((short) -2, "LOGIN_REQ"),
        LOGIN_RESP((short) -3, "LOGIN_RESP"),

        LOGOUT_REQ((short) -4, "LOGOUT_REQ"),
        LOGOUT_RESP((short) -5, "LOGOUT_RESP"),

        RELOAD_REQ((short) -6, "RELOAD_REQ"),
        RELOAD_RESP((short) -7, "RELOAD_RESP"),

        HEARTBEAT_REQ((short) -8, "HEARTBEAT_REQ"),
        HEARTBEAT_RESP((short) -9, "HEARTBEAT_RESP"),
        PING_APP_REQ((short) -10, "PING_APP_REQ"),
        PING_APP_RESP((short) -11, "PING_APP_RESP"),

        CONSOLE_PUTAPPS_REQ((short) -50, "CONSOLE_PUTAPPS_REQ"),//Console 上传Apps信息（appId,appKey,appAuthInfo），定时调用或者手动调用
        CONSOLE_PUTAPPS_RESP((short) -51, "CONSOLE_PUTAPPS_RESP"),
        CONSOLE_PUTAPP_REQ((short) -52, "CONSOLE_PUTAPP_REQ"),
        CONSOLE_PUTAPP_RESP((short) -53, "CONSOLE_PUTAPP_RESP"),

        CONSOLE_GETAPPS_REQ((short) -54, "CONSOLE_GETAPPS_REQ"),//Console 获取Router中Apps信息（状态，收发条数，数据大小），定时调用或者手动调用
        CONSOLE_GETAPPS_RESP((short) -55, "CONSOLE_GETAPPS_RESP"),

        CONSOLE_PUTBLACKLIST_REQ((short) -58, "CONSOLE_PUTBLACKLIST_REQ"),//Console 上传Router中BlackList信息（IP），定时调用或者手动调用
        CONSOLE_PUTBLACKLIST_RESP((short) -59, "CONSOLE_PUTBLACKLIST_RESP"),

        CONSOLE_GET_CLIENT_LIST_REQ((short) -60, "CONSOLE_GET_CLIENT_LIST_REQ"),

        CONSOLE_SERVICE_REGISTER((short) 1001, "CONSOLE_SERVICE_REGISTER"),//Router 发送服务注册 到console
        CONSOLE_SERVICE_STATISTIC((short) 1002, "CONSOLE_SERVICE_STATISTIC"),//Router 发送流程统计 到console
        CONSOLE_SERVICE_HEARTBEAT((short) 1003, "CONSOLE_SERVICE_HEARTBEAT"),//Router 发送心跳记录 到console
        TOID_NOT_FOUND((short) -101, "TOID_NOT_FOUND"),//发送错误
        TOID_NOT_AUTH((short) -102, "TOID_NOT_AUTH");
        private short code;
        private String desc;

        FUNCID(short code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public short code() {
            return this.code;
        }

        public String desc() {
            return this.desc;
        }

        public static String codeToDesc(short code) {
            FUNCID[] arr = values();
            for (FUNCID type : arr) {
                if (type.code == code) {
                    return type.desc;
                }
            }
            return null;
        }

        public static short descToCode(String desc) {
            FUNCID[] arr = values();
            for (FUNCID type : arr) {
                if (type.desc.equals(desc)) {
                    return type.code;
                }
            }
            return 0;
        }
    }

}
