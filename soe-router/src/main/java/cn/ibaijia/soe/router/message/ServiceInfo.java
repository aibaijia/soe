package cn.ibaijia.soe.router.message;

/**
 * tableName:service_info_t
 */

public class ServiceInfo {

    public Long id;
    /**
     * defaultVal:NULL,
     * comments:
     */
    public Short appId;
    /**
     * defaultVal:NULL,
     * comments:
     */
    public Short serviceId;
    /**
     * defaultVal:NULL,
     * comments:
     */
    public String name;
    /**
     * defaultVal:NULL,
     * comments:
     */
    public String clazz;
    /**
     * defaultVal:NULL,
     * comments:
     */
    public String method;
    /**
     * defaultVal:NULL
     * comments:1 有效 0 无效
     */
    public Integer status;
}