package cn.ibaijia.soe.router.protocol;

import cn.ibaijia.soe.router.Consts;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class SoeObject {
    private static Logger logger = LoggerFactory.getLogger(SoeObject.class);

    private static int HEADER_LEN = 13;

    private short fromId;// 分配的Id -32768 - 32767
    private byte fromSn;// 同一应用多开的标识 -128 - 127
    private short toId;// 分配的Id
    private byte toSn;

    private short serviceId;// 2 bytes -100 < serviceId < 0 为系统内部信息
    private byte type;//0-byte[]  1-string 2-json 3-xml
    private int msgId;//4个byte已经够用了，唯一性由 应用维护

    private ByteBuffer buffer;//FullBuffer

    private String fromName;//fromId.fromSn
    private String toName;//toId.toSn
    private byte[] body;
    private String logStr;

    /**
     * 解码时使用
     *
     * @param fullBuffer
     */
    public SoeObject(ByteBuffer fullBuffer) {
        this.buffer = fullBuffer;
        if (this.buffer != null) {
            this.buffer.flip();
            this.fromId = this.buffer.getShort(0);
            this.fromSn = this.buffer.get(2);
            this.toId = this.buffer.getShort(3);
            this.toSn = this.buffer.get(5);
            this.serviceId = this.buffer.getShort(6);
            this.type = this.buffer.get(8);
            this.msgId = this.buffer.getInt(9);
        }
    }

    public SoeObject(short serviceId, Object obj) {
        this.serviceId = serviceId;
        if (obj instanceof byte[]) {
            this.body = (byte[]) obj;
        } else if (obj instanceof ByteBuffer) {
            this.body = ((ByteBuffer) obj).array();
        } else {
            String body = null;
            if (obj instanceof String) {
                body = (String) obj;
            } else {
                body = JSON.toJSONString(obj);
            }
            byte[] bytes = body.getBytes(StandardCharsets.UTF_8);
            this.body = bytes;
        }
        initBuffer();
    }

    public SoeObject(short toId, short serviceId, Object obj) {
        this(toId, (byte) 0, serviceId, Consts.MESSAGE_TYPE.BYTE.code(), 0, obj);
    }

    public SoeObject(short toId, byte toSn, short serviceId, Object obj) {
        this(toId, toSn, serviceId, Consts.MESSAGE_TYPE.BYTE.code(), 0, obj);
    }

    public SoeObject(short toId, byte toSn, short serviceId, byte type, int msgId, Object obj) {
        this.toId = toId;
        this.toSn = toSn;
        this.type = type;
        this.serviceId = serviceId;
        this.msgId = msgId;
        if (obj instanceof byte[]) {
            this.body = (byte[]) obj;
        } else if (obj instanceof ByteBuffer) {
            this.body = ((ByteBuffer) obj).array();
        } else {
            String body = null;
            if (obj instanceof String) {
                body = (String) obj;
            } else {
                body = JSON.toJSONString(obj, SerializerFeature.WriteNonStringKeyAsString);
            }
            byte[] bytes = body.getBytes(StandardCharsets.UTF_8);
            this.body = bytes;
        }
        initBuffer();
    }

    private void initBuffer() {
        this.buffer = ByteBuffer.allocate(body.length + HEADER_LEN);
        this.buffer.position(3);
        this.buffer.putShort(toId);
        this.buffer.put(toSn);
        this.buffer.putShort(serviceId);
        this.buffer.put(type);
        this.buffer.putInt(msgId);
        this.buffer.put(body);
        this.buffer.flip();
    }

    public ByteBuffer getBuffer() {

        return this.buffer;
    }

    public void setFromInfo(Short fromId, Byte fromSn) {
        this.buffer.putShort(0, fromId);
        this.buffer.put(2, fromSn);
        this.fromId = fromId;
        this.fromSn = fromSn;
        this.fromName = null;
    }

    public Short getToId() {
        return toId;
    }

    public Short getServiceId() {
        return serviceId;
    }

    public Byte getFromSn() {
        return fromSn;
    }

    public Byte getToSn() {
        return toSn;
    }

    public void setServiceId(Short serviceId) {
        this.serviceId = serviceId;
    }

    public String getFromName() {
        if (this.fromName == null) {
            this.fromName = this.fromId + "." + this.fromSn;
        }
        return fromName;
    }

    public String getToName() {
        if (this.toName == null) {
            this.toName = this.toId + "." + this.toSn;
        }
        return toName;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public byte[] getBody() {
        if (body == null && this.buffer != null) {
            body = new byte[this.buffer.capacity() - HEADER_LEN];
            this.buffer.mark();
            this.buffer.position(HEADER_LEN);
            this.buffer.get(body);
            this.buffer.reset();
        }
        return body;
    }

    public String getLogStr() {
        if (logStr == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.getFromName()).append("-->").append(this.getToName());
            sb.append(" serviceId:").append(this.serviceId).append(" type:").append(this.type).append(" msgId:").append(this.msgId);
            if (this.type == 0) {
                sb.append(" length:").append(this.getBody().length);
            } else {
                sb.append(" content:").append(this.getBodyAsString());
            }
            this.logStr = sb.toString();
        }
        return logStr;
    }


    public SoeObject makeResponse(short serviceId, byte type, Object object) {
        SoeObject soeObject = new SoeObject(fromId, fromSn, serviceId, type, msgId, object);
        soeObject.setMsgId(msgId);
        return soeObject;
    }

    public SoeObject makeResponse(byte type, Object object) {
        short serviceId = 0;
        return makeResponse(serviceId, type, object);
    }

    public SoeObject makeResponse(Object object) {
        short serviceId = 0;
        byte type = Consts.MESSAGE_TYPE.STRING.code();
        return makeResponse(serviceId, type, object);
    }

    public String getBodyAsString() {
        return new String(getBody());
    }

    public JSONObject getBodyAsJson() {
        JSONObject json = null;
        try {
            json = JSON.parseObject(getBodyAsString());
        } catch (Exception e) {
            logger.error("body parse json error!", e);
        }
        return json;
    }

}
