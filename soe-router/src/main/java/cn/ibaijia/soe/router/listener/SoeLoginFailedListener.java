package cn.ibaijia.soe.router.listener;

public interface SoeLoginFailedListener {

    void run(Short appId);

}
