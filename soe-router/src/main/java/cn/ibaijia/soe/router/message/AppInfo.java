package cn.ibaijia.soe.router.message;

import java.util.List;

public class AppInfo {

    public Long id;

    /**
     * defaultVal:NULL,
     * comments:
     */
    public String name;
    /**
     * defaultVal:NULL,
     * comments:
     */
    public Short appId;
    /**
     * defaultVal:NULL,
     * comments:
     */
    public String password;
    /**
     * defaultVal:NULL
     * comments:1 消费者 2 生产者
     */
    public Integer type;
    /**
     * defaultVal:NULL
     * comments:1 启用 0 禁用
     */
    public Integer status;
    /**
     * defaultVal:NULL
     * comments:1 全部授权 2 部分授权
     */
    public Integer authType;

    /**
     * APP对应的 授权信息
     */
    public List<AppAuth> appAuthList;

    /**
     * openAll == false时需要判断
     */
    public List<ServiceInfo> serviceInfoList;


}
