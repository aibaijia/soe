package cn.ibaijia.soe.router.session;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.soe.router.listener.DefaultSoeSessionListener;
import cn.ibaijia.soe.router.listener.SoeSessionListener;
import cn.ibaijia.soe.router.protocol.SoeObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class SoeSession {
    private static final Logger logger = LoggerFactory.getLogger(SoeSession.class);
    private Session session;

    private short id;//client or provider's id
    private byte sn;//同一应用多开的id 同一应用唯一

    private String fullName;

    public SoeSession(Session session) {
        this.session = session;
        this.session.setAttribute("self", this);
    }

    public void write(SoeObject soeObject) {
        try {
            this.session.write(soeObject);
        } catch (Exception e) {
            logger.error("write error.", e);
            SoeSessionManager.close(this);
        }
    }

    public void close() {
        logger.debug("close.");
        if (session == null) {
            logger.warn("session is null.");
            return;
        }
        SoeSessionListener soeSessionListener = (SoeSessionListener)session.getContext().getSessionListener();
        soeSessionListener.closed(this);
        SessionManager.close(this.session);
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public byte getSn() {
        return sn;
    }

    public void setSn(byte sn) {
        this.sn = sn;
    }

    public Session getSession() {
        return session;
    }


    public String getFullName() {
        if (fullName == null) {
            this.fullName = (this.id + "." + this.sn);
        }
        return fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SoeSession that = (SoeSession) o;
        return id == that.id &&
                sn == that.sn;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, sn);
    }
}
