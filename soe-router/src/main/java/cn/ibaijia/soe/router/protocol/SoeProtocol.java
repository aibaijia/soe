package cn.ibaijia.soe.router.protocol;

import cn.ibaijia.isocket.protocol.Protocol;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.isocket.session.SessionManager;
import cn.ibaijia.soe.router.Consts;
import cn.ibaijia.soe.router.session.SoeSession;
import cn.ibaijia.soe.router.session.SoeSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class SoeProtocol implements Protocol<ByteBuffer,SoeObject> {

    private static Logger logger = LoggerFactory.getLogger(SoeProtocol.class);

    @Override
    public SoeObject decode(ByteBuffer data, Session session) {
        logger.debug("decode");
        if(data == null){
            return null;
        }
        SoeSession soeSession = SoeSessionManager.get(session);
        SoeObject soeObject = new SoeObject(data);
        if(soeSession != null){
            soeObject.setFromInfo(soeSession.getId(),soeSession.getSn());
        }
        logger.info("recv:{}",soeObject.getLogStr());
        return soeObject;
    }

    @Override
    public ByteBuffer encode(SoeObject data, Session session) {
        logger.debug("encode");
        if(data == null){
            return null;
        }
        logger.info("send:{}",data.getLogStr());
        return data.getBuffer();
    }
}
