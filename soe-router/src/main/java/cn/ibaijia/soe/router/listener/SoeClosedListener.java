package cn.ibaijia.soe.router.listener;

import cn.ibaijia.soe.router.session.SoeSession;

public interface SoeClosedListener {

    void run(SoeSession session);

}
