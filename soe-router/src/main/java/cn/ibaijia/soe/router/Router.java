package cn.ibaijia.soe.router;

import java.util.Set;

import cn.ibaijia.isocket.Server;
import cn.ibaijia.isocket.protocol.FixLengthBigBufferProtocol;
import cn.ibaijia.soe.router.listener.*;
import cn.ibaijia.soe.router.processor.SoeObjectProcessor;
import cn.ibaijia.soe.router.protocol.SoeProtocol;

public class Router {


    private Server server;
    private String host;
    private int port;
    private int threadNumber = 10;
    private short consoleId;
    private String consoleIp;
    private String consoleKey;
    private Set<String> blackList;
    private SoeSessionListener soeSessionListener = new DefaultSoeSessionListener();

    public Router(String host, int port, short consoleId, String consoleKey, String consoleIp) {
        this.host = host;
        this.port = port;
        this.consoleId = consoleId;
        this.consoleKey = consoleKey;
        this.consoleIp = consoleIp;
    }

    @SuppressWarnings("unchecked")
	public void start(){
        server = new Server(host, port);
        server.addProtocol(new SoeProtocol());
        server.addProtocol(new FixLengthBigBufferProtocol());
        server.setProcessor(new SoeObjectProcessor(this));
        server.setThreadNumber(threadNumber);
        server.setSessionListener(soeSessionListener);
        server.start();

        server.setBlackList(blackList);
    }

    public void shutdown(){
        server.close();
    }

    public short getConsoleId() {
        return consoleId;
    }

    public void setConsoleId(short consoleId) {
        this.consoleId = consoleId;
    }

    public String getConsoleIp() {
        return consoleIp;
    }

    public void setConsoleIp(String consoleIp) {
        this.consoleIp = consoleIp;
    }

    public Set<String> getBlackList() {
        return blackList;
    }

    @SuppressWarnings("unchecked")
	public void setBlackList(Set<String> blackList) {
        if(this.server != null){
            server.setBlackList(blackList);
        }
        this.blackList = blackList;
    }

    public String getConsoleKey() {
        return consoleKey;
    }

    public void setConsoleKey(String consoleKey) {
        this.consoleKey = consoleKey;
    }

    public int getThreadNumber() {
        return threadNumber;
    }

    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    public void setSoeLoginSuccessListener(SoeLoginSuccessListener listener){
        this.soeSessionListener.setSoeLoginSuccessListener(listener);
    }

    public void setSoeLoginFailedListener(SoeLoginFailedListener listener){
        this.soeSessionListener.setSoeLoginFailedListener(listener);
    }

    public void setSoeClosedListener(SoeClosedListener listener){
        this.soeSessionListener.setSoeClosedListener(listener);
    }

    public SoeSessionListener getSoeSessionListener() {
        return soeSessionListener;
    }
}
