package cn.ibaijia.soe.router.processor;

import cn.ibaijia.isocket.processor.Processor;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.router.Consts;
import cn.ibaijia.soe.router.Router;
import cn.ibaijia.soe.router.listener.SoeSessionListener;
import cn.ibaijia.soe.router.message.*;
import cn.ibaijia.soe.router.protocol.SoeObject;
import cn.ibaijia.soe.router.session.SoeSession;
import cn.ibaijia.soe.router.session.SoeSessionManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class SoeObjectProcessor implements Processor<SoeObject> {

    private static Logger logger = LoggerFactory.getLogger(SoeObjectProcessor.class);

    private Router router;

    private Type appInfoListType = new TypeReference<List<AppInfo>>() {
    }.getType();
    private Type appInfoType = new TypeReference<AppInfo>() {
    }.getType();
    private Type blackListType = new TypeReference<Set<String>>() {
    }.getType();
    private Map<Short, AppInfo> appInfoMap = new HashMap<Short, AppInfo>();

    public SoeObjectProcessor(Router router) {
        this.router = router;
        AppInfo appInfo = new AppInfo();
        appInfo.appId = router.getConsoleId();
        appInfo.password = router.getConsoleKey();
        appInfoMap.put(appInfo.appId, appInfo);
    }

    @Override
    public boolean process(Session session, SoeObject soeObject) {
        Short toId = soeObject.getToId();
        Byte toSn = soeObject.getToSn();
        Short serviceId = soeObject.getServiceId();
        SoeSession fromSoeSession = SoeSessionManager.get(session);
        if (isInnerMessage(serviceId)) {
            if (fromSoeSession != null && isConsoleSession(fromSoeSession)) {
                processConsoleMessage(soeObject, session);
            } else {
                processInnerMessage(soeObject, session);
            }
        } else {
            if (fromSoeSession == null) {
                logger.error("fromSoeSession is null.checkSession attachment.sessionId:{}", session.getSessionID());
                return false;
            }
            Short fromId = fromSoeSession.getId();
            Byte fromSn = fromSoeSession.getSn();
            boolean authOk = checkAuth(fromId, toId, serviceId);
            if (!authOk) {
                logger.error("not authorized. fromId:{},toId:{},serviceId:{}.", fromId, toId, serviceId);
                SoeObject soeObject1 = soeObject.makeResponse(Consts.FUNCID.TOID_NOT_AUTH.code(), Consts.MESSAGE_TYPE.STRING.code(), soeObject.getMsgId());
                fromSoeSession.write(soeObject1);
                return false;
            }
            //option set fromId by server ,options 防止冒充fromId
            soeObject.setFromInfo(fromId, fromSn);
            //找到目标 session 发到 并写入
            SoeSession toSoeSession = SoeSessionManager.get(toId, toSn);
            if (toSoeSession == null) {
                logger.error("toSoeSession is null.toId:{}", toId);
                SoeObject soeObject1 = soeObject.makeResponse(Consts.FUNCID.TOID_NOT_FOUND.code(), Consts.MESSAGE_TYPE.STRING.code(), soeObject.getMsgId());
                fromSoeSession.write(soeObject1);
            } else {
                toSoeSession.write(soeObject);
                //write log to console statistics
                consoleStatistic(soeObject);
            }
        }
        return true;

    }

    @Override
    public void processError(Session session, SoeObject object, Throwable throwable) {
        logger.error("processError!", throwable);
    }

    private void consoleStatistic(SoeObject soeObject) {
        SoeSession consoleSoeSession = SoeSessionManager.get(this.router.getConsoleId(), (byte) 0);
        if (consoleSoeSession != null) {
            SoeObject soeObjectLog = new SoeObject(router.getConsoleId(), Consts.FUNCID.CONSOLE_SERVICE_STATISTIC.code(), soeObject.getLogStr());
            consoleSoeSession.write(soeObjectLog);
        }
    }

    private void consoleHeartbeat(SoeObject soeObject) {
        SoeSession consoleSoeSession = SoeSessionManager.get(this.router.getConsoleId(), (byte) 0);
        if (consoleSoeSession != null) {
            SystemStat systemStat = JSON.parseObject(soeObject.getBodyAsString(), SystemStat.class);
            systemStat.name = soeObject.getFromName();
            SoeObject soeObjectLog = new SoeObject(router.getConsoleId(), Consts.FUNCID.CONSOLE_SERVICE_HEARTBEAT.code(), systemStat);
            consoleSoeSession.write(soeObjectLog);
        }
    }

    private void consoleRegister(List<ServiceInfo> serviceInfos) {
        SoeSession consoleSoeSession = SoeSessionManager.get(this.router.getConsoleId(), (byte) 0);
        if (consoleSoeSession != null) {
            SoeObject soeObjectLog = new SoeObject(Consts.FUNCID.CONSOLE_SERVICE_REGISTER.code(), serviceInfos);
            consoleSoeSession.write(soeObjectLog);
        }
    }

    private void consoleClientList(SoeObject request) {
        SoeSession consoleSoeSession = SoeSessionManager.get(this.router.getConsoleId(), (byte) 0);
        if (consoleSoeSession != null) {
            SoeObject response = request.makeResponse(SoeSessionManager.getAllClientInfoVo());
            consoleSoeSession.write(response);
        }
    }

    private boolean isConsoleSession(SoeSession soeSession) {
        if (soeSession.getId() != this.router.getConsoleId()) {
            return false;
        }
        if (soeSession.getSession().getRemoteAddress() == null || !soeSession.getSession().getRemoteAddress().toString().contains(this.router.getConsoleIp())) {
            return false;
        }
        return true;

    }

    private void processConsoleMessage(SoeObject soeObject, Session session) {
        try {
            Short serviceId = soeObject.getServiceId();
            String body = soeObject.getBodyAsString();
            logger.debug("console {} serviceId:{} body:{}", Consts.FUNCID.codeToDesc(serviceId), serviceId, body);
            if (serviceId == Consts.FUNCID.CONSOLE_PUTAPPS_REQ.code()) {
                logger.info("put apps.");
                List<AppInfo> appInfoList = JSON.parseObject(body, appInfoListType);
                for (AppInfo appInfo : appInfoList) {
                    appInfoMap.put(appInfo.appId, appInfo);
                }
            } else if (serviceId == Consts.FUNCID.CONSOLE_PUTAPP_REQ.code()) {
                logger.info("put app.");
                AppInfo appInfo = JSON.parseObject(body, appInfoType);
                appInfoMap.put(appInfo.appId, appInfo);
            } else if (serviceId == Consts.FUNCID.CONSOLE_PUTBLACKLIST_REQ.code()) {
                logger.info("put blacklist.");
                Set<String> blackList = JSON.parseObject(body, blackListType);
                router.setBlackList(blackList);
            } else if (serviceId == Consts.FUNCID.CONSOLE_GET_CLIENT_LIST_REQ.code()) {
                consoleClientList(soeObject);
            } else if (serviceId == Consts.FUNCID.HEARTBEAT_REQ.code()) {
                SoeSession soeSession = SoeSessionManager.get(session);
                if (soeSession != null) {
                    SoeObject respSoeObject = new SoeObject(soeSession.getId(), soeSession.getSn(), Consts.FUNCID.HEARTBEAT_RESP.code(), "");
                    session.write(respSoeObject);
                }
            }
        } catch (Exception e) {
            logger.error("processConsoleMessage error.", e);
        }

    }

    private void processInnerMessage(SoeObject soeObject, Session session) {
        try {
            Short serviceId = soeObject.getServiceId();
            byte[] bytes = soeObject.getBody();
            String body = new String(bytes, StandardCharsets.UTF_8);
            logger.debug("inner {} serviceId:{} body:{}", Consts.FUNCID.codeToDesc(serviceId), serviceId, body);
            if (serviceId == Consts.FUNCID.LOGIN_REQ.code()) {
                ServerLoginResp respBody = login(body, session);
                SoeObject respSoeObject = new SoeObject(Consts.FUNCID.LOGIN_RESP.code(), respBody);
                session.write(respSoeObject);
            } else if (serviceId == Consts.FUNCID.LOGOUT_REQ.code()) {
                SoeSession soeSession = SoeSessionManager.get(session);
                SoeSessionManager.close(soeSession);
//            } else if (serviceId == Consts.FUNCID.RELOAD_REQ.code()) { //由client发起reload有风险
//                ServerLoginResp respBody = reload(session);
//                SoeSession soeSession = SoeSessionManager.get(session);
//                SoeObject respSoeObject = new SoeObject(soeSession.getId(), soeSession.getSn(), Consts.FUNCID.RELOAD_RESP.code(), respBody);
//                session.write(respSoeObject);
            } else if (serviceId == Consts.FUNCID.HEARTBEAT_REQ.code()) {
                SoeSession soeSession = SoeSessionManager.get(session);
                if (soeSession != null) {
                    SoeObject respSoeObject = new SoeObject(soeSession.getId(), soeSession.getSn(), Consts.FUNCID.HEARTBEAT_RESP.code(), "");
                    session.write(respSoeObject);
                    consoleHeartbeat(soeObject);
                }
            } else if (serviceId == Consts.FUNCID.PING_APP_REQ.code()) {
                SoeSession soeSession = SoeSessionManager.get(session);
                if (soeSession != null) {
                    SoeObject respSoeObject = new SoeObject(soeSession.getId(), soeSession.getSn(), Consts.FUNCID.PING_APP_RESP.code(), SoeSessionManager.listClientInfoVo(soeObject.getToId()));
                    session.write(respSoeObject);
                }
            } else {
                logger.error("unknown InnerMessage error. serviceId:{}", serviceId);
            }
        } catch (Exception e) {
            logger.error("processInnerMessage error.", e);
        }

    }

//    private ServerLoginResp reload(Session session) {
//        //TODO reload from console
//        ServerLoginResp resp = new ServerLoginResp();
//        resp.status = true;
//        resp.backupHost = new ArrayList<HostInfo>();
//        resp.appInfos = new ArrayList<AppInfo>();
//        return resp;
//    }

    private ServerLoginResp login(String body, Session session) {
        ClientLoginReq req = JSON.parseObject(body, ClientLoginReq.class);
        ServerLoginResp resp = new ServerLoginResp();
        resp.status = true;
        AppInfo appInfo = appInfoMap.get(req.appId);
        logger.debug("db appInfo:{}", JSON.toJSONString(appInfo));
        if (appInfo == null || !appInfo.password.equals(req.appKey)) {
            resp.status = false;
            return resp;
        }
        resp.backupHost = new ArrayList<HostInfo>();
        resp.appInfos = new ArrayList<AppInfo>();
        if (resp.status) {//登录成功
            byte sn = SoeSessionManager.getNewSn(req.appId);
            resp.sn = sn;
            SoeSession soeSession = new SoeSession(session);
            soeSession.setId(req.appId);
            soeSession.setSn(sn);
            SoeSessionManager.put(soeSession);
            //register functions
            if (appInfo.appId != this.router.getConsoleId()) {
                consoleRegister(req.serviceInfos);
            }
            SoeSessionListener soeSessionListener = (SoeSessionListener) session.getContext().getSessionListener();
            soeSessionListener.loginSuccess(soeSession);
        } else {
            SoeSessionListener soeSessionListener = (SoeSessionListener) session.getContext().getSessionListener();
            soeSessionListener.loginFailed(req.appId);
        }

        return resp;
    }

    private boolean isInnerMessage(Short serviceId) {
        if (serviceId > -100 && serviceId < 0) {
            return true;
        }
        return false;
    }

    private boolean checkAuth(Short fromId, Short toId, Short serviceId) {
        AppInfo fromAppInfo = appInfoMap.get(fromId);
        if (fromAppInfo.type == Consts.APP_TYPE_PROVIDER) {
            logger.debug("provider pass.");
            return true;// 服务者不验证
        }
        AppInfo toAppInfo = appInfoMap.get(toId);
        if (fromAppInfo == null || toAppInfo == null) {
            logger.error("fromAppInfo == null or toAppInfo == null");
            return false;
        }
        if (fromAppInfo.appAuthList == null) {
            logger.error("fromAppInfo no auth info.");
            return false;
        }
        for (AppAuth appAuth : fromAppInfo.appAuthList) {
            if (appAuth.consumerAppId.equals(fromId) && appAuth.serviceAppId.equals(toId)) {
                if (appAuth.serviceFuncIds == null || appAuth.serviceFuncIds == "" || appAuth.serviceFuncIds.contains("," + serviceId + ", ")) {
                    return true;
                }
            }
        }
        return false;
    }

}
