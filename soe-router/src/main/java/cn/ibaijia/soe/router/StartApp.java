package cn.ibaijia.soe.router;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartApp {

    private static Logger logger = LoggerFactory.getLogger(StartApp.class);

    public static void main(String[] args) {
        String host = System.getProperty("soe.router.host", "127.0.0.1");
        String portStr = System.getProperty("soe.router.port", "5664");
        int port = Integer.valueOf(portStr);
        String consoleIdStr = System.getProperty("soe.router.consoleId", "1");
        short consoleId = Short.valueOf(consoleIdStr);//1;
        String consoleKey = System.getProperty("soe.router.consoleKey", "111111");
        String consoleIp = System.getProperty("soe.router.consoleIp", "127.0.0.1");
        logger.info("host:{}", host);
        logger.info("port:{}", port);
        logger.info("consoleId:{}", consoleId);
        logger.info("consoleKey:{}", consoleKey);
        logger.info("consoleIp:{}", consoleIp);

        Router router = new Router(host, port, consoleId, consoleKey, consoleIp);
        router.start();

    }

    private static String getHost() {
        return System.getProperty("soe.router.host", "127.0.0.1");
    }

}
