package cn.ibaijia.soe.router.listener;

import cn.ibaijia.isocket.listener.SessionListener;
import cn.ibaijia.soe.router.session.SoeSession;

public interface SoeSessionListener extends SessionListener {

    void loginSuccess(SoeSession session);

    void loginFailed(Short appId);

//    void closing(SoeSession session);

    void closed(SoeSession session);

    void setSoeLoginSuccessListener(SoeLoginSuccessListener listener);

    void setSoeLoginFailedListener(SoeLoginFailedListener listener);

    void setSoeClosedListener(SoeClosedListener listener);
}
