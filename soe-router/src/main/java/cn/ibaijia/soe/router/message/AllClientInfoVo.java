package cn.ibaijia.soe.router.message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AllClientInfoVo {

	public Map<Short, Byte> snMap = new HashMap<>();
	public Map<Short, List<ClientInfoVo>> idMap = new HashMap<>();
	public Map<String, ClientInfoVo> nameMap = new HashMap<>();

}