package cn.ibaijia.soe.router.listener;

import cn.ibaijia.isocket.listener.DefaultSessionListener;
import cn.ibaijia.isocket.listener.SessionClosedListener;
import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.router.Consts;
import cn.ibaijia.soe.router.protocol.SoeObject;
import cn.ibaijia.soe.router.session.SoeSession;
import cn.ibaijia.soe.router.session.SoeSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultSoeSessionListener extends DefaultSessionListener implements SoeSessionListener {
    private static final Logger logger = LoggerFactory.getLogger(DefaultSoeSessionListener.class);

    public DefaultSoeSessionListener(){
        logger.debug("create DefaultSoeSessionListener");
    }

    private SoeLoginSuccessListener soeLoginSuccessListener = new SoeLoginSuccessListener() {
        @Override
        public void run(SoeSession session) {

        }
    };

    private SoeLoginFailedListener soeLoginFailedListener = new SoeLoginFailedListener() {
        @Override
        public void run(Short appId) {

        }
    };

    private SoeClosedListener soeClosedListener = new SoeClosedListener() {
        @Override
        public void run(SoeSession session) {

        }
    };

    @Override
    public void create(Session session) {
        logger.info("create:{}", session.getSessionID());
        String body = "123";
        SoeObject soeObject = new SoeObject(Consts.FUNCID.LOGIN_HELLO.code(), body);
        session.write(soeObject);
    }

    @Override
    public void loginSuccess(final SoeSession soeSession) {
        logger.info("loginSuccess:{}", soeSession.getFullName());
        soeLoginSuccessListener.run(soeSession);
        setSessionClosedListener(new SessionClosedListener() {
            @Override
            public void run(Session session) {
                SoeSessionManager.close(soeSession);
            }
        });
    }

    @Override
    public void loginFailed(Short appId) {
        logger.info("loginFailed:{}", appId);
        soeLoginFailedListener.run(appId);
    }

    @Override
    public void closed(SoeSession session) {
        logger.info("closedxxx:{}", session.getFullName());
        soeClosedListener.run(session);
    }

    @Override
    public void setSoeLoginSuccessListener(SoeLoginSuccessListener listener) {
        this.soeLoginSuccessListener = listener;
    }

    @Override
    public void setSoeLoginFailedListener(SoeLoginFailedListener listener) {
        this.soeLoginFailedListener = listener;
    }

    @Override
    public void setSoeClosedListener(SoeClosedListener listener) {
        this.soeClosedListener = listener;
    }

    @Override
    public void closed(Session session) {
        logger.info("closed:{}", session.getSessionID());
        SoeSessionManager.close(SoeSessionManager.get(session));
    }
}
