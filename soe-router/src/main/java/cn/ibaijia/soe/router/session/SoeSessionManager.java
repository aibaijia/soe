package cn.ibaijia.soe.router.session;

import cn.ibaijia.isocket.session.Session;
import cn.ibaijia.soe.router.message.AllClientInfoVo;
import cn.ibaijia.soe.router.message.ClientInfoVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SoeSessionManager {

    private static final Logger logger = LoggerFactory.getLogger(SoeSessionManager.class);
    private static Map<String, SoeSession> nameMap = new ConcurrentHashMap<>();
    private static Map<Short, List<SoeSession>> idMap = new ConcurrentHashMap<>();
    private static Map<Short, Byte> snMap = new ConcurrentHashMap<>();
    private static final byte DEFAULT_SN = 0;

    public static synchronized SoeSession get(Session session) {
        if (session == null) {
            return null;
        }
        logger.debug("get session:{}", session.getSessionID());
        for (SoeSession soeSession : nameMap.values()) {
            if (session.equals(soeSession.getSession())) {
                return soeSession;
            }
        }
        return null;
    }

    public static synchronized void put(SoeSession session) {
        if (session.getId() == 0 || session.getSn() == 0) {
            logger.error("put SoeSession: id and sn,must not be 0.");
            return;
        }
        String name = session.getId() + "." + session.getSn();
        logger.debug("put soeSession to nameMap");
        nameMap.put(name, session);

        List<SoeSession> idQueue = idMap.get(session.getId());
        if (idQueue == null) {
            idQueue = new LinkedList<>();
            idMap.put(session.getId(), idQueue);
        }
        logger.debug("put soeSession to idQueue");
        idQueue.add(session);
    }

    public static synchronized SoeSession get(Short id, Byte sn) {
        if (sn == DEFAULT_SN) {
            LinkedList<SoeSession> idQueue = (LinkedList) idMap.get(id);
            if (idQueue != null) {
                SoeSession soeSession = idQueue.poll();
                idQueue.add(soeSession);
                return soeSession;
            }
        } else {
            String name = id + "." + sn;
            return nameMap.get(name);
        }
        return null;
    }

    private static synchronized boolean close(Short id, Byte sn) {
        if (id == 0 || sn == 0) {
            logger.error("close SoeSession: id and sn,must not be 0.");
            return false;
        }
        String name = id + "." + sn;
        logger.debug("remove from nameMap");
        SoeSession soeSession = nameMap.remove(name);
        logger.debug("remove from idQueue");
        List<SoeSession> idQueue = idMap.get(id);
        if (idQueue == null) {
            logger.warn("close idQueue is null");
            return false;
        }
        idQueue.remove(soeSession);
        if (soeSession == null) {
            logger.warn("close soeSession is null");
            return false;
        }
        soeSession.close();
        return true;
    }

    public static synchronized boolean close(SoeSession soeSession) {
        if (soeSession == null) {
            logger.warn("close soeSession is null");
            return false;
        }
        return close(soeSession.getId(), soeSession.getSn());
    }

    public static synchronized Byte getNewSn(Short appId) {
        Byte currentSn = snMap.get(appId);
        if (currentSn == null) {
            currentSn = 1;
            snMap.put(appId, currentSn);
            return currentSn;
        } else {
            currentSn = (byte) (currentSn + 1);
            snMap.put(appId, currentSn);
        }
        //检查是否已经存在
        List<SoeSession> idQueue = idMap.get(appId);
        if (idQueue != null && !idQueue.isEmpty()) {
            boolean exists = false;
            Iterator<SoeSession> it = idQueue.iterator();
            while (it.hasNext()) {
                SoeSession session = it.next();
                if (session == null) {
                    it.remove();
                } else {
                    if (session.getSn() == currentSn) {
                        exists = true;
                        break;
                    }
                }
            }
            if (exists) { //sn不能为已经存在
                return getNewSn(appId);
            }
        }
        return currentSn;
    }

    public static synchronized AllClientInfoVo getAllClientInfoVo() {
        AllClientInfoVo allClientInfoVo = new AllClientInfoVo();
        allClientInfoVo.snMap = snMap;
        for (Map.Entry<Short, List<SoeSession>> entry : idMap.entrySet()) {
            List<ClientInfoVo> list = new ArrayList<>();
            for (SoeSession soeSession : entry.getValue()) {
                ClientInfoVo clientInfoVo = new ClientInfoVo();
                clientInfoVo.id = soeSession.getId();
                clientInfoVo.sn = soeSession.getSn();
                list.add(clientInfoVo);
            }
            allClientInfoVo.idMap.put(entry.getKey(), list);
        }
        for (Map.Entry<String, SoeSession> entry : nameMap.entrySet()) {
            SoeSession soeSession = entry.getValue();
            ClientInfoVo clientInfoVo = new ClientInfoVo();
            clientInfoVo.id = soeSession.getId();
            clientInfoVo.sn = soeSession.getSn();
//            clientInfoVo.reading = soeSession.getSession().isReading();
//            clientInfoVo.writing = soeSession.getSession().isWriting();
            clientInfoVo.writeCacheQueueSize = soeSession.getSession().getWriteQueueSize();
            clientInfoVo.readBufferPosition = soeSession.getSession().getReadBufferPosition();
            clientInfoVo.readBufferLimit = soeSession.getSession().getReadBufferLimit();
            clientInfoVo.readBufferCapacity = soeSession.getSession().getReadBufferCapacity();
            allClientInfoVo.nameMap.put(entry.getKey(), clientInfoVo);
        }
        return allClientInfoVo;
    }

    public static synchronized List<ClientInfoVo> listClientInfoVo(Short appId) {
        List<ClientInfoVo> list = new ArrayList<>();
        List<SoeSession> idQueue = idMap.get(appId);
        if (idQueue != null && !idQueue.isEmpty()) {
            for (SoeSession soeSession : idQueue) {
                ClientInfoVo clientInfoVo = new ClientInfoVo();
                clientInfoVo.id = soeSession.getId();
                clientInfoVo.sn = soeSession.getSn();
                list.add(clientInfoVo);
            }
        }
        return list;
    }
}
